import React from 'react';
import { IntlProvider, intlShape } from 'react-intl';
import { render, shallow } from 'enzyme';
import { ThemeProvider } from 'glamorous';
import { Themes } from '../src/commons';

const messages = require('../translations/en.json');
const intlProvider = new IntlProvider({ locale: 'en', messages }, {});
const { intl } = intlProvider.getChildContext();

const nodeWithIntlProp = node => React.cloneElement(node, { intl });

export const renderWithIntl = (node, { context, childContextTypes } = {}) => render(
  nodeWithIntlProp(node),
  {
    context: Object.assign({}, context, { intl }),
    childContextTypes: Object.assign({}, { intl: intlShape }, childContextTypes),
  },
);

export const renderWithGoogles = (node, options) => {
  return renderWithIntl(
    <ThemeProvider theme={Themes.mainTheme}>{node}</ThemeProvider>
  );
};
