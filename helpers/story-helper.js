import React from 'react';
import { injectIntl } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { boolean } from '@storybook/addon-knobs/react';

export const storyWithBoil = ({
  Formats, Themes = {}, backgroundColor = 'white', margin = 30, padding = 0
}) => (Component) => {
  const isRtl = boolean('isRtl', false);
  const isNarrow = boolean('isNarrow', false);
  const width = isNarrow ? Formats.NARROW : Formats.WIDE;
  const isThemeAlternative = boolean('isThemeAlternative', false);
  const theme = isThemeAlternative ? Themes.alternativeTheme : Themes.mainTheme;

  class BoiledComponent extends React.Component {
    render() {
      return (
        <ThemeProvider theme={theme || {}}>
          <div style={{ margin, padding, width, backgroundColor }}>
            <Component {...this.props} isRtl={isRtl} isNarrow={isNarrow} />
          </div>
        </ThemeProvider>
      );
    }
  }

  return injectIntl(BoiledComponent);
};
