import { configure, addDecorator } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import { setIntlConfig, withIntl } from 'storybook-addon-intl';
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import enTranslations from '../translations/en.json';
import keysTranslations from '../translations/keys.json';
import '@blueprintjs/core/dist/blueprint.css';
import "./story.css";
import metadata from '../package.json';

import { setLogLevel } from 'webpack/hot/log';
setLogLevel('none');

addLocaleData(enLocaleData);

const messages = {
  en: enTranslations,
  keys: keysTranslations,
};

const getMessages = locale => messages[locale];

setIntlConfig({ locales: ['en', 'keys'], defaultLocale: 'en', getMessages });
addDecorator(withIntl);

setOptions({
  name: `${metadata.name}@${metadata.version}`,
  url: 'https://github.com/cis-itn-oecd/web-components/tree/dev/lib/visions',
  addonPanelInRight: true,
  hierarchySeparator: /\|/,
});

const loadStories = () => {
  const reqCommons = require.context('../src/commons', true, /\.story\.js$/);
  reqCommons.keys().forEach(filename => reqCommons(filename));

  const reqComponents = require.context('../src/components', true, /\.story\.js$/);
  reqComponents.keys().forEach(filename => reqComponents(filename));
};

configure(loadStories, module);
