import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DEFooter, DEFooterIcon, DEFooterLink, Formats, Themes } from '../../';
import icon from '../../assets/sis-cc-icon.png';

describe('DE - Footer', () => {
  const props = {
    authorLabel: (
      <FormattedMessage
        id="de.footer.author"
        values={{
          icon: <DEFooterIcon src={icon} alt="icon" />,
          link: <DEFooterLink href="http://siscc.oecd.org/">SIS-CC</DEFooterLink>,
        }}
      />
    ),
    disclaimerLabel: (
      <FormattedMessage
        id="de.footer.disclaimer"
        values={{ br: <br/> }}
      />
    ),
  };

  it('should match snapshot wide and ltr', () => {
    const component = renderWithGoogles(<DEFooter {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot wide and rtl', () => {
    const component = renderWithGoogles(<DEFooter {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot narrow and ltr', () => {
    const component = renderWithGoogles(<DEFooter {...props} isNarrow />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot narrow and ltr', () => {
    const component = renderWithGoogles(<DEFooter {...props} isNarrow isRtl />);
    expect(component).toMatchSnapshot();
  });
});
