import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEFooter, DEFooterIcon, DEFooterLink, Formats, Themes } from '../../';
import icon from '../../assets/sis-cc-icon.png';

export const SBFooter = boilers => (
  <DEFooter
    {...boilers}
    authorLabel={
      <FormattedMessage
        id="de.footer.author"
        values={{
          icon: <DEFooterIcon src={icon} alt="icon" />,
          link: <DEFooterLink href="http://siscc.oecd.org/">SIS-CC</DEFooterLink>,
        }}
      />
    }
    disclaimerLabel={
      <FormattedMessage id="de.footer.disclaimer" values={{ br: <br/> }} />
    }
  />
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('footer', () => {
  const BoiledFooter = storyWithBoil({ Formats, Themes })(SBFooter);
  return <BoiledFooter />;
});
