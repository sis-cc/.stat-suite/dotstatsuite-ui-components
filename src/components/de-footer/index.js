import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';

const Wrapper = glamorous.div({
  lineHeight: '22px',
}, ({ isNarrow, isRtl, theme }) => {
  const style = {
    borderTop: `1px solid ${theme.footer.secondaryColor}`,
    ...theme.footer,
  }

  if (isNarrow) return style;

  return {
    ...style,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    direction: isRtl ? 'rtl' : 'ltr'
  };
});

const Author = glamorous.div({
  fontWeight: 'bold',
}, ({ isRtl }) => ({
  textAlign: isRtl ? 'right' : 'left',
}));

const Disclaimer = glamorous.div(
  {},
  ({ isNarrow, isRtl }) => ({
    textAlign: (!isNarrow && !isRtl) || (isNarrow && isRtl) ? 'right' : 'left',
  }),
);

export const DEFooterIcon = glamorous.img({
  maxHeight: 20,
  verticalAlign: 'middle',
});

export const DEFooterLink = glamorous.a({
  textDecoration: 'none',
}, ({ theme }) => ({
  color: theme.footer.selectedColor,
  '&:hover': {
    color: `${theme.footer.selectedColor} !important`,
  },
}));

export const DEFooter = ({ isNarrow, isRtl, authorLabel, disclaimerLabel }) => (
  <Wrapper isNarrow={isNarrow} isRtl={isRtl}>
    <Author isRtl={isRtl}>{authorLabel}</Author>
    <Disclaimer isNarrow={isNarrow} isRtl={isRtl}>{disclaimerLabel}</Disclaimer>
  </Wrapper>
);

DEFooter.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  authorLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  disclaimerLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};
