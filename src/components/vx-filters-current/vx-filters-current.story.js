import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import R from 'ramda';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXFiltersCurrent, Formats, Themes } from '../../';
import { items } from '../vx-filters/vx-filters.story';

export const SBFiltersCurrent = boilers => (
  <VXFiltersCurrent
    items={boolean('isBlank (current)', false) ? [] : [R.head(items)]}
    changeFilters={action('changeFilters')}
    titleLabel={<FormattedMessage id="vx.filters.current.title" />}
    clearAllLabel={<FormattedMessage id="vx.filters.current.clear"/>}
    {...boilers}
    noScroll
  />
);

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('filters-current', () => {
  const BoiledFiltersCurrent = storyWithBoil({ Formats, Themes })(SBFiltersCurrent);
  return <BoiledFiltersCurrent />;
});
