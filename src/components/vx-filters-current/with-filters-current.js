import { compose, withHandlers } from 'recompose';

export const withFiltersCurrent = Component => compose(withHandlers({
  onClearAll: ({ changeFilters }) => (event) => {
    event.preventDefault();
    changeFilters();
  },
  onClearFilter: ({ changeFilters }) => filterId => (event) => {
    event.preventDefault();
    changeFilters(filterId);
  },
  onClearFilterValue: ({ changeFilters }) => (filterId, filterValueId) => (event) => {
    event.preventDefault();
    changeFilters(filterId, filterValueId);
  },
}))(Component);
