import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import R from 'ramda';
import { VXFiltersCurrent, Themes } from '../../';
import { filters } from './vx-filters-current.story';

const items = [{
    id: 'filterx', 
    label: 'filter 2', 
    values: [
      {id: '1', label: "صباح الخير"},
      {id: '2', label: "value 2", 
        childNodes: [
          {id: '20', label: "value 20"},
          {id: '21', label: "value 21"},
          {id: '22', 
            label: "value 22", 
            className: "pt-disabled", 
            isDisabled: true, 
            childNodes: [
              {id: '220', label: "value 220"}
            ],
          },
        ],
      },
      {id: '3', label: "value 3"},
      {id: '4', 
        label: "value 4", 
        className: "pt-disabled", 
        isDisabled: true, 
        childNodes: [
          {id: '40', label: "value 40"},
          {id: '41', label: "value 41"},
          {id: '42', label: "value 42", childNodes: [{id: '420', label: "value 420"}]},
          {id: '43', label: "value 43", secondaryLabel: 'test'}
        ],
      },
    ], 
    tag: '2/21',
  },
];

describe('VX - CurrentFilters', () => {
  const props = {
    items,
    changeFilters:() => {},
    statusLabel: <FormattedMessage id="vx.filters.current.status" values={{ size: 5 }} />,
    titleLabel: <FormattedMessage id="vx.filters.current.title" />,
    clearAllLabel: <FormattedMessage id="vx.filters.current.clear"/>,
  }

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXFiltersCurrent {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<VXFiltersCurrent {...props} isRtl/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot items empty', () => {
    const component = renderWithGoogles(<VXFiltersCurrent {...props} items={[]}/>);
    expect(component).toMatchSnapshot();
  });
});
