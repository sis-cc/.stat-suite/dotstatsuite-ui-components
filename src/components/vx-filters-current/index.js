import React from 'react';
import PropTypes from 'prop-types';
import { Tag, Classes, Intent } from '@blueprintjs/core';
import glamorous from 'glamorous';
import cx from 'classnames';
import R from 'ramda';
import { withFiltersCurrent } from './with-filters-current';
import { VXFilterContainer } from '../vx-filter-container';

const FilterCurrentWrapper = glamorous.div({
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'row',
});

const StyledFilterTag = glamorous(Tag, { filterProps: ['isRtl'] })({
  marginBottom: 5,
}, ({ isRtl }) => ({
  marginRight: isRtl ? null : 5,
  marginLeft: isRtl ? 5 : null,
  [`.${Classes.TAG_REMOVABLE}`]: {
    paddingRight: isRtl ? 6 : 20,
    paddingLeft: isRtl ? 20 : 6,
  },
  [`> .${Classes.TAG_REMOVE}`]: {
    left: isRtl ? 0 : null,
  },
}));

const ClearAllWrapper = glamorous.div({
  paddingTop: 5,
  paddingBottom: 5,
}, ({ theme }) => ({
  borderTop: theme.filtersCurrent.borderTop,
}));

const FilterCurrent = ({
  id, label, values, onClearFilter, onClearFilterValue, isRtl,
}) => (
  <FilterCurrentWrapper>
    <StyledFilterTag
      className={cx(Classes.MINIMAL)}
      intent={Intent.PRIMARY}
      onRemove={onClearFilter(id)}
      isRtl={isRtl}
    >
      {label}
    </StyledFilterTag>
    {
      R.map(value => (
        <StyledFilterTag
          key={value.id}
          className={cx(Classes.MINIMAL)}
          onRemove={onClearFilterValue(id, value.id)}
          isRtl={isRtl}
        >
          {value.label}
        </StyledFilterTag>
      ))(values)
    }
  </FilterCurrentWrapper>
);

FilterCurrent.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    label: PropTypes.string,
  })),
  onClearFilter: PropTypes.func.isRequired,
  onClearFilterValue: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

const FiltersCurrent = ({
  titleLabel, statusLabel, clearAllLabel,
  items,
  onClearAll, onClearFilter, onClearFilterValue,
  isOpen, isRtl, noScroll,
}) => (
  <VXFilterContainer
    titleLabel={titleLabel}
    statusLabel={statusLabel}
    isOpen={isOpen}
    isBlank={R.isEmpty(items)}
    isRtl={isRtl}
    noCollapse
    noScroll
  >
    {R.pipe(R.values, R.map(filter =>
        <FilterCurrent
          key={filter.id}
          {...filter}
          onClearFilter={onClearFilter}
          onClearFilterValue={onClearFilterValue}
          isRtl={isRtl}
        />))(items)
    }
    <ClearAllWrapper>
      <StyledFilterTag className={cx(Classes.MINIMAL)} onRemove={onClearAll} isRtl={isRtl}>
        {clearAllLabel}
      </StyledFilterTag>
    </ClearAllWrapper>
  </VXFilterContainer>
);

FiltersCurrent.propTypes = {
  titleLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  statusLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  clearAllLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
  })),
  onClearAll: PropTypes.func.isRequired,
  onClearFilter: PropTypes.func.isRequired,
  onClearFilterValue: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  isRtl: PropTypes.bool,
  noScroll: PropTypes.bool,
};

FiltersCurrent.defaultProps = {
  items: {},
};

export const VXFiltersCurrent = withFiltersCurrent(FiltersCurrent);
