import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import R from 'ramda';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXFilters, Formats, Themes } from '../../';
import { spotlightTreeEngine } from '../../utils';
import { list, tree } from '../vx-filter/vx-filter.story';
export const items = [
  { id: 'filterx', label: 'filter 2', values: tree, tag: '2/21' },
  ...R.times(
    n => ({ id: `filter${n}`, label: `filter ${n}`, values: list, tag: `${n}/${n*n}` })
  )(7),
  { id: 'filter',
    label: R.pipe(
      R.times(n => R.take(n, 'concatenating')),
      R.join(' ')
    )(14),
    values: list,
    tag: 'R.times(R.identity, 5)'
  },
];

export const SBFilters = ({ intl, isRtl }) => (
  <VXFilters
    items={items}
    isRtl={isRtl}
    changeActiveFilter={action('changeActiveFilter')}
    selectFilterValue={action('selectFilterValue')}
    computeHasSpotlight={R.T}
    spotlight={{
      engine: spotlightTreeEngine,
      placeholder: intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
      fields: {
        'vx.spotlight.field.label': {
          id: 'vx.spotlight.field.label',
          accessor: 'label',
          isSelected: true,
        },
      },
    }}
  />
);

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('filters', () => {
  const BoiledFilters = storyWithBoil({ Formats, Themes })(SBFilters);
  return <BoiledFilters />;
});
