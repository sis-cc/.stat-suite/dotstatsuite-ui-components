import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import R from 'ramda';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { spotlightTreeEngine } from '../../utils';
import { list, tree } from '../vx-filter/vx-filter.story';
import { VXFilters, Themes } from '../../';

const items = [{
    id: 'filterx', 
    label: 'filter 2', 
    values: [
      {id: '1', label: "صباح الخير"},
      {id: '2', label: "value 2", 
        childNodes: [
          {id: '20', label: "value 20"},
          {id: '21', label: "value 21"},
          {id: '22', 
            label: "value 22", 
            className: "pt-disabled", 
            isDisabled: true, 
            childNodes: [
              {id: '220', label: "value 220"}
            ],
          },
        ],
      },
      {id: '3', label: "value 3"},
      {id: '4', 
        label: "value 4", 
        className: "pt-disabled", 
        isDisabled: true, 
        childNodes: [
          {id: '40', label: "value 40"},
          {id: '41', label: "value 41"},
          {id: '42', label: "value 42", childNodes: [{id: '420', label: "value 420"}]},
          {id: '43', label: "value 43", secondaryLabel: 'test'}
        ],
      },
    ], 
    tag: '2/21',
  },
]

describe('VX - filters', () => {
  const props = {
    changeActiveFilter:() => {},
    selectFilterValue:() => {},
    spotlight: {
      engine: spotlightTreeEngine,
      placeholder: <FormattedMessage id='vx.spotlight.placeholder'/>,
      fields: {
        'vx.spotlight.field.label': {
          id: 'vx.spotlight.field.label',
          accessor: 'label',
          isSelected: true,
        },
      },
    },
  };

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXFilters items={items} {...props}/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<VXFilters  items={items} {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });
});
