import React from 'react';
import R from 'ramda';

export const withFilters = Component => class extends React.Component {
  state = { activeFilterId: undefined };

  bubbleUp = (id) => {
    if (R.is(Function, this.props.changeActiveFilter)) {
      this.props.changeActiveFilter(id);
    }
  };

  rehydrateState = props => {
    this.setState({
      activeFilterId: R.propOr(R.prop('activeFilterId')(this.state), 'activeFilterId')(props)
    });
  };

  onChangeActiveFilter = activeFilterId => () => {
    this.setState({
      activeFilterId: activeFilterId === this.state.activeFilterId ? null : activeFilterId,
    }, () => this.bubbleUp(activeFilterId));
  };

  componentWillMount = () => this.rehydrateState(this.props);
  componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

  render = () => (
    <Component
      {...this.props}
      activeFilterId={this.state.activeFilterId}
      onChangeActiveFilter={this.onChangeActiveFilter}
    />
  );
};
