import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import R from 'ramda';
import { withFilters } from './with-filters';
import { VXFilter } from '../../';

const FilterWrapper = glamorous.div({
  marginBottom: 5,
});

export const Filters = ({
  items, isRtl, activeFilterId, onChangeActiveFilter, computeHasSpotlight, ...rest,
  }) => (
  <Fragment>
    { R.map(item => (
        <FilterWrapper key={item.id}>
          <VXFilter
            id={item.id}
            items={item.values}
            titleLabel={item.label}
            tagLabel={item.tag}
            isRtl={isRtl}
            isOpen={activeFilterId === item.id}
            onChangeIsOpen={onChangeActiveFilter(item.id)}
            hasSpotlight={computeHasSpotlight(item)}
            {...rest}
          />
        </FilterWrapper>
      ))(items)
    }
  </Fragment>
);

Filters.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    label: PropTypes.string,
    tag: PropTypes.string,
    values: PropTypes.array,
  })),
  onChangeActiveFilter: PropTypes.func.isRequired,
  computeHasSpotlight: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

export const VXFilters = withFilters(Filters);
