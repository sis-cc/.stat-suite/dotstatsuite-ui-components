export const facets = [
  {
    "id": "spaceList_s",
    "label": "Source de donnees",
    "values": [
      {
        "id": "ILO",
        "label": "ILO"
      },
      {
        "id": "FEDev2",
        "label": "FEDev2"
      }
    ]
  },
  {
    "id": "cs_OECDCS1_s",
    "label": "FR_Dissemination category scheme",
    "values": [
      {
        "id": "OECDCS1|SOC",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "label": "Conditions sociales"
      },
      {
        "id": "OECDCS1|AGRI",
        "label": "Agriculture et pêche"
      },
      {
        "id": "OECDCS1|DEMO",
        "label": "Démographie et population"
      },
      {
        "id": "OECDCS1|ENV",
        "label": "Environnement et énergie"
      },
      {
        "id": "OECDCS1|IND_TOUR",
        "label": "Industrie et tourisme"
      },
      {
        "id": "OECDCS1|DEMO.DEMO2",
        "label": "Population"
      },
      {
        "id": "OECDCS1|ECO",
        "label": "Économie et finance"
      }
    ]
  },
  {
    "id": "dim_REF_AREA_s",
    "label": "Zone de référence",
    "values": [
      {
        "id": "TN",
        "label": "Tunisie",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          "Centre Est",
          "Centre Ouest"
        ]
      }
    ]
  }
]
