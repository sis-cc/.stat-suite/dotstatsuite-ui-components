import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Collapse, Button, Classes, NonIdealState, Intent, Spinner, MenuDivider } from '@blueprintjs/core';
import cx from 'classnames';
import R from 'ramda';
import { DESearchTopic, withListBlank, withCollapse, withListLoading } from '../../';
import { withFacets } from './de-with-facets';

const Container = glamorous.div({
  maxWidth: 1000,
  minWidth: 300,
});

const Wrapper = glamorous.div({
  display: 'flex',
  justifyContent: 'center',
}, ({ isNarrow }) => ({
  flexWrap: isNarrow ? 'nowrap' : 'wrap',
  flexDirection: isNarrow ? 'column' : 'row',
}));

const IsOpen = glamorous(Button, { filterProps: ['isOpen'] })({
  padding: '5px 5px 5px 10px',
  margin: '10px 5px 0 5px',
  boxShadow: 'none !important',
  backgroundImage: 'unset !important',
  minHeight: 30,
}, ({ isOpen, theme }) => ({
  color: `${theme.topics.text} !important`,
  backgroundColor: isOpen ? `${theme.topics.active} !important` : 'rgba(0, 0, 0, 0.2) !important',
  ':hover': {
    boxShadow: 'none',
    backgroundColor: `${theme.topics.active} !important`,
  },
}));

const StyledFacets = glamorous.div({
  marginTop: 20,
  display: 'flex',
  justifyContent: 'space-between',
  boxShadow: '0 0 0 1px rgba(16, 22, 26, 0.1), 0 1px 1px rgba(16, 22, 26, 0.2), 0 2px 6px rgba(16, 22, 26, 0.2)',
  backgroundColor: 'white',
  borderRadius: 3,
}, ({ isNarrow }) => ({
  flexWrap: isNarrow ? 'nowrap' : 'wrap',
  flexDirection: isNarrow ? 'column' : 'row',
}));

const Topic = glamorous.div({
  margin: 2,
  padding: 5,
  minHeight: 30,
  cursor: 'pointer',
  ':hover': {
    backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
}, ({ isNarrow }) => ({
  width: isNarrow ? '100%' : 300,
}));

const StyledLegend = glamorous.div({
  width: '100%',
  color: '#ccc',
  display: 'flex',
  margin: '20px 0px'
});

const Styledspan = glamorous.span({
  whiteSpace: 'nowrap',
  padding: '0 10px',
});

const Blank = glamorous(NonIdealState)({
  paddingTop: 20,
  paddingBottom: 20,
});

export const DEFacetsBlank = ({ blankLabel }) => (
  <Wrapper>
    <Blank visual="circle" className={cx(Classes.DARK)} description={blankLabel} />
  </Wrapper>
);

DEFacetsBlank.propTypes = {
  blankLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
};

const StyledSpinner = glamorous(Spinner)({
  marginTop: 20,
  width: '30px !important',
  '& path.pt-spinner-head': {
    stroke: 'rgba(255, 255, 255, 0.8)',
  },
});

const StyledMenuDivider = glamorous(MenuDivider)({
  width: '100%',
  marginTop: '10px !important',
  borderColor: '#ccc !important',
});


const DEFacetsLoading = () => (
  <Wrapper>
    <StyledSpinner />
  </Wrapper>
);

const Facets = ({
  items, onChangeActivePanel, activePanelId, isRtl, isNarrow, selectTopic,
  browseLabel,
}) => (
  <Container>
    <StyledLegend><StyledMenuDivider /> <Styledspan>{browseLabel}</Styledspan><StyledMenuDivider /></StyledLegend>
    <Wrapper isNarrow={isNarrow}>
      {
        R.map((item) => {
          const isOpen = R.equals(activePanelId, item.id);
          return (
            <IsOpen
              key={item.id}
              isOpen={isOpen}
              iconName={isRtl ? cx({ 'chevron-up': isOpen, 'chevron-down': !isOpen }) : null}
              rightIconName={isRtl ? null : cx({ 'chevron-up': isOpen, 'chevron-down': !isOpen })}
              onClick={() => onChangeActivePanel(item.id)}
            >
              {item.label}
            </IsOpen>
          );
        })(items)
      }
    </Wrapper>
    {
     R.map((item) => {
        const isOpen = R.equals(activePanelId, item.id);
        return (
          <Collapse isOpen={isOpen} key={item.id}>
            <StyledFacets isNarrow={isNarrow} >
              {R.map(values => (
                <Topic key={values.id} isNarrow={isNarrow}>
                  <DESearchTopic {...values} topicId={item.id} selectTopic={selectTopic} isRtl={isRtl} />
                </Topic>
              ))(R.sortBy(R.prop('label'))(item.values))}
            </StyledFacets>
          </Collapse>
        );
      })(items)
    }
  </Container>
);

Facets.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
  })),
  activePanelId: PropTypes.string,
  selectTopic: PropTypes.func,
  onChangeActivePanel: PropTypes.func,
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  browseLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

Facets.defaultProps = {
  items: [],
};

export const DESearchFacets = R.compose(
  withFacets,
  withListBlank(DEFacetsBlank),
  withListLoading(DEFacetsLoading),
)(Facets);
