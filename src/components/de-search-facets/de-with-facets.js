import React from 'react';

export const withFacets = Component => class extends React.Component {
  state = { activePanelId: undefined };

  onChangeActivePanel = activePanelId => {
    this.setState({
      activePanelId: activePanelId === this.state.activePanelId ? null : activePanelId,
    });
  };

  render = () => (
    <Component
      {...this.props}
      activePanelId={this.state.activePanelId}
      onChangeActivePanel={this.onChangeActivePanel}
    />
  );
};
