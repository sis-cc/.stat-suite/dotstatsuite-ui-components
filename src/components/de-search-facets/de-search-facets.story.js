import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import R from 'ramda';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DESearchFacets, Formats, Themes } from '../../';
import { facets } from './mocks';

export const SBSearchFacets = boilers => (
  <div>
    <DESearchFacets
      items={boolean('isBlank (facets)', false) ? [] : R.values(facets)}
      {...boilers}
      isLoading={boolean('isLoading (facets)', false)}
      selectTopic={action('selectTopic')}
      blankLabel={<FormattedMessage id="de.search.facets.blank" />}
      browseLabel={<FormattedMessage id="de.search.facets.browse" />}
    />
  </div>
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('search-facets', () => {
  const BoiledFacets = storyWithBoil({ Formats, Themes })(SBSearchFacets);
  return <BoiledFacets />;
});
