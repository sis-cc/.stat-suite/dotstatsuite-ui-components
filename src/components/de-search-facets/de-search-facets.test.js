import React from 'react';
import { FormattedMessage } from 'react-intl';
import { boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DESearchTopics, Themes } from '../../';
import R from 'ramda';

const topics = R.times(
  n => ({
    id: `t${n}`,
    label: `topic-${n}`,
    subtopics: n === 1 ? ['subtopic-00-000'] : R.times(m => `subtopic-${n}-${m}`, 7),
  }),
  5,
);

describe('DE - search topics', () => {
  const props = {
    items: boolean('isBlank (topics)', false) ? [] : topics,
    isOpen: boolean('isOpen (topics)', true),
    changeIsOpen: action('changeIsOpen (topics)'),
    isLoading: boolean('isLoading (topics)', false),
    selectTopic: action('selectTopic'),
    collapseLabel: <FormattedMessage id="de.search.topics.browse" />,
    blankLabel: <FormattedMessage id="de.search.topics.blank" />,
  }

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<DESearchTopics {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<DESearchTopics {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isNarrow', () => {
    const component = renderWithGoogles(<DESearchTopics {...props} isNarrow />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isLoading', () => {
    const component = renderWithGoogles(<DESearchTopics {...props} isLoading />);
    expect(component).toMatchSnapshot();
  });
}); 