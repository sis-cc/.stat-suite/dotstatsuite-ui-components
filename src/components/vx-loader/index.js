import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes, Spinner } from '@blueprintjs/core';
import cx from 'classnames';

const Wrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  paddingTop: 20,
});

const StyledSpinner = glamorous(Spinner)({
  paddingBottom: 20,
});

export const VXLoader = ({ loadingLabel, isLarge, isSmall }) => (
  <Wrapper>
    <StyledSpinner className={cx({ [Classes.SMALL]: isSmall, [Classes.LARGE]: isLarge })} />
    {loadingLabel}
  </Wrapper>
);

VXLoader.propTypes = {
  loadingLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  isLarge: PropTypes.bool,
  isSmall: PropTypes.bool,
};
