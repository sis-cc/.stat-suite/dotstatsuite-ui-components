import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { VXLoader, Themes } from '../../';
import { withKnobs } from '@storybook/addon-knobs/react';

describe('Vx - loader', () => {
  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(
      <VXLoader  
        loadingLabel="loading..."
        isLarge={false}
        isSmall={false} 
      />
    );
    expect(component).toMatchSnapshot();
  });
});