import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXLoader, Formats, Themes } from '../../';

export const SBLoader = boilers => (
  <VXLoader
    {...boilers}
    loadingLabel="loading..."
    isLarge={boolean('isLarge (loader)', false)}
    isSmall={boolean('isSmall (loader)', false)}
  />
);

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('loader', () => {
  const BoiledLoader = storyWithBoil({ Formats, Themes })(SBLoader);
  return <BoiledLoader />;
});
