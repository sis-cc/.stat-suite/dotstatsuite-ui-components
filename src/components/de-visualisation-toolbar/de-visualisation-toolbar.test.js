import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DEVisualisationToolbar, Themes } from '../../';

const charts = [
  { icon: 'timeline-bar-chart',
    id: 'BarChart',
    label: <FormattedMessage id="de.visualisation.toolbar.barchart" />,
  },
  { icon: 'horizontal-bar-chart',
    id: 'RowChart',
    label: <FormattedMessage id="de.visualisation.toolbar.rowchart" />,
  },
];

const menus = [
  { label: <FormattedMessage id="de.visualisation.toolbar.table" />,
    id: 'table',
  },
  { label: <FormattedMessage id="de.visualisation.toolbar.chart" />,
    id: 'chart',
    children: charts,
  },
];

const downloads = [
  { id: 'excel-table',
    label: 'Table in Excel',
    action:() => {},
  },
  { id: 'csv-selection',
    label: 'Selection in CSV',
    action:() => {},
  },
  { id: 'csv-full',
    label: 'Full data in CSV',
    action:() => {},
  },
];

const actions = [
  { icon: 'fullscreen',
    id: 'fullscreen',
    label: 'Full screen',
    action:() => {},
  },
  { icon: 'cog',
    id: 'config',
    label: 'Customize',
    action:() => {},
  },
  { icon: 'import',
    id: 'download',
    label: 'Download',
    children: downloads,
  },
  { icon: 'social-media',
    id: 'share',
    label: 'Share',
    action:() => {},
  },
  { icon: 'satellite',
    id: 'satellite',
    label: 'Spy everyone',
    action:() => {},
  },
];

const commonProps = {
  changeMenu:() => {},
  showLabel: <FormattedMessage id="de.visualisation.toolbar.show" />,
};

const selectedMenuId = selectedMenuId => ({
  selectedMenuId: selectedMenuId ? 'RowChart' : 'table',
});

const selectedActionId = selectedActionId => ({
  selectedActionId: selectedActionId ? 'satellite' : null,
});

const loadingActionId = loadingActionId => ({
  loadingActionId: loadingActionId ? 'download' : null,
});

describe('DE - visualisation-toolbar', () => {
  const props = {
    ...commonProps,
    ...selectedMenuId(true),
    ...selectedActionId(true),
    ...loadingActionId(false),
    menus,
    actions,
  }

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isNarrow', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} isNarrow />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isRtl and isNarrow', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} isRtl isNarrow />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot selectedMenuId', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} {...selectedMenuId(false)} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot selectedActionId', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} {...selectedActionId(false)} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot loadingActionId', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} {...loadingActionId(true)} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isInline', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} isInline />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot hasNoAction', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} hasNoAction />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot menu isEmpty', () => {
    const component = renderWithGoogles(<DEVisualisationToolbar {...props} menus={[]} />);
    expect(component).toMatchSnapshot();
  });
});