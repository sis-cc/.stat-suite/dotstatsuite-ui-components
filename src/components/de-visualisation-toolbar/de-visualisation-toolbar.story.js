import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEVisualisationToolbar, Formats, Themes } from '../../';

const charts = [
  { icon: 'timeline-bar-chart',
    id: 'BarChart',
    label: <FormattedMessage id="de.visualisation.toolbar.barchart" />,
    test: 'toto',
  },
  { icon: 'horizontal-bar-chart',
    id: 'RowChart',
    label: <FormattedMessage id="de.visualisation.toolbar.rowchart" />,
  },
];

const menus = [
  { label: <FormattedMessage id="de.visualisation.toolbar.table" />,
    id: 'table',
  },
  { label: <FormattedMessage id="de.visualisation.toolbar.chart" />,
    id: 'chart',
    children: [charts],
  },
];

const downloads = [
    { id: 'excel-table',
      label: 'Table in Excel',
      action: action('excel-table'),
    },
    { id: 'csv-selection',
      label: 'Selection in CSV',
      action: action('csv-selection'),
    },
    { id: 'csv-full',
      label: 'Full data in CSV',
      action: action('csv-full'),
    },
];

const externals =  [
  {
    id: "external1",
    label: "All data in Excel", // label || defaultLabel
    link: "http://www.ilo.org/ilostat-files/Documents/Excel/MBI_535_EN.xlsx", // if isNil don't show button
    img: "https://www.ilo.org/ilostat/images/downloadexcel-24.png", // optionnal
  },
  {
    id: "external2",
    label: "All data in Excel", // label || defaultLabel
    link: "http://google.fr", // if isNil don't show button
    img: "https://www.ilo.org/ilostat/images/downloadexcel-24.png", // optionnal
  }
];

const actions = [
  { icon: 'fullscreen',
    id: 'fullscreen',
    label: 'Full screen',
    action: action('fullscreen'),
  },
  { icon: 'cog',
    id: 'config',
    label: 'Customize',
    action: action('config'),
  },
  { icon: 'import',
    id: 'download',
    label: 'Download',
    children: [downloads, externals],
  },
  { icon: 'social-media',
    id: 'share',
    label: 'Share',
    action: action('share'),
  },
  { icon: 'satellite',
    id: 'satellite',
    label: 'Spy everyone',
    action: action('satellite'),
  },
];

const commonProps = {
  changeMenu: action('changeMenu'),
  showLabel: <FormattedMessage id="de.visualisation.toolbar.show" />,
};

export const SBVisualisationToolbar = (boilers) => (
  <DEVisualisationToolbar
    menus={menus}
    actions={actions}
    {...boilers}
    {...commonProps}
  />
);

const SBVisualisationToolbars = (boilers) => {
  const ext = {
    selectedMenuId: boolean('isRowChart (toolbar)', false) ? 'RowChart' : 'table',
    selectedActionId: boolean('isSpying (toolbar)', true) ? 'satellite' : null,
    loadingActionId: boolean('isDownloading (toolbar)', true) ? 'download' : null,
  };
  return (
    <div style={{ direction: boilers.isRtl ? 'rtl' : 'ltr' }}>
      <div style={{paddingTop: 15}}>
        <DEVisualisationToolbar {...boilers} {...commonProps} {...ext} menus={menus} />
      </div>
      <div style={{paddingTop: 15}}>
        <DEVisualisationToolbar {...boilers} {...commonProps} {...ext} actions={actions} />
      </div>
      <div style={{paddingTop: 15}}>
        <SBVisualisationToolbar {...boilers} {...ext} />
      </div>
      <div style={{paddingTop: 15}}>
        <SBVisualisationToolbar {...boilers} menus={[]} actions={[R.head(actions)]} />
      </div>
    </div>
  );
};

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('visualisation-toolbar', () => {
  const BoiledVisualisationToolbars = storyWithBoil({ Formats, Themes })(SBVisualisationToolbars);
  return <BoiledVisualisationToolbars />;
});
