import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import glamorous from 'glamorous';
import { Classes, Button } from '@blueprintjs/core';
import Items from './items';

const Wrapper = glamorous.div({
  display: 'flex',
  padding: '5px 0',
  flexWrap: 'wrap',
}, ({ isInline, theme }) => ({
  flexDirection: 'row',
  borderTop: isInline ? null : `${theme.visualisationToolbar.borderTop}`,
  alignItems: isInline ? 'center' : 'baseline',
  justifyContent: isInline ? 'center' : 'space-between',
}));

const StyledMenu = glamorous(Button, { filterProps: ['isActive'] })({
  padding: '0 0 !important',
  margin: '0 10px !important',
  borderRadius: '0 !important',
  '&:hover': {
    background: 'transparent !important',
  },
}, ({ isActive, theme }) => ({
  color: `${theme.visualisationToolbar.color} !important`,
  [`& .${Classes.ICON_STANDARD}, &::before`]: {
    color: `${theme.visualisationToolbar.color} !important`,
  },
  boxShadow: isActive ? `inset 0 -2px ${theme.visualisationToolbar.color} !important` : null,
}));

const StyledAction = glamorous(Button, { filterProps: ['isActive'] })({
  '&::before': {
    margin: '0 !important',
  },
});

export const DEVisualisationToolbar = ({
  isRtl, isNarrow, changeMenu, menus, actions, showLabel, isInline,
  loadingActionId, selectedMenuId, selectedActionId,
}) => {
  const hasNoAction = R.isEmpty(actions);
  const hasOneAction = R.and(isNarrow, R.equals(R.length(actions), 1));
  let refinedActions = actions;
  if (isNarrow && !hasOneAction && !hasNoAction) {
    refinedActions = [{
      icon: 'menu',
      id: 'narrow',
      children: actions,
    }];
  }
  return (
    <Wrapper isInline={isInline}>
      { R.isEmpty(menus)
          ? <div></div>
          : <Items
              isRtl={isRtl}
              isNarrow={isNarrow}
              changeItem={changeMenu}
              items={menus}
              selectedId={selectedMenuId}
              showLabel={showLabel}
              StyledButton={StyledMenu}
            />
      }
      { hasOneAction
          ? <Items
              isRtl={isRtl}
              isNarrow={isNarrow}
              isAction
              items={refinedActions}
              selectedId={selectedActionId}
              loadingActionId={loadingActionId}
              StyledButton={StyledAction}
            />
          : null
      }
      { R.or(hasNoAction, hasOneAction)
          ? hasOneAction ? <div></div> : null
          : <Items
              isRtl={isRtl}
              isNarrow={isNarrow}
              isAction
              items={refinedActions}
              selectedId={selectedActionId}
              loadingActionId={loadingActionId}
              StyledButton={StyledAction}
            />
      }
    </Wrapper>
  );
};

DEVisualisationToolbar.proptypes = {
  changeMenu: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  menus: PropTypes.array,
  actions: PropTypes.array,
  loadingActionId: PropTypes.string,
  selectedMenuId: PropTypes.string,
  selectedActionId: PropTypes.string,
  showLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

DEVisualisationToolbar.defaultProps = {
  menus: [],
  actions: [],
};
