import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import cx from 'classnames';
import R from 'ramda';
import {
  Classes, Button, Popover, Position, Menu, MenuItem, MenuDivider, PopoverInteractionKind, Spinner,
} from '@blueprintjs/core';
import { Colors } from '../../';

const Wrapper = glamorous.div({
  display: 'flex',
  alignItems: 'center',
}, ({ isRtl, isAction }) => ({
  marginBottom: isAction ? null : 10,
  flexWrap: isAction ? 'wrap' : null,
  justifyContent: isAction ? 'flex-end' : null,
}));

const StyledLabel = glamorous.span({
}, ({ isRtl }) => ({
  marginRight: isRtl ? null : 10,
  marginLeft: isRtl ? 10 : null,
}));

const StyledButtonLabel = glamorous.div({
  whiteSpace: 'nowrap',
});

const ContainerImgMenuItem = glamorous.div({
  display: 'flex',
  '& > li': {
    width: '100%',
  },
}, ({ isRtl }) => ({
  textAlign: isRtl ? 'right' : 'left',
  [`& .${Classes.MENU_ITEM}::before`]: {
    transform: isRtl ? 'scaleX(-1)' : null,
    float: isRtl ? 'right' : 'left',
    marginLeft: isRtl ? 7 : null,
    marginRight: isRtl ? null : 7,
  },
  flexDirection: isRtl ? 'row-reverse' : 'row'
}));

const StyledImgMenuItem = glamorous.img({
  marginTop: 6,
  maxWidth: 16,
  height: 'auto'
}, ({ isRtl }) => ({
  marginLeft: isRtl ? 0 : 6,
  marginRight: isRtl ? 6 : 0,
}));

const StyledAction = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const StyledSpinner = glamorous(Spinner)({
  marginBottom: 6,
});

const isActive = (selectedId, itemId) => R.pipe(
  R.propOr([], 'children'),
  R.pluck('id'),
  R.concat([itemId]),
  R.contains(selectedId)
);

const getSubMenu = ({ isRtl, isAction, selectedId, changeItem }) => R.map(({ id, label, action = null, children = [], icon, img, link, ...item }) => (
  <ContainerImgMenuItem isRtl={isRtl} key={id}>
    {R.isNil(img) ? null : <div><StyledImgMenuItem isRtl={isRtl} src={img} /></div>}
    <MenuItem
      key={id}
      icon={R.isNil(img) ? icon : null}
      href={link}
      target={R.isNil(link) ? undefined : "_blank" }
      iconName={icon}
      text={label}
      onClick={isAction ? action : () => changeItem(id, { id, ...item })}
      className={cx({ [Classes.ACTIVE]: id === selectedId })}
    >
    { R.isEmpty(children) ? null : getMenu({ isRtl, isAction, selectedId, id })(children) }
    </MenuItem>
  </ContainerImgMenuItem>
));

const getMenu = ({ ...props }) => R.pipe(
  R.map(
    R.pipe(
      R.ifElse(R.isNil,  R.always([]), R.identity),
      R.ifElse(R.is(Array), R.identity, R.of),
      getSubMenu(props)
    ),
  ),
  R.reject(R.isEmpty),
  R.intersperse(0),
  R.addIndex(R.map)((v, index) => R.is(Number)(v) ? <MenuDivider key={`divider-${props.id}-${index}`}/> : v),
);

const Items = ({
  isRtl, isNarrow, isAction, changeItem, items, selectedId, loadingActionId, showLabel, StyledButton,
}) => (
  <Wrapper isAction={isAction}>
    { isAction ? null : <StyledLabel isRtl={isRtl}>{showLabel}</StyledLabel> }
    { R.map(item => {
        const hasNoChild = R.anyPass([R.isNil, R.isEmpty])(item.children);
        const caretIcon = hasNoChild || isAction ? null : 'caret-down';
        const onParentClick = isAction
          ? (item.action ? item.action : null)
          : changeItem && hasNoChild ? () => changeItem(item.id, item) : null;
        const isLoading = isAction && R.equals(loadingActionId, item.id);
        const icon = isAction ? isLoading ? null : item.icon : isRtl ? caretIcon : item.icon;
        const rightIcon = isAction ? null : isRtl ? item.icon : caretIcon;
        const button = (
          <StyledButton
            key={item.id}
            onClick={onParentClick}
            isActive={isAction ? false : isActive(selectedId, item.id)(item)}
            active={isAction ? R.equals(selectedId, item.id) : false}
            iconName={icon}
            rightIconName={rightIcon}
            className={Classes.MINIMAL}
          >
            { isAction
              ? isNarrow
                ? null
                : <StyledAction>
                    { isLoading ? <StyledSpinner className={Classes.SMALL} /> : null }
                    <StyledButtonLabel>{item.label}</StyledButtonLabel>
                  </StyledAction>
              : item.label
            }
          </StyledButton>
        );
        if (hasNoChild) return button;
        const content = <Menu>{ getMenu({ isRtl, isAction, selectedId, id: item.id, changeItem })(item.children)}</Menu>

        const position = isAction && isNarrow
          ? isRtl ? Position.RIGHT : Position.LEFT
          : Position.BOTTOM
        return (
          <Popover
            position={position}
            key={item.id}
            target={button}
            content={content}
            interactionKind={PopoverInteractionKind.HOVER}
          />
        );
      })(items)
    }
  </Wrapper>
);

Items.proptypes = {
  changeItem: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
  isAction: PropTypes.bool,
  items: PropTypes.shape({
    icon: PropTypes.string,
    id: PropTypes.string.isRequired,
    label: PropTypes.string,
    children: PropTypes.array,
  }),
  selectedId: PropTypes.string,
  loadingActionId: PropTypes.string,
  showLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  StyledButton: PropTypes.element.isRequired,
};

Items.defaultProps = {
  items: [],
}

export default Items;
