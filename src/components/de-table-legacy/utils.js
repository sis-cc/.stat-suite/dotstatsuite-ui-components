import R from 'ramda';

export const cartesian = R.reduce(R.pipe(R.xprod, R.map(R.unnest)), [[]]); // gandalf level...
export const compact = R.reject(R.isEmpty);

const computeCount = (dimensions, layout, type) => {
  return R.pipe(
    R.pick(R.propOr({}, type)(layout)),
    R.values,
    R.reduce((sum, dimension) => sum *= R.pipe(R.propOr([0], 'values'), R.length)(dimension), 1)
  )(dimensions);
};

export const computeRowCount = (dimensions, layout) => {
  const PREFIX = 1; // x row
  const xCount = R.pipe(R.prop(R.prop('x')(layout)), R.propOr([], 'values'), R.length)(dimensions);
  const yCount = R.pipe(R.propOr([], 'y'), R.length)(layout);
  const zCount = computeCount(dimensions, layout, 'z');
  return PREFIX + yCount + (R.equals(zCount, 1) ? 0 : zCount) + xCount * zCount;
};

export const computeColumnCount = (dimensions, layout) => {
  const PREFIX = 2; // header and separator
  const columnCount = computeCount(dimensions, layout, 'y');
  // we should not have dimension with no or 1 value here
  return R.equals(columnCount, 1) ? 2 : PREFIX+columnCount; // 2 for no Y case
};
