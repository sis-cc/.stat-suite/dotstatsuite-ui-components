import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import { WindowScroller, List, Grid, AutoSizer, ColumnSizer, CellMeasurer, CellMeasurerCache } from 'react-virtualized';

const cache = new CellMeasurerCache();

// Grid data as an array of arrays
const list = R.repeat([ 'Brian Vaughn', 'Software Engineer', 'San Jose', 'CA', 95125 ], 100000);

//<div style={{...style, width: rowIndex === 1 ? getColumnWidth*5 : null}}>
//{rowIndex === 1 ? 'Renders tabular data with virtualization along the vertical and horizontal axes.' : list[rowIndex][columnIndex]}

const cellRenderer = (getColumnWidth) => ({ columnIndex, key, rowIndex, parent, style }) => {
  //if (rowIndex === 1 && columnIndex !== 0) return;
  return (
    <CellMeasurer 
      cache={cache}
      columnIndex={columnIndex}
      key={key}
      parent={parent}
      rowIndex={rowIndex}
    >
      <div style={{...style, border: '1px solid grey' }}>
        {list[rowIndex][columnIndex]}
      </div>
    </CellMeasurer>
  );
};

const Header = ({ columnCount, width, height }) => (
  <div style={{ position: 'fixed', top: 0, backgroundColor: 'red' }}>
    <ColumnSizer columnCount={columnCount} width={width}>
      {({ adjustedWidth, getColumnWidth, registerChild }) => (
        <Grid
          ref={registerChild}
          columnWidth={getColumnWidth}
          columnCount={columnCount}
          height={height}
          cellRenderer={cellRenderer()}
          rowHeight={cache.rowHeight}
          rowCount={2}
          width={adjustedWidth}
          deferredMeasurementCache={cache}
        />
      )}
    </ColumnSizer>
  </div>
);

export const DETable = ({ observations, dimensions, layout, blankLabel, valueLabel }) => (
  <WindowScroller>
    {({ height, scrollTop }) => (
      <Fragment>
        <AutoSizer disableHeight>
          {({ width }) => (
            <Fragment>
              <ColumnSizer columnCount={list[0].length} width={width}>
                {({ adjustedWidth, getColumnWidth, registerChild }) => (
                  <Grid
                    autoHeight
                    ref={registerChild}
                    columnWidth={getColumnWidth}
                    columnCount={list[0].length}
                    height={height}
                    cellRenderer={cellRenderer(getColumnWidth)}
                    rowHeight={30}
                    rowCount={list.length}
                    scrollTop={scrollTop}
                    width={adjustedWidth}
                  />
                )}
              </ColumnSizer>
              { scrollTop > 0 ? <Header columnCount={list[0].length} width={width} height={60} /> : null }
            </Fragment>
          )}
        </AutoSizer>
      </Fragment>
    )}
  </WindowScroller>
);

DETable.propTypes = {
  observations: PropTypes.object,
  dimensions: PropTypes.object,
  layout: PropTypes.object,
  blankLabel: PropTypes.string,
  valueLabel: PropTypes.string,
};

DETable.defaultProps = {
  blankLabel: 'Nothing to display',
  valueLabel: 'Value',
};
