import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import { NonIdealState, Icon } from '@blueprintjs/core';
import sizeMe from 'react-sizeme';
import * as U from '../../utils';
import * as S from './styled';

const SizedValuesHeader = sizeMe({ noPlaceholder: true })(S.ValuesHeader);

class Table extends Component {
  constructor(props) {
    super(props);
    this.frozenYs = {};
  }

  onScroll = () =>  { // imperative, re-render is too costly
    if (R.not(this.frozen)) return;
    if (R.not(this.table)) return this.frozen.style.display = 'none';
    this.frozen.style.display = R.gt(window.pageYOffset, this.table.offsetTop) ? 'inherit' : 'none';
  };

  onSize = index => size => { // imperative, re-render is too costly
    if (R.not(R.prop(index)(this.frozenYs))) return;
    R.prop(index)(this.frozenYs).style.width = `${R.prop('width')(size)}px`; // values
  };

  componentDidMount = () => {
    this.onScroll();
    window.addEventListener('scroll', this.onScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.onScroll);
  };

  render = () => {
    // no observation -> blank
    if (R.isEmpty(this.props.observations)) {
      return <NonIdealState visual="circle" description={this.props.blankLabel} />;
    }

    // single observation -> value
    if (R.equals(1, U.size(this.props.observations))) {
      return (
        <S.ValueTable>
          <S.ValueHeader>
            {this.props.valueLabel}
          </S.ValueHeader>
          <S.ValueCell>
            {R.pipe(R.values, R.head, R.prop('value'))(this.props.observations)}
          </S.ValueCell>
        </S.ValueTable>
      );
    }

    const xDimension = R.prop(R.prop('x')(this.props.layout))(this.props.dimensions);
    const xId = R.prop('id')(xDimension);
    const xLabel = R.prop('label')(xDimension);
    const xValues = R.prop('values')(xDimension);

    // no x or no x value -> blank
    if (R.or(R.isNil(xId), R.isNil(xValues), R.isEmpty(xValues))) {
      return <NonIdealState visual="circle" description={this.props.blankLabel} />;
    }

    // TODO: rewrite properly (R.way)
    const values = type => R.pipe(
      R.pick(R.prop(type)(this.props.layout) || []),
      R.values,
      R.pluck('values')
    )(this.props.dimensions);
    const cartesian = R.pipe(values, U.cartesian, U.compact);
    const yCartesian = cartesian('y');
    const zCartesian = cartesian('z');

    // no y or no y values and no z or no z values
    if (R.and(R.isEmpty(yCartesian), R.isEmpty(zCartesian))) {
      return (
        <Fragment>
          <S.ValuesTable innerRef={n => this.table = n}>
            <SizedValuesHeader isDimension onSize={this.onSize(0)}>{xLabel}</SizedValuesHeader>
            <SizedValuesHeader onSize={this.onSize(1)} hasRightBorder>{this.props.valueLabel}</SizedValuesHeader>
            { R.map(({id, label}) => (
                <Fragment key={id}>
                  <S.ValuesCell isDimension>{label}</S.ValuesCell>
                  <S.ValuesCell hasRightBorder>{`x(${id})`}</S.ValuesCell>
                </Fragment>
              ))(xValues)
            }
          </S.ValuesTable>
          <S.Frozen innerRef={n => this.frozen = n}>
            <S.FrozenHeader>
              <S.ValuesHeader isDimension innerRef={n => this.frozenYs[0] = n}>
                {xLabel}
              </S.ValuesHeader>
              <S.ValuesHeader innerRef={n => this.frozenYs[1] = n} hasRightBorder>
                {this.props.valueLabel}
              </S.ValuesHeader>
            </S.FrozenHeader>
          </S.Frozen>
        </Fragment>
      );
    }

    // no z values
    if (R.isEmpty(zCartesian)) {
      return (
        <Fragment>
          <S.ValuesTable innerRef={n => this.table = n} columnCount={R.add(R.length(yCartesian), 1)}>
            { R.addIndex(R.map)((dimensionId, i) => (
                <Fragment key={dimensionId}>
                  <SizedValuesHeader isDimension onSize={this.onSize(i)}>
                    { R.path([dimensionId, 'label'])(this.props.dimensions) }
                  </SizedValuesHeader>
                  { R.addIndex(R.map)(({id, label}, j) => (
                      <SizedValuesHeader key={`${i}:${j}:${id}`} onSize={this.onSize(j)} hasRightBorder>
                        { label }
                      </SizedValuesHeader>
                    ))(R.prop(i)(R.transpose(yCartesian)))
                  }
                </Fragment>
              ))(R.prop('y')(this.props.layout))
            }
            <S.ValuesCell isDimension>{xLabel}</S.ValuesCell>
            { R.times(n => <S.ValuesCell hasRightBorder isBoundary />)(R.length(yCartesian)) }
            { R.map(({id, label}) => (
                <Fragment key={id}>
                  <S.ValuesCell isDimension>{label}</S.ValuesCell>
                  { R.times(n => (
                      <S.ValuesCell hasRightBorder key={`${id}:${n}`}>-</S.ValuesCell>
                    ))(R.length(yCartesian))
                  }
                </Fragment>
              ))(xValues)
            }
          </S.ValuesTable>
          <S.Frozen innerRef={n => this.frozen = n}>
          </S.Frozen>
        </Fragment>
      );
    }

    // no y values
    if (R.isEmpty(yCartesian)) {
      return (
        <Fragment>
          <S.ValuesTable innerRef={n => this.table = n}>
            <SizedValuesHeader isDimension onSize={this.onSize(0)}>{xLabel}</SizedValuesHeader>
            <SizedValuesHeader onSize={this.onSize(1)} hasRightBorder>{this.props.valueLabel}</SizedValuesHeader>
            { R.addIndex(R.map)((z, i) => (
                <Fragment>
                  <S.ValuesSection>
                    { R.addIndex(R.map)(({id, label}, j) => (
                        <Fragment>
                          { j !== 0 ? <Icon iconName="dot" /> : null }
                          <S.ValuesSectionCell isDimension>
                            { R.path([R.path(['z', j])(this.props.layout), 'label'])(this.props.dimensions) }:
                          </S.ValuesSectionCell>
                          <S.ValuesSectionCell>{label}</S.ValuesSectionCell>
                        </Fragment>
                      ))(z)
                    }
                  </S.ValuesSection>
                  { R.map(({id, label}) => (
                      <Fragment key={id}>
                        <S.ValuesCell isDimension>{label}</S.ValuesCell>
                        <S.ValuesCell hasRightBorder>{`x(${id})`}</S.ValuesCell>
                      </Fragment>
                    ))(xValues)
                  }
                </Fragment>
              ))(zCartesian)
            }
          </S.ValuesTable>
          <S.Frozen innerRef={n => this.frozen = n}>
            <S.FrozenHeader>
              <S.ValuesHeader isDimension innerRef={n => this.frozenYs[0] = n}>
                {xLabel}
              </S.ValuesHeader>
              <S.ValuesHeader innerRef={n => this.frozenYs[1] = n} hasRightBorder>
                {this.props.valueLabel}
              </S.ValuesHeader>
            </S.FrozenHeader>
            <S.ValuesSection>Reference area: France Adjustment indicator: Working day</S.ValuesSection>
          </S.Frozen>
        </Fragment>
      );
    }
  };
};

Table.propTypes = {
  observations: PropTypes.object,
  dimensions: PropTypes.object,
  layout: PropTypes.object,
  blankLabel: PropTypes.string,
  valueLabel: PropTypes.string,
};

Table.defaultProps = {
  blankLabel: 'Nothing to display',
  valueLabel: 'Value',
};

export { Table as DETable };
