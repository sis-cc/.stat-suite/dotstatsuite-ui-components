# table

## model
```javascript
{
  dimensions: {
    dimensionId: {
      id, label, values: {
        dimensionValueId: { id, label }
      }
    },
  },
  layout: {
    yDimensions: [dimensionId,], // header dimensions (columns)
    zDimensions: [dimensionId,], // in context dimensions (sections)
    xDimension: dimensionId, // variable dimension (rows)
    cDimensions: [dimensionId,] // out context dimensions (external)
  },
  observations: {
    // registry of observations, not relevant for the model
  },
  units: {
    // registry of units,not relevant for the model
    isX: boolean,
    isY: boolean,
  },
}
```

## usecases (layout combinations)

### {}
value
-----
o

### {xDimension: d1}
1  | value
----------
11 | 11
12 | 12

### {xDimension: d1, zDimensions: [d2]}
1  | value
----------
2: 21
----------
11 | 21,11
12 | 21,12
----------
2: 22
----------
11 | 22,11
12 | 22,12

### {xDimension: d1, yDimensions: [d2]}
2  | 21    | 22
------------------
11 | 21,11 | 22,11
12 | 21,12 | 22,12

### {xDimension: d1, yDimensions: [d2], zDimensions: [d3]}
2  | 21    | 22
------------------------
1  |
------------------------
3: 31
------------------------
11 | 21,31,11 | 22,31,11
12 | 21,31,12 | 22,31,12
------------------------
3: 32
------------------------
11 | 21,32,11 | 22,32,11
12 | 21,32,12 | 22,32,12

### {xDimension: d1, yDimensions: [d2, d3], zDimensions: [d4, d5]}
2  | 21 | 21 | 22 | 22
----------------------
3  | 31 | 32 | 31 | 32
----------------------
1  |
----------------------
4:41, 5:51
----------------------
11 | oo | oo | oo | oo
12 | oo | oo | oo | oo
----------------------
4:41, 5:52
----------------------
11 | oo | oo | oo | oo
12 | oo | oo | oo | oo
----------------------
4:42, 5:51
----------------------
11 | oo | oo | oo | oo
12 | oo | oo | oo | oo
----------------------
4:42, 5:52
----------------------
11 | oo | oo | oo | oo
12 | oo | oo | oo | oo

header
header (layout) -> cartesianP values -> [[h11,h12,h11,h12], [h21,h21,h21,h21]]
labelh1 h11 h12 h11 h12
labelh2 h21 h21 h21 h21

variable

sections
section (layout) -> cartesianP values (X)
R.map(combination => {
  // draw dark blue line with combination
  R.map(row => {
    // row header (label)
    R.map(h => {
      row cross between header + combination + variable value
    })(header)
  })(values of variable)
})(X)








