import glamorous from 'glamorous';
import R from 'ramda';
import { Colors } from '../../';

export const DETable = glamorous.table({
  borderCollapse: 'collapse',
});

export const DEThead = glamorous.thead({
  backgroundColor: Colors.TABLE_BLUE_Y_BG,
  tableLayout: 'fixed',
});

export const DETh = glamorous.th({
  border: `1px solid ${Colors.GREY4}`,
  padding: '2px 5px',
  fontWeight: 'bold',
}, ({ isDimension }) => ({
  color: isDimension ? Colors.TABLE_BLUE_DF : Colors.TABLE_BLUE_VF,
  textAlign: isDimension ? 'right' : 'middle',
}));

export const DETd = glamorous.td({
  border: `1px solid ${Colors.GREY4}`,
  padding: '2px 5px',
}, ({ isDimension, isObservation }) => ({
  backgroundColor: isObservation ? Colors.WHITE1 : Colors.TABLE_BLUE_X_DBG,
  color: isDimension ? Colors.TABLE_BLUE_DF : isObservation ? null : Colors.TABLE_BLUE_VF,
  fontWeight: isDimension ? 'bold' : 'normal',
}));

export const DEZ = glamorous.td({
  padding: '2px 5px',
  border: `1px solid ${Colors.TABLE_BLUE_Z_BG}`,
  backgroundColor: Colors.TABLE_BLUE_Z_BG,
  color: Colors.TABLE_BLUE_Y_BG,
  fontWeight: 'bold',
  '& span': {
    color: Colors.WHITE1,
  },
});

export const DEN = glamorous.td({
  border: `1px solid ${Colors.GREY4}`,
  backgroundColor: Colors.GREY1,
  minWidth: 40,
});
