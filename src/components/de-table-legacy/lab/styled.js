import glamorous from 'glamorous';
import R from 'ramda';
import { Colors } from '../../';

const TABLE_MIN_CELL_WIDTH = 70;

//--------------------------------------------------------------------------------------------------
const Table = glamorous.div({
  display: 'grid',
  gridAutoRows: 'auto',
});

const Cell = glamorous.div({
  textAlign: 'right',
  padding: '2px 4px',
  minWidth: TABLE_MIN_CELL_WIDTH,
  borderBottom: `1px solid ${Colors.GREY2}`,
  borderLeft: `1px solid ${Colors.GREY2}`,
}, ({ hasRightBorder }) => ({
  borderRight: hasRightBorder ? `1px solid ${Colors.GREY2}` : null,
}));

const Header = glamorous(Cell)({
  fontWeight: 'bold',
  borderTop: `1px solid ${Colors.GREY2}`,
});

//--------------------------------------------------------------------------------------------------
export const ValueTable = glamorous(Table)({
  gridTemplateColumns: `repeat(1, minmax(min-content, max-content))`,
});

export const ValueCell = glamorous(Cell)({
  borderRight: `1px solid ${Colors.GREY2}`,
});

export const ValueHeader = glamorous(Header)({
  color: Colors.TABLE_X_FONT_HEADER,
  backgroundColor: Colors.TABLE_X_BACKGROUND,
  borderRight: `1px solid ${Colors.GREY2}`,
});

//--------------------------------------------------------------------------------------------------
export const ValuesTable = glamorous(Table)({
}, ({ columnCount = 2 }) => ({
  gridTemplateColumns: `repeat(${columnCount}, minmax(min-content, max-content))`,
}));

export const ValuesCell = glamorous(Cell)({
}, ({ isDimension, isBoundary }) => ({
  textAlign: isDimension ? 'left' : null,
  fontWeight: isDimension ? 'bold' : null,
  color: isDimension ? Colors.TABLE_X_FONT_HEADER_VALUE : null,
  backgroundColor: isDimension
    ? Colors.TABLE_X_BACKGROUND_HEADER
    : isBoundary ? Colors.TABLE_X_BACKGROUND_BOUNDARY : Colors.TABLE_X_BACKGROUND,
}));

export const ValuesHeader = glamorous(Header)({
  color: Colors.TABLE_X_FONT_HEADER,
}, ({ isDimension }) => ({
  textAlign: isDimension ? 'left' : null,
  backgroundColor: isDimension ? Colors.TABLE_X_BACKGROUND_HEADER : Colors.TABLE_X_BACKGROUND,
}));

export const ValuesSection = glamorous(Header)({
  gridColumn: 'span 2',
  backgroundColor: Colors.TABLE_Z_BACKGROUND,
  color: Colors.TABLE_Z_FONT,
  textAlign: 'left',
  border: 'none',
});

export const ValuesSectionCell = glamorous.span({
  padding: '0 2px',
}, ({ isDimension }) => ({
  color: isDimension ? Colors.TABLE_Z_FONT_HEADER : null,
}));

//--------------------------------------------------------------------------------------------------
export const Frozen = glamorous.div({
  position: 'fixed',
  top: 0,
});

export const FrozenHeader = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
});
