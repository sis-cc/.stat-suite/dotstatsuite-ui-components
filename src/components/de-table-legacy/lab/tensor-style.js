import glamorous from 'glamorous';
import { isNil, times, join, always } from 'ramda';

const Cell = glamorous.div({
  padding: '4px 8px',
}, ({ hasBorderRight, hasBorderBottom, hasNoBorderTop, spanCount, yOffset, theme }) => ({
  borderTop: hasNoBorderTop ? null : `1px solid ${theme.table.border}`,
  borderLeft: `1px solid ${theme.table.border}`,
  borderRight: hasBorderRight ? `1px solid ${theme.table.border}` : null,
  borderBottom: hasBorderBottom ? `1px solid ${theme.table.border}` : null,
  '@supports (display: grid)': {
    gridColumn: `span ${spanCount}`,
  },
  '@supports (position: sticky)': {
    position: isNil(yOffset) ? null : 'sticky',
    top: yOffset,
  },
}));

export const Table = glamorous.div({
  display: '-ms-grid',
  '@supports (display: grid)': {
    display: 'grid',
  },
  gridAutoRows: 'auto',
  msGridAutoFlow: 'row',
}, ({ columnCount }) => ({
  msGridColumns: join(' ', times(always('auto'), columnCount)),
  gridTemplateColumns: `repeat(${columnCount}, auto)`,
}));

export const YCell = glamorous(Cell)({}, ({ isDimension, theme }) => ({
  color: isDimension ? theme.table.yFontHeader : theme.table.yFont,
  textAlign: isDimension ? 'right' : 'center',
  backgroundColor: theme.table.yBg,
}));

export const XCell = glamorous(Cell)({}, ({ isDimension, theme }) => ({
  color: isDimension ? theme.table.xFontHeader : theme.table.xFont,
  backgroundColor: isDimension ? theme.table.xBgHeader : theme.table.xBg,
  textAlign: isDimension ? 'right' : null,
  '&:hover': {
    backgroundColor: isDimension ? theme.table.xBgHeader : theme.table.xBgHeader,
  },
}));

export const ZCell = glamorous(Cell)({
  textAlign: 'left',
  borderTop: 'none',
  borderLeft: 'none',
  borderRight: 'none',
}, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
}));

export const ZAtom = glamorous.div({}, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
}));

export const ZQuant = glamorous.span({
  fontFamily: 'Segoe UI Symbol',
}, ({ isDimension, isValue, theme }) => ({
  padding: isValue || isDimension ? 0 : 5,
  color: isDimension ? theme.table.zFontHeader : theme.table.zFont,
}));

export const SCell = glamorous(Cell)({}, ({ isDimension, theme }) => ({
  backgroundColor: theme.table.sBg,
  paddingLeft: isDimension ? 20 : null,
}));
