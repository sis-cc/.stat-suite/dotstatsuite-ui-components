import React, { Fragment, Component, createRef } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes, Icon, NonIdealState } from '@blueprintjs/core';
import cx from 'classnames';
import R from 'ramda';
import { compose, setPropTypes, mapProps, withProps, branch, renderComponent } from 'recompose';
import * as pretty from './pretty';
import { size } from '../../utils';

export const Blank = ({ blankLabel }) => <NonIdealState visual="circle" description={blankLabel} />;

export const Value = ({ valueLabel, observations, xValues }) => (
  <pretty.DETable>
    <tbody>
      <tr>
        <pretty.DETd isDimension>
          {valueLabel}
        </pretty.DETd>
      </tr>
      <tr>
        <pretty.DETd isObservation>
          { R.pipe(R.values, R.head, R.prop('value'))(observations) }
        </pretty.DETd>
      </tr>
    </tbody>
  </pretty.DETable>
);

export const Values = ({ xLabel, xValues, valueLabel }) => (
  <pretty.DETable>
    <tbody>
      <tr>
        <pretty.DETd isDimension>{xLabel}</pretty.DETd>
        <pretty.DETd>{valueLabel}</pretty.DETd>
      </tr>
      {
        R.map(
          ({ id, label }) => (
            <tr key={id}>
              <pretty.DETd>{label}</pretty.DETd>
              <pretty.DETd isObservation>{`o(${id})`}</pretty.DETd>
            </tr>
          )
        )(xValues)
      }
    </tbody>
  </pretty.DETable>
);

class Table extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = (event) =>  {
    const node = findDOMNode(this);
    console.log('the scroll things', window.pageXOffset)
  }

  render() {
    const { observations, dimensions, layout, yCartesian, zCartesian, valueLabel } = this.props;

    // conditional stuff
    const hasBoundaries = true;
    const yColSpan = hasBoundaries ? 2 : 1;

    // shortcuts
    const x = R.prop('x')(layout);
    const o = values => i => R.pipe(R.prop(i), R.pluck('id'), R.join(':'))(values);
    const c = d => t => i => R.isEmpty(d) ? null : `${t}(${o(d)(i)})`;

    // partials
    const xRows = i => R.map(
      ({id, label}) => (
        <tr key={id}>
          <pretty.DETd>{label}</pretty.DETd>
          { hasBoundaries && !R.isEmpty(yCartesian) ? <pretty.DEN /> : null }
          {
            R.times(
              n => (
                <pretty.DETd isObservation key={`${id}:${n}`}>
                  {
                    R.pipe(
                      R.reject(R.isNil),
                      R.join(' - ')
                    )([`x(${id})`, c(yCartesian)('y')(n), c(zCartesian)('z')(i)])
                  }
                </pretty.DETd>
              )
            )(R.length(yCartesian) || 1)
          }
        </tr>
      )
    )(R.path([x, 'values'])(dimensions));

    return (
      <Fragment>
        <div style={{
          height: 93,
          overflow: 'hidden',
          position: 'fixed',
          //top: isFixed ? 0 : null,
          //visibility: isFixed ? 'visible' : 'hidden',
        }}>
          <pretty.DETable>
            { R.isEmpty(yCartesian)
              ? null
              : <pretty.DEThead>
                  {
                    R.addIndex(R.map)(
                      (dimensionId, i) => (
                        <tr key={dimensionId}>
                          <pretty.DETh isDimension colSpan={yColSpan}>
                            {R.path([dimensionId, 'label'])(dimensions)}
                          </pretty.DETh>
                          {
                            R.addIndex(R.map)(
                              ({id, label}, j) => <pretty.DETh key={`${i}:${j}:${id}`}>{label}</pretty.DETh>
                            )(R.prop(i)(R.transpose(yCartesian)))
                          }
                        </tr>
                      )
                    )(R.prop('y')(layout))
                  }
                </pretty.DEThead>
            }
            <tbody>
              { hasBoundaries
                ? <tr>
                    <pretty.DETd isDimension>{R.pipe(R.path([x, 'label']))(dimensions)}</pretty.DETd>
                    { R.isEmpty(yCartesian) ? <pretty.DETd>{valueLabel}</pretty.DETd> : <pretty.DEN /> }
                    { R.times(n => <pretty.DEN key={`boundary:${n}`} />)(R.length(yCartesian)) }
                  </tr>
                : null
              }
              {
                R.addIndex(R.map)(
                  (z, i) => (
                    <Fragment>
                      <tr>
                        <pretty.DEZ colSpan={R.length(yCartesian) + yColSpan}>
                          {
                            R.addIndex(R.map)(
                              ({ id, label }, j) => (
                                <Fragment>
                                  { j !== 0 ? <Icon iconName="dot" /> : null }
                                  {R.path([R.path(['z', j])(layout), 'label'])(dimensions)}:
                                  <span>{label}</span>
                                </Fragment>
                              )
                            )(z)
                          }
                        </pretty.DEZ>
                      </tr>
                      { xRows(i) }
                    </Fragment>
                  )
                )([R.head(zCartesian)])
              }
            </tbody>
          </pretty.DETable>
        </div>
        <pretty.DETable ref="test">
          { R.isEmpty(yCartesian)
            ? null
            : <pretty.DEThead>
                {
                  R.addIndex(R.map)(
                    (dimensionId, i) => (
                      <tr key={dimensionId}>
                        <pretty.DETh isDimension colSpan={yColSpan}>
                          {R.path([dimensionId, 'label'])(dimensions)}
                        </pretty.DETh>
                        {
                          R.addIndex(R.map)(
                            ({id, label}, j) => <pretty.DETh key={`${i}:${j}:${id}`}>{label}</pretty.DETh>
                          )(R.prop(i)(R.transpose(yCartesian)))
                        }
                      </tr>
                    )
                  )(R.prop('y')(layout))
                }
              </pretty.DEThead>
          }
          <tbody>
            { hasBoundaries
              ? <tr>
                  <pretty.DETd isDimension>{R.pipe(R.path([x, 'label']))(dimensions)}</pretty.DETd>
                  { R.isEmpty(yCartesian) ? <pretty.DETd>{valueLabel}</pretty.DETd> : <pretty.DEN /> }
                  { R.times(n => <pretty.DEN key={`boundary:${n}`} />)(R.length(yCartesian)) }
                </tr>
              : null
            }
            {
              R.addIndex(R.map)(
                (z, i) => (
                  <Fragment>
                    <tr>
                      <pretty.DEZ colSpan={R.length(yCartesian) + yColSpan}>
                        {
                          R.addIndex(R.map)(
                            ({ id, label }, j) => (
                              <Fragment>
                                { j !== 0 ? <Icon iconName="dot" /> : null }
                                {R.path([R.path(['z', j])(layout), 'label'])(dimensions)}:
                                <span>{label}</span>
                              </Fragment>
                            )
                          )(z)
                        }
                      </pretty.DEZ>
                    </tr>
                    { xRows(i) }
                  </Fragment>
                )
              )(zCartesian)
            }
            { R.isEmpty(zCartesian) ? xRows() : null }
          </tbody>
        </pretty.DETable>
      </Fragment>
    );
  }
}

export const DETable = compose(
  setPropTypes({
    observations: PropTypes.object,
    dimensions: PropTypes.object,
    layout: PropTypes.object,
    blankLabel: PropTypes.string,
    valueLabel: PropTypes.string,
  }),
  branch(({ observations }) => R.isEmpty(observations), renderComponent(Blank)),
  branch(({ observations }) => R.equals(1, size(observations)), renderComponent(Value)),
  withProps(({ dimensions, layout }) => {
    const d = R.prop(R.prop('x')(layout))(dimensions);
    return { xId: R.prop('id')(d), xLabel: R.prop('label')(d), xValues: R.prop('values')(d) || [] };
  }),
  branch(({ xId, xValues }) => R.or(R.isNil(xId), R.isEmpty(xValues)), renderComponent(Blank)),
  withProps(({ dimensions, layout }) => {
    const cartesianProduct = R.reduce(R.pipe(R.xprod, R.map(R.unnest)), [[]]); // gandalf level...
    const values = type => R.pipe(R.pick(R.prop(type)(layout) || []), R.values, R.pluck('values'))(dimensions);
    const compact = R.reject(R.isEmpty);
    const engine = R.pipe(values, cartesianProduct, compact);
    return { yCartesian: engine('y'), zCartesian: engine('z') };
  }),
  branch(
    ({ yCartesian, zCartesian }) => R.and(R.isEmpty(yCartesian), R.isEmpty(zCartesian)),
    renderComponent(Values)
  )
)(Table);
