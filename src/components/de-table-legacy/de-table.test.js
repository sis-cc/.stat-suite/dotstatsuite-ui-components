import React from 'react';
import R from 'ramda';
import { render, mount } from 'enzyme';
import Benchmark from 'react-component-benchmark';
import { DETable } from '.';
import * as U from './utils';
import { dimensions, layouts, extremeLayout } from './mocks';

describe('DE - table - layouts', () => {
  R.forEachObjIndexed((layout, label) => {
    it(`should render: ${label} layout`, () => {
      const component = render(<DETable dimensions={dimensions} layout={layout} />);
      expect(component).toMatchSnapshot();
    });
  })(layouts);
});

describe('DE - table - banchmarks', () => {
  let props;
  let meanTime;

  beforeEach(() => {
    meanTime = 0;
    props = {
      component: DETable,
      onComplete: jest.fn(results => meanTime = results.mean),
      samples: 10,
    };
  });

  R.forEachObjIndexed((layout, label) => {
    it(`should mount in less than 50ms: ${label} layout`, () => {
      const component = mount(
        <Benchmark {...props} componentProps={{dimensions, layout}} />
      );
      component.instance().start();
      expect(meanTime).toBeLessThan(10);
    });

    it(`should update in less than 50ms: ${label} layout`, () => {
      const component = mount(
        <Benchmark {...props} componentProps={{dimensions, layout}} type="update" />
      );
      component.instance().start();
      expect(meanTime).toBeLessThan(10);
    });
  })(layouts);

  it(`should mount in less than 100ms: extreme layout (1 million o)`, () => {
    const component = mount(
      <Benchmark {...props} componentProps={{dimensions, layout: extremeLayout}} />
    );
    component.instance().start();
    expect(meanTime).toBeLessThan(75);
  });

  it(`should update in less than 100ms: extreme layout (1 million o)`, () => {
    const component = mount(
      <Benchmark {...props} componentProps={{dimensions, layout: extremeLayout}} type="update" />
    );
    component.instance().start();
    expect(meanTime).toBeLessThan(75);
  });
});

describe('DE - table - utils', () => {
  it('should compute row count', () => {
    expect(U.computeRowCount(dimensions, { x: 'd1', y: ['d1', 'd2'], z: ['d2', 'd3'] })).toBe(39);
  });

  it('should compute column count (no value)', () => {
    expect(U.computeColumnCount(dimensions, { y: ['d0'] })).toBe(2);
  });
  it('should compute column count (normal)', () => {
    expect(U.computeColumnCount(dimensions, { y: ['d1'] })).toBe(10);
  });
  it('should compute column count (1 dim with no value)', () => {
    expect(U.computeColumnCount(dimensions, { y: ['d0', 'd1'] })).toBe(10);
  });
  it('should compute column count (2 dimensions)', () => {
    expect(U.computeColumnCount(dimensions, { y: ['d2', 'd3'] })).toBe(6);
  });
  it('should compute column count (3 dimensions)', () => {
    expect(U.computeColumnCount(dimensions, { y: ['d1', 'd2', 'd3'] })).toBe(34);
  });
});
