import R from 'ramda';

export const benchmarkDimension = (id, count) => ({
  id,
  label: id,
  values: R.times(n => ({ id: `${id}-${n}`, label: n, index: n }))(count),
});

export const dimensions = {
  'd0': {},
  'd1': {
    id: 'd1',
    label: 'Classification',
    values: [
      { id: '11', index: 0, label: 'New light vehicles' },
      { id: '12', index: 1, label: 'New heavy vehicles' },
      { id: '13', index: 2, label: 'New passenger car' },
      { id: '14', index: 3, label: 'New commercial vehicles' },
      { id: '15', index: 4, label: 'Old light vehicles' },
      { id: '16', index: 5, label: 'Old heavy vehicles' },
      { id: '17', index: 6, label: 'Old passenger car' },
      { id: '18', index: 7, label: 'Old commercial vehicles' },
    ],
  },
  'd2': {
    id: 'd2',
    label: 'Time period',
    values: [
      { id: '21', index: 0, label: '2013' },
      { id: '22', index: 1, label: '2014' },
    ],
  },
  'd3': {
    id: 'd3',
    label: 'Gender',
    values: [
      { id: '32', index: 22, label: 'Women' },
      { id: '31', index: 44, label: 'Men' },
    ],
  },
  'd4': {
    id: 'd4',
    label: 'Reference area',
    values: [
      { id: '42', index: 0, label: 'Tanzania' },
      { id: '43', index: 1, label: 'Nepal' },
    ],
  },
  'd5': {
    id: 'd5',
    label: 'Adjustment indicator',
    values: [
      { id: '51', index: 0, label: 'Working day' },
      { id: '52', index: 1, label: 'Season day' },
    ],
  },
  'd9': {
    id: 'd9',
    label: 'Talks',
    values: [
      { id: '92', index: 0, label: 'He travels the fastest who travels alone. Rudyard Kipling.' },
      { id: '94', index: 1, label: 'The limits of the possible can only be defined by going beyond them into the impossible. Arthur C Clarke.' },
    ],
  },
  'd10': benchmarkDimension('d10', 100),
  'd11': benchmarkDimension('d11', 10),
  'd12': benchmarkDimension('d12', 10),
  'd13': benchmarkDimension('d13', 100),
  'd14': benchmarkDimension('d14', 100),
  'd20': benchmarkDimension('d20', 7),
  'd21': benchmarkDimension('d21', 4),
  'd22': benchmarkDimension('d22', 3),
  'd23': benchmarkDimension('d23', 8),
  'd24': benchmarkDimension('d24', 5),
};

export const layouts = {
  '-/-/- (0o)':         {y:[],z:[]},
  '2v/-/- (2o)':        {x:'d3',y:[],z:[]},
  '2v/-/1d2v (4o)':     {x:'d3',y:[],z:['d9']},
  '2v/-/2d2v (8o)':     {x:'d3',y:[],z:['d5','d9']},
  '2v/1d2/- (4o)':      {x:'d3',y:['d2'],z:[]},
  '2v/1d2/1d2v (8o)':   {x:'d3',y:['d2'],z:['d9']},
  '2v/1d2/2d2v (16o)':  {x:'d3',y:['d2'],z:['d5','d9']},
  '2v/2d2v/- (8o)':     {x:'d3',y:['d2','d4'],z:[]},
  '2v/2d2v/1d2v (16o)': {x:'d3',y:['d2','d4'],z:['d9']},
  '2v/2d2v/2d2v (32o)': {x:'d3',y:['d2','d4'],z:['d5','d9']},
};

export const extremeLayout = {x:'d10',y:['d11','d12'],z:['d13','d14']};
