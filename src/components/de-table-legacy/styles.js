import glamorous from 'glamorous';
import { Icon } from '@blueprintjs/core';

const cellStyle = {
  height: '100%',
  padding: '4px 6px',
  fontSize: 16,
};

export const YCell = glamorous.div(cellStyle, ({ isValue, isYLast, theme }) => ({
  backgroundColor: theme.table.yBg,
  borderLeft: `1px solid ${theme.table.border}`,
  borderBottom: `1px solid ${theme.table.border}`,
  color: isValue ? theme.table.yFont : theme.table.yFontHeader,
  textAlign: isValue ? 'center' : 'right',
  borderRight: isYLast ? `1px solid ${theme.table.border}` : null,
}));

export const XCell = glamorous.div(cellStyle, ({ isValue, isXLast, hasNoZ, theme }) => ({
  backgroundColor: theme.table.xBgHeader,
  borderLeft: `1px solid ${theme.table.border}`,
  color: isValue ? theme.table.xFont : theme.table.xFontHeader,
  borderBottom: isValue && !isXLast || hasNoZ ? `1px solid ${theme.table.border}` : null,
}));

export const ZCell = glamorous.div(cellStyle, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
}));

export const ZAtom = glamorous.span({
}, ({ isValue, theme }) => ({
  paddingRight: isValue ? 0 : 5,
  color: isValue ? theme.table.zFont : theme.table.zFontHeader,
}));

export const ZSeparator = glamorous(Icon)({
  padding: '0 5px',
}, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
  color: theme.table.zFont,
}));

export const Separator = glamorous.div(cellStyle, ({ isValue, isYLast, isXLast, theme }) => ({
  borderBottom: isValue && !isXLast ? `1px solid ${theme.table.border}` : null,
  borderRight: isYLast ? `1px solid ${theme.table.border}` : null,
  backgroundColor: theme.table.sBg,
  borderLeft: `1px solid ${theme.table.border}`,
}));

export const Cell = glamorous.div({
  ...cellStyle,
  textAlign: 'right',
}, ({ isYLast, isXLast, theme }) => ({
  color: theme.table.oFont,
  borderRight: isYLast ? `1px solid ${theme.table.border}` : null,
  borderBottom: isXLast ? null : `1px solid ${theme.table.border}`,
  backgroundColor: theme.table.xBg,
  borderLeft: `1px solid ${theme.table.border}`,
}));
