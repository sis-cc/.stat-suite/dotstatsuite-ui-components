import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs/react';
import R from 'ramda';
import { Tag, Classes } from '@blueprintjs/core';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DETable, Formats, Themes } from '../../';
import { dimensions, layouts } from './mocks';
import Tensor from './lab/tensor';
import Tabulator from './tabulator';

export const dimensionGetters = {
  label: R.prop('label'),
  code: R.prop('id'),
  both: dimension => `${R.prop('id')(dimension)}:${R.prop('label')(dimension)}`,
};

export const observationGetters = {
  coords: R.pipe(
    R.toPairs,
    R.map(([id, index]) => (
      <Tag key={`${id}:${index}`} className={Classes.MINIMAL}>{id}: {index}</Tag>
    ))
  ),
  fakeValue: R.pipe(R.values, R.sum),
};
export const SBTable = () => {
  const _layouts = {
    ...layouts,
    'sdmx (500+)': {x:'d4',y:['d3','d22'],z:['d23','d24']},
    'sdmx (1000+)': {x:'d21',y:['d3','d22'],z:['d23','d24']},
    'sdmx (3000+)': {x:'d20',y:['d21','d22'],z:['d23','d24']},
  };
  const layoutOptions = R.keys({ ..._layouts });
  const layout = select(
    'layouts (table)',
    layoutOptions,
    R.nth(-4)(layoutOptions)
  );

  const dimensionGetterOptions = R.keys({ ...dimensionGetters });
  const dimensionGetter = select(
    'dimensionGetter (table)',
    dimensionGetterOptions,
    R.head(dimensionGetterOptions)
  );

  const observationGetterOptions = R.keys({ ...observationGetters });
  const observationGetter = select(
    'observationGetter (table)',
    observationGetterOptions,
    R.head(observationGetterOptions)
  );

  return (
    <DETable
      dimensions={dimensions}
      layout={R.prop(layout)({ ..._layouts })}
      observationGetter={R.prop(observationGetter)({ ...observationGetters })}
      dimensionGetter={R.prop(dimensionGetter)({ ...dimensionGetters })}
    />
  );
};

const stories = storiesOf('data-explorer|table-stuff|lab', module);
stories.addDecorator(withKnobs);
stories.add('virtualized', () => {
  const BoiledTable = storyWithBoil({ Formats, Themes })(SBTable);
  return <BoiledTable />;
});

stories.add('cssgrid', () => {
  const BoiledTable = storyWithBoil({ Formats, Themes })(() => <Tensor />);
  return <BoiledTable />;
});

stories.add('tabulator', () => {
  const BoiledTable = storyWithBoil({ Formats, Themes })(() => <Tabulator />);
  return <BoiledTable />;
});
