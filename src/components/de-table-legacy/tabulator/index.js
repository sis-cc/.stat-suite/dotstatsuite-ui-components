import React from 'react';
import { times, ifElse, always, equals } from 'ramda';
import 'react-tabulator/lib/styles.css';
import { ReactTabulator } from 'react-tabulator';

const Tabulator = ({ data, columns }) => (
  <ReactTabulator
  height={500}
    data={data}
    columns={columns}
    tooltips={true}
    layout={"fitColumns"}
  />
);

Tabulator.defaultProps = {
  data: times(
    ifElse(
      equals(10),
      always({
        id:1,
        name:"Billy Bob",
        age:"12",
        gender:"male",
        height:1,
        col:"red",
        dob:"As of version 3.0 tabulator renders its table using a Virtual DOM",
        cheese:1
      }),
      always({
        id:1,
        name:"Billy Bob",
        age:"12",
        gender:"male",
        height:1,
        col:"red",
        dob:"",
        cheese:1
      }),
    ),
    300,
  ),
  columns:[
    {title:"Name", field:"name"},
    {title:"Personal Info",columns:[
      {title:"Age", field:"age"},
      {title:"Gender", field:"gender"},
      {title:"Height", field:"height"},
    ]},
    {title:"Work Info",columns:[
      {title:"Favourite Color", field:"col"},
      {title:"Date Of Birth", field:"dob"},
      {title:"Cheese Preference", field:"cheese"},
    ]},
  ],
};

export default Tabulator;
