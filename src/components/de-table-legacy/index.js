import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import {
  WindowScroller, List, Grid, AutoSizer, ColumnSizer, CellMeasurer, CellMeasurerCache,
} from 'react-virtualized';
import throttle from 'react-throttle-render';
import { NonIdealState } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { Colors } from '../../';
import * as U from './utils';
import * as S from './styles';

const cache = new CellMeasurerCache({ fixedWidth: true });

const myCellRenderer = ({
  columnCount, dimensions, layout, columnWidth,
  yCartesian, zCartesian, valueLabel, observationGetter, dimensionGetter,
}) => ({ columnIndex, key, rowIndex, parent, style }) => {
  let width = columnWidth;
  let type;
  let left = style.left;
  const SEPARATOR_WIDTH = 30;
  const separatorDelta = columnWidth-SEPARATOR_WIDTH;
  const x = R.prop('x')(layout);
  const xDimension = R.prop(x)(dimensions);
  const ySize = R.pipe(R.prop('y'), R.length)(layout);
  const hasNoY = R.equals(0, ySize);
  const hasNoZ = R.pipe(R.prop('z'), R.length, R.equals(0))(layout);
  const PREFIX_Y = hasNoY ? 1 : 2;
  const yTransposedCartesian = R.transpose(yCartesian);
  const isYLast = R.equals(columnIndex-PREFIX_Y+1, R.length(yTransposedCartesian));
  if (R.lt(rowIndex, ySize)) {
    if (R.lt(columnIndex, PREFIX_Y)) {
      if (R.equals(columnIndex, 0)) {
        width = 2*columnWidth;
        const yDimensionId = R.pipe(
          R.prop('y'),
          R.nth(rowIndex)
        )(layout);
        const yLabel = R.pipe(
          R.prop(yDimensionId),
          dimensionGetter
        )(dimensions);
        type = <S.YCell>{yLabel}</S.YCell>;
      } else {
        type = null;
      }
    } else {
      const yValue = R.pipe(
        R.nth(rowIndex),
        R.nth(columnIndex-PREFIX_Y),
        dimensionGetter
      )(yCartesian);
      type = <S.YCell isValue isYLast={isYLast}>{yValue}</S.YCell>;
    }
  } else if (R.equals(rowIndex, ySize)) {
    if (R.equals(columnIndex, 0)) {
      width = hasNoY ? columnWidth : columnWidth+separatorDelta;
      const xLabel = dimensionGetter(xDimension);
      type = <S.XCell hasNoZ={hasNoZ}>{xLabel}</S.XCell>;
    } else if (hasNoY) {
      type = <S.Cell isXLast={!hasNoZ || !hasNoY} isYLast>{valueLabel}</S.Cell>;
    } else {
      const isValue = R.equals(columnIndex, 1);
      width = isValue ? SEPARATOR_WIDTH : width;
      left = isValue ? left+separatorDelta : left;
      type = <S.Separator isYLast={isYLast} isValue={hasNoZ} />;
    }
  } else {
    const xValues = R.prop('values')(xDimension);
    const xSize = R.length(xValues);
    const ZPREFIX = hasNoZ ? 0 : 1;
    const xRowIndex = rowIndex-(ySize+ZPREFIX);
    const xIndex = xRowIndex%(xSize+ZPREFIX);
    const isXLast = R.equals(xIndex, xSize);
    const zRowIndex = Math.floor(R.divide(xRowIndex, xSize+1));
    if (R.equals(xIndex, 0) && !hasNoZ) {
      if (R.equals(columnIndex, 0)) {
        width = columnCount*columnWidth;
        const z = R.prop('z')(layout);
        const zLabelsAndValues = R.pipe(
          R.nth(zRowIndex),
          R.addIndex(R.map)((zValue, index) => ([
            R.pipe(R.prop(R.nth(index)(z)), dimensionGetter)(dimensions),
            dimensionGetter(zValue)
          ]))
        )(zCartesian);
        type = (
          <S.ZCell>
            { R.addIndex(R.map)(([ label, value ], index) => (
                <Fragment key={`${label}${value}`}>
                  { index === 0 ? null : <S.ZSeparator iconName="dot" /> }
                  <S.ZAtom>{label}:</S.ZAtom>
                  <S.ZAtom isValue>{value}</S.ZAtom>
                </Fragment>
              ))(zLabelsAndValues)
            }
          </S.ZCell>
        );
      } else {
        type = null;
      }
    } else if (R.lt(columnIndex, PREFIX_Y)) {
      if (R.equals(columnIndex, 0)) {
        const xValue = R.pipe(
          R.nth(xIndex-1),
          dimensionGetter
        )(xValues);
        width = hasNoY ? columnWidth : columnWidth+separatorDelta;
        type = <S.XCell isValue isXLast={isXLast}>{xValue}</S.XCell>;
      } else {
        width = SEPARATOR_WIDTH;
        left = left+separatorDelta;
        type = <S.Separator isValue isXLast={isXLast} />;
      }
    } else {
      const xCoord = R.pipe(
        R.nth(hasNoZ ? xRowIndex-1 : xIndex-1),
        R.prop('index'),
      )(xValues);
      const yIndexes = hasNoY
        ? []
        : R.pipe(R.nth(columnIndex-PREFIX_Y), R.pluck('index'))(yTransposedCartesian);
      const zIndexes = hasNoZ
        ? []
        : R.pipe(R.nth(zRowIndex), R.pluck('index'))(zCartesian);

      const coords = (c, ix) => R.addIndex(R.reduce)(
        (m, id, i) => ({ ...m, [id]: R.nth(i)(ix) }),
        {}
      )(R.prop(c)(layout));
      const coordinates = {
        [R.prop('id')(xDimension)]: xCoord,
        ...coords('y', yIndexes),
        ...coords('z', zIndexes),
      };
      type = (
        <S.Cell isYLast={isYLast || hasNoY} isXLast={isXLast}>
          {observationGetter(coordinates)}
        </S.Cell>
      );
    }
  }

  return (
    <CellMeasurer 
      cache={cache}
      columnIndex={columnIndex}
      key={key}
      parent={parent}
      rowIndex={rowIndex}
    >
      <div style={{ ...style, width, left }}>
        {type}
      </div>
    </CellMeasurer>
  );
};

const StyledGrid = glamorous(Grid)({
  borderTop: `1px solid ${Colors.GREY4}`,
  outline: 'none',
}, ({ hasNoZ }) => ({
  borderBottom: hasNoZ ? `1px solid ${Colors.GREY4}` : null,
}));

const StyledNonIdealState = glamorous(NonIdealState)({
  justifyContent: 'flex-start !important',
});

class MyGrid extends React.Component {
  componentWillUpdate(nextProps) {
    const shouldClearCache = R.anyPass([
      () => nextProps.columnCount !== this.props.columnCount,
      () => nextProps.columnWidth !== this.props.columnWidth,
    ])();

    if (shouldClearCache) cache.clearAll();
  }

  render () {
    return (
      <StyledGrid
        {...this.props}
        deferredMeasurementCache={cache}
        rowHeight={cache.rowHeight}
      />
    )
  }
};

const ThrottledGrid = throttle(50)(MyGrid);

export const DETable = ({
  observationGetter, dimensionGetter, dimensions, layout, blankLabel, valueLabel,
}) => {
  const rowCount = U.computeRowCount(dimensions, layout);
  const columnCount = U.computeColumnCount(dimensions, layout);

  // TODO: rewrite properly (R.way)
  const values = type => R.pipe(
    R.pick(R.prop(type)(layout) || []),
    R.values,
    R.pluck('values')
  )(dimensions);
  const cartesian = R.pipe(values, U.cartesian, U.compact);
  const yCartesian = R.transpose(cartesian('y'));
  const zCartesian = cartesian('z');

  const hasNoZ = R.pipe(R.prop('z'), R.length, R.equals(0))(layout);
  const hasNoX = !R.propOr(undefined, 'x')(layout);

  if (hasNoX) return <StyledNonIdealState visual="circle" description={blankLabel} />;

  return (
    <WindowScroller>
      {({ height, scrollTop }) => (
        <AutoSizer disableHeight>
          {({ width }) => {
            if (R.equals(width, 0)) return;

            const fitWidth = R.pipe(
              R.flip(R.modulo)(columnCount),
              R.subtract(width)
            )(width);
            const columnWidth = R.divide(fitWidth)(columnCount);
            const commonProps = {
              width: fitWidth,
              height,
              columnCount,
              columnWidth,
              autoHeight: true,
            };
            const cellRenderer = myCellRenderer({
              columnCount, dimensions, layout, columnWidth,
              yCartesian, zCartesian, valueLabel, observationGetter, dimensionGetter,
            });

            return (
              <Fragment>
                <ThrottledGrid hasNoZ={!hasNoZ}
                  {...commonProps}
                  scrollTop={scrollTop}
                  rowCount={rowCount}
                  cellRenderer={cellRenderer}
                />
                { scrollTop > 0
                  ? <div style={{ position: 'fixed', top: 0 }}>
                      <ThrottledGrid hasNoZ={!hasNoZ}
                        {...commonProps}
                        rowCount={R.length(yCartesian)+1}
                        cellRenderer={cellRenderer}
                      />
                    </div>
                  : null
                }
              </Fragment>
            );
          }}
        </AutoSizer>
      )}
    </WindowScroller>
  );
};

DETable.propTypes = {
  observationGetter: PropTypes.func,
  dimensionGetter: PropTypes.func,
  dimensions: PropTypes.object,
  layout: PropTypes.object,
  blankLabel: PropTypes.string,
  valueLabel: PropTypes.string,
};

DETable.defaultProps = {
  blankLabel: 'Nothing to display',
  valueLabel: 'Value',
  observationGetter: R.always('observationGetter not set'),
  dimensionGetter: R.prop('label'),
};
