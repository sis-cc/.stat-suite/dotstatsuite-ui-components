import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXList, Formats, Colors, FontSizes, Themes } from '../../';

const Element = ({ label, itemsHandlers }) => (
  <div
    key={label.id}
    style={{
      width: '100%',
      height: 30,
      margin: '5px 0',
      backgroundColor: Colors.BLUE1,
      color: Colors.WHITE1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: FontSizes.SIZE2,
    }}>
    <div onClick={() => itemsHandlers.itemHandler(label)}>{label}</div>
  </div>
);

const items = [{ label: 'item 1', id: 'item 1' }, { label: 'item 1', id: 'item 2' }, { label: 'item 1', id: 'item 3' }];

export const SBList = boilers => (
  <VXList
    items={boolean('isBlank (list)', false) ? [] : items}
    isLoading={boolean('isLoading (list)', false)}
    itemRenderer={Element}
    itemsHandlers={{ itemHandler: action('itemHandler') }}
    blankLabel="blankLabel"
    loadingLabel="loadingLabel"
    statusLabel="status"
    {...boilers}
  />
);

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('list', () => {
  const BoiledList = storyWithBoil({ Formats, Themes })(SBList);
  return <BoiledList />;
});
