import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { VXList, VXListBlank, Themes, Colors, FontSizes } from '../../';

const Element = ({ label, itemsHandlers }) => (
  <div
    key={label.id}
    style={{
      width: '100%',
      height: 30,
      margin: '5px 0',
      backgroundColor: Colors.BLUE1,
      color: Colors.WHITE1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: FontSizes.SIZE2,
    }}>
    <div onClick={() => itemsHandlers.itemHandler(label)}>{label}</div>
  </div>
);

const items = [{ label: 'item 1', id: 'item 1' }, { label: 'item 1', id: 'item 2' }, { label: 'item 1', id: 'item 3' }];

describe('VX - list', () => {
  const props = { 
    items: items,
    isLoading: false,
    itemRenderer: Element,
    itemsHandlers: { itemHandler: () => {} },
    blankLabel: "blankLabel",
    loadingLabel: "loadingLabel",
    statusLabel: "status",
  }
  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXList {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<VXList {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot itemRenderer null', () => {
    const component = renderWithGoogles(<VXList {...props} itemRenderer={null} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isLoading', () => {
    const component = renderWithGoogles(<VXList {...props} isLoading={true} isRtl />);
    expect(component).toMatchSnapshot();
  });  
});

describe('VXListBlank ', () => {
  it('should match snapshot isLoading', () => {
    const component = renderWithGoogles(<VXListBlank blankLabel="blankLabel" />);
    expect(component).toMatchSnapshot();
  });
});