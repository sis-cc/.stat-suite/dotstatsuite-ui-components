import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'ramda';
import glamorous from 'glamorous';
import { NonIdealState, Spinner } from '@blueprintjs/core';
import { compose } from 'recompose';
import { withListBlank, withListLoading } from '../../';

const Wrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  paddingTop: 20,
});

export const VXListBlank = ({ blankLabel }) => (
  <Wrapper>
    <NonIdealState visual="circle" description={blankLabel} />
  </Wrapper>
);

VXListBlank.propTypes = {
  blankLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
};

const StyledSpinner = glamorous(Spinner)({
  paddingBottom: 20,
});

export const VXListLoading = ({ loadingLabel }) => (
  <Wrapper>
    <StyledSpinner />
    {loadingLabel}
  </Wrapper>
);

VXListLoading.propTypes = {
  loadingLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
};

const StyledList = glamorous.div(
  {},
  ({ isRtl }) => ({
    direction: isRtl ? 'rtl' : 'ltr',
  }),
);

const ListWrapper = glamorous.div({
  marginBottom: 20,
}, ({ theme}) => ({
  borderTop: theme.list.borderTop,
}));

export const List = ({
  items, itemRenderer, statusLabel, isRtl, isNarrow, ...rest
}) => (
  <StyledList isRtl={isRtl}>
    {statusLabel}
    <ListWrapper isNarrow={isNarrow}>
      {
        map((item) => {
          const Renderer = item.renderer || itemRenderer;
          if (!Renderer) {
            console.log('no renderer found', 'item.id', item.id);
            return null;
          }
          return <Renderer key={item.id} {...item} {...rest} isRtl={isRtl} isNarrow={isNarrow} />;
        })(items)
      }
    </ListWrapper>
  </StyledList>
);

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    renderer: PropTypes.func,
  })),
  itemRenderer: PropTypes.func,
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  statusLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  isRtl: PropTypes.bool,
};

List.defaultProps = {
  items: [],
};

export const VXList = compose(
  withListLoading(VXListLoading),
  withListBlank(VXListBlank),
)(List);

