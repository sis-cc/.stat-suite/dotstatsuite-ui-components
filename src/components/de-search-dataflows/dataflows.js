//3 highlights, undefined for dimension
export const d1 = [
{
  actions: [{
    icon: 'import',
    id: 'download',
    label: 'download',
    action: () => console.log('OECD:POPULATION(1.0)'),
  }],
  description : "Population broken down by country and sex for various indicators.",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"OECD:POPULATION(1.0)",
  index:1,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Demography and population", "Population"],
  matches:[
    {
      'match-text':"Population broken down by country and sex for <mark>various</mark> <mark>indicators</mark>. (<mark>POPULATION</mark>)",
      "concept": null,
      type:"dataflow",
    },
    {
      'match-text':"<mark>Population</mark> broken down by country and sex for various indicators.",
      "concept": null,
      type:"description",
    },
    {
      type: "topic",
      "concept": null,
      'match-text': "Demography and <mark>population</mark>",
    },
    {
      'match-text':"<mark>Population</mark> (hist5)  00-04, persons (YP9901L1) Population (hist5)  05-09, persons (YP9902L1) ..." ,
      "concept": 'Code',
      type:"code",
    },
  ],
  title:"Population broken down by country and sex for various indicators. (POPULATION)",
},
{
  description : "",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"TN1:DEMO_POP_EST2(1.0)",
  index:2,
  lastUpdated:"2017-12-08 15:52:27",
  matches:[
    {
      'match-text':"Estimated <mark>population</mark> on 1st July (with fixed hierarchy in REF_AREA) (DEMO_POP_EST2)",
      type:"dataflow",
    },
    {
      type: "topic",
      'match-text': "Demography and <mark>population</mark>",
    },
  ],
  title:"Estimated population on 1st July (with fixed hierarchy in REF_AREA) (DEMO_POP_EST2)",
},
{
  description : "",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"SPC:DF_Ton_pop5(1.0)",
  index:3,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Demography and population", "Population"],
  matches:[
    {
      'match-text':"<mark>Population</mark> by sex, 5 years age groups, Block Village District Island and Country (DF_Ton_pop5)",
      type:"dataflow",
      "concept": null,
    },
    {
      type: "topic",
      'match-text': "Demography and <mark>population</mark>",
      "concept": null,
    },
  ],
  title:"Population by sex, 5 years age groups, Block Village District Island and Country (DF_Ton_pop5)",
},
{
  description : "Income distribution Poverty rates Source: Wolrd Bank - World Development Indicators (WDI)",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"WB:WDI_POVERTY(1.0)",
  index:4,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Economy and finance"],
  matches:[
    {
      'match-text':"Income share held by lowest 20% (SI_DST_FRST_20) Poverty headcount ratio at $2 a day (PPP) (% of <mark>population</mark>) (SI_POV_2DAY) Poverty headcount ratio at $1_25 a day (PPP) (% of population) (SI_POV_DDAY)",
      type:"code",
      concept:"code to be highlighted",
    },
    {
      'match-text':"Income share held by lowest 20% (SI_DST_FRST_20) Poverty headcount ratio at $2 a day (PPP) (% of <mark>population</mark>) (SI_POV_2DAY) Poverty headcount ratio at $1_25 a day (PPP) (% of population) (SI_POV_DDAY)",
      type:"code",
      concept:"Series",
    },
  ],
  title:"Poverty (WDI_POVERTY)",
},
{
  description : "",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"TN1:DEMO_POP_INS(1.0)",
  index:5,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Demography and population", "Population"],
  matches:[
    {
      'match-text':"Demography and <mark>population</mark>",
      type:"topic",
      "concept": null,
    },
    {
      'match-text':"Distribution of the <mark>population</mark> (IND_DEM_DP) ",
      type:"code",
      "concept": "Code",
    },
  ],
  title:"Demographic indicators (DEMO_POP_INS)",
},
{
  description : "",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"TN2:DEMO_POP_INS(1.0)",
  index:6,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Demography and population", "Population"],
  matches:[
    {
      'match-text': 'Source of laundry price <mark>statistics</mark> (RPP_SOURCE)',
      type: 'dimension',
      "concept": null,
    },
    {
      'match-text': 'Source of property price <mark>statistics</mark> (RPP_SOURCE)',
      type: 'dimension',
      "concept": null,
    },
  ],
  title:"Demographic indicators (DEMO_POP_INS)",
},
{
  "id":"MA_3:AGRI_CROPS_S(1.0)",
  "title":null,
  "description":null,
  "lastUpdated":"2017-11-30 09:09:04",
  "topics":["Agriculture et pêche"],
  "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S,1.0/....",
  "href":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S,1.0/....",
  "index":2,
  "matches":[
    {
      "type":"topic",
      "concept": null,
      "match-text":"Agriculture et <mark>pêche</mark>",
    }
  ]
},
{
  "id":"MA_3:AGRI_CROPS_S2(1.0)",
  "title":"Agricultural land by crop type (simplified, with data) (AGRI_CROPS_S2)",
  "description":"",
  "lastUpdated":"2017-11-30 10:38:48",
  "topics":["Agriculture and fisheries"],
  "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S2,1.0/....",
  "href":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S2,1.0/....",
  "index":1,
  "matches":[
    {
      "type":"dataflow",
      "concept": null,
      "match-text":"Agricultural land by crop type (simplified, with <mark>data</mark>) (AGRI_CROPS_S2)",
    },
  ]
},
{
  "id":"WB:WDI_POVERTY(2.0)",
  "title":"Poverty (WDI_POVERTY)",
  "description":"Income distribution Poverty rates Source: Wolrd Bank - World Development Indicators (WDI)",
  "lastUpdated":"2017-12-08 15:52:27",
  "topics":["Economy and finance"],
  "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/WB,WDI_POVERTY,1.0/..",
  "href":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/WB,WDI_POVERTY,1.0/..",
  "index":2,
  "matches":[
    {
      "type":"dimension",
      "concept": null,
      "match-text":"Series (SERIES), The phenomenon or phenomena to be measured in the <mark>data</mark> set. The word SERIES is used for consistency with the term used in the MDG Database. SERIES are all the official MDG series and background series currently in the MDG Database",
    }
  ]
},
{
  "id":"MA_3:AGRI_CROPS(1.0)",
  "title":"Agricultural land by crop type (AGRI_CROPS)",
  "description":"","lastUpdated":"2017-11-29 16:39:28",
  "topics":["Agriculture and fisheries","Economy and finance"],
  "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS,1.0/.....",
  "href":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS,1.0/.....",
  "index":3,
  "matches":[
    {
      "type":"dimension",
      "concept": null,
      "match-text":"Indicator (INDICATOR), The phenomenon or phenomena to be measured in the <mark>data</mark> set. The word INDICATOR is used for consistency with the term used in the DevInfo."
    }
  ]
}
];

export const d2 = [
{
  description : "Population broken down by country and sex for various indicators.",
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id:"OECD:POPULATION(1.0)",
  index:1,
  lastUpdated:"2017-12-08 15:52:27",
  topics: ["Demography and population", "Population"],
  matches:[
    {
      'match-text':"Population broken down by country and sex for <mark>various</mark> <mark>indicators</mark>. (<mark>POPULATION</mark>)",
      type:"description",
      "concept": null,
    },
    {
      'match-text':"Population broken down by country and sex for <mark>various</mark> <mark>indicators</mark>. (<mark>POPULATION</mark>)",
      "concept": null,
      type:"dataflow",
    },
  ],
  title:"Population broken down by country and sex for various indicators. (POPULATION)",
},
];

export const d3 = [
    {
      "id":"MA_3:AGRI_CROPS_S2(1.0)",
      "title": "asjdhkasjajkdhfuiefhnsmdfksfiashdfskfnsdkfhsdkfhals",
      "description":null,
      "lastUpdated":"2017-11-30 10:38:48",
      "topics":["Agriculture et pêche"],
      "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S2,1.0/....",
      "index":1,
      "matches":[
        {
          "type":"topic",
          "concept":"",
          "match-text": "Agriculture et <mark>pêche</mark>",
        }
      ],
    },
    {
      "id":"MA_3:AGRI_CROPS_S(1.0)",
      "title":null,
      "description":null,
      "lastUpdated":"2017-11-30 09:09:04",
      "topics":["Agriculture et pêche"],
      "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS_S,1.0/....",
      "index":2,
      "matches":[
        {
          "type":"topic",
          "concept":"",
          "match-text":"Agriculture et <mark>pêche</mark>",
        }
      ]
    },
    {
        "id":"MA_3:AGRI_CROPS(1.0)",
        "title":null,
        "description":null,
        "lastUpdated":"2017-11-29 16:39:28",
        "topics":["Agriculture et pêche","Economie et finance"],
        "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/MA_3,AGRI_CROPS,1.0/.....",
        "index":3,
        "matches":[
          {
            "type":"topic",
            "concept":"",
            "match-text":"<mark>Economie</mark> et finance",
          }
        ]
      }
  ];

const item = n => ({
  uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
  id: `OECD:IMMIGRATION(1.${n})`,
  title: 'Immigration (IMMIGRATION)',
  description: 'Immigration broken down by country, place of birth, citizenship, education level, age.',
  lastUpdated: '2017-12-19 11:35',
  topics: ['Demographic and social statistics', 'Population and migration'],
});

const items = [{
  ...item(0),
}, {
  ...item(1),
  title: '<mark>Immigration</mark> (IMMIGRATION)',
}, {
  ...item(2),
  description: '<mark>Immigration</mark> broken down by country, place of birth, citizenship, education level, age.',
}, {
  ...item(3),
  topics: ['Demographic and social statistics', 'Population and <mark>migration</mark>'],
}, {
  ...item(4),
  matches: [{
    'match-text': 'Source of property price <mark>statistics</mark> (RPP_SOURCE)',
    type: 'dimension',
    "concept": null,
  }],
}, {
  ...item(5),
  matches: [{
    concept: 'Bank lending survey item',
    'match-text': 'Bank lending volume (BLV) Financial situation - market <mark>financing</mark> conditions (BMFC) Financial situation - profitability (BPRO)',
    type: 'code',
  }],
}, {
  ...item(6),
  matches: [{
    concept: 'Bank lending survey item',
    'match-text': 'Bank lending volume (BLV) Financial situation - market <mark>financing</mark> conditions (BMFC) Financial situation - profitability (BPRO)',
    type: 'code',
  },
  {
    'match-text': 'Source of property price <mark>statistics</mark> (RPP_SOURCE)',
    type: 'dimension',
    "concept": null,
  }],
}];

export const items2 = [
  {
    id: 'OECD:POPULATION(1.0)',
    title: 'Population broken down by country and sex for various indicators. (POPULATION)',
    description: 'Population broken down by country and sex for various indicators.',
    lastUpdated: '2017-12-11 13:34:43',
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    topics: ['Demography and population', 'Population'],
    index: 1,
    matches: [
      {
        "type":"dataflow",
        "concept": null,
        "match-text":"<mark>Population</mark> broken down by country and sex for various indicators. (<mark>POPULATION</mark>)",
      },
      {
        "type":"description",
        "concept": null,
        "match-text":"<mark>Population</mark> broken down by country and sex for various indicators.",
      },
      {
        "type":"topic",
        "concept": null,
        "match-text":"Demography and <mark>population</mark>",
      },
      {
        "type":"code",
        "concept": null,
        "match-text":"<mark>Population</mark> (hist5)  00-04, persons (YP9901L1) \r\nPopulation (hist5)  05-09, persons (YP9902L1) \r\n...",
      },
    ],
  },
  {
    id: 'TN1:DEMO_POP_EST2(1.0)',
    title: 'Estimated population on 1st July (with fixed hierarchy in REF_AREA) (DEMO_POP_EST2)',
    description: '',
    lastUpdated: '2017-12-07 12:35:47',
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/TN1,DEMO_POP_EST2,1.0/...',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/TN1,DEMO_POP_EST2,1.0/...',
    index: 2,
    topics: ['Demography and population', 'Population'],
    matches: [
      {
        "type":"dataflow",
        "concept": null,
        "match-text":"Estimated <mark>population</mark> on 1st July (with fixed hierarchy in REF_AREA) (DEMO_POP_EST2)"
      },
      {
        "type":"topic",
        "concept": null,
        "match-text":"Demography and <mark>population</mark>",
      },
    ],
  },
  {
    "id":"SPC:DF_Ton_pop5(1.0)",
    "title":"Population by sex, 5 years age groups, Block Village District Island and Country (DF_Ton_pop5)",
    "description":"test empty matches: no highlight",
    "lastUpdated":"2017-12-05 14:58:10",
    "topics":["Demography and population"],
    "uri":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/SPC,DF_Ton_pop5,1.0/.....",
    "href":"http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/SPC,DF_Ton_pop5,1.0/.....",
    "index":3,
    "matches":[],
  },
  {
    id: 'TN1:DEMO_POP_INS(1.0)',
    title: 'Demographic indicators (DEMO_POP_INS)',
    description: '',
    lastUpdated: '2017-12-01 14:38:42',
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/TN1,DEMO_POP_INS,1.0/.IND_DEM_DP.....',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/TN1,DEMO_POP_INS,1.0/.IND_DEM_DP.....',
    topics: ['Demography and population', 'Population'],
    index: 4,
    matches: [
      {
        "type":"topic",
        "concept": null,
        "match-text":"Demography and <mark>population</mark>"
      },
      {
        "type":"code",
        "match-text":"Distribution of the <mark>population</mark> (IND_DEM_DP) ",
        "concept": 'Whatever',
      },
    ],
  },
];

