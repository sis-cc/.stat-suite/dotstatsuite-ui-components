import React from 'react';
import { VXList, DESearchDataflow } from '../../';

export const DESearchDataflows = props => (
  <VXList {...props} itemRenderer={DESearchDataflow} />
);
