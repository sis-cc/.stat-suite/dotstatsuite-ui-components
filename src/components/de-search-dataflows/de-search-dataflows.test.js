import React from 'react';
import { FormattedMessage } from 'react-intl';
import { boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DESearchDataflows } from '../../';
import { d1, d2, d3, items2 } from './dataflows';

describe('DE - search dataflows', () => {
  const lastUpdatedLabel = date => (
      <FormattedMessage
        id="de.search.dataflow.last.updated"
        values={{ date: <span className="text-muted">{date}</span> }}
      />
    );

  const size = 10;
  const props = {
    statusLabel: <FormattedMessage id="de.search.list.status" values={{ size}} />,
    items: boolean('isBlank (dataflows)', false) ? [] : d1,
    isLoading: boolean('isLoading (dataflows)', false),
    selectDataflow: action('selectDataflow'),
    blankLabel: <FormattedMessage id="de.search.list.blank" />,
    loadingLabel: <FormattedMessage id="de.search.list.loading" />,
    topicLabel: <FormattedMessage id="de.search.dataflow.topic"/>,
    dimensionLabel: <FormattedMessage id='de.search.dataflow.dimension' />,
    lastUpdatedLabel: lastUpdatedLabel,
  }

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<DESearchDataflows {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<DESearchDataflows {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });
});
