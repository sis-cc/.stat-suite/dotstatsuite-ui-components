import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DESearchDataflows, Formats, Themes } from '../../';
import { d1 } from './dataflows';


export const SBSearchDataflows = boilers => (
  <DESearchDataflows
    {...boilers}
    items={boolean('isBlank (dataflows)', false) ? [] : d1}
    isLoading={boolean('isLoading (dataflows)', false)}
    selectDataflow={action('selectDataflow')}
    blankLabel={<FormattedMessage id="de.search.list.blank" />}
    loadingLabel={<FormattedMessage id="de.search.list.loading" />}
    topicLabel={<FormattedMessage id="de.search.dataflow.topic"/>}
    dimensionLabel={boilers.intl.formatMessage({ id: 'de.search.dataflow.dimension' })}
    lastUpdatedLabel={date => (
      <FormattedMessage
        id="de.search.dataflow.last.updated"
        values={{ date: <span className="text-muted">{date}</span> }}
      />
    )}
  />
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('search-dataflows', () => {
  const BoiledSearchDataflows = storyWithBoil({ Formats, Themes })(SBSearchDataflows);
  return <BoiledSearchDataflows />;
});
