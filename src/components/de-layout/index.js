import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import R from 'ramda';

const Wrapper = glamorous.div({
  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  display: 'flex',
  height: '100vh',
  flexDirection: 'column',
}, ({ theme }) => ({
  fontFamily: theme.layout.fontFamily,
}));

const WrapperContent = glamorous.div({
  display: 'flex',
  flexDirection: 'column',

  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  flex: '1 0 auto',
}, ({ isHome, theme }) => ({
  backgroundColor: isHome ? theme.layout.background : null,
  padding: `${isHome ? '30px' : 0} ${theme.layout.xPadding}`,
  justifyContent: isHome ? 'center' : 'initial',
  alignItems: isHome ? 'center' : 'stretch',
}));

const WrapperInnerContent = glamorous.div({
  display: 'flex',
  paddingTop: 20,
}, ({ isRtl, isNarrow }) => ({
  direction: isRtl ? 'rtl' : 'ltr',
  flexDirection: isNarrow ? 'column' : 'row',
}));

const WrapperHeader = glamorous.div({

});

const WrapperSide = glamorous.div({
}, ({ isNarrow, theme }) => ({
  flex: isNarrow ? 1 : `0 0 ${theme.layout.sideWidth}px`,
  maxWidth: isNarrow ? null : theme.layout.sideWidth,
  width: isNarrow ? null : '100%',
}));

const WrapperMain = glamorous.div({
  flex: 1,
  paddingBottom: 20,
}, ({ isRtl, isNarrow, isFull }) => ({
  paddingLeft: isRtl || isNarrow ? 0 : 20,
  paddingRight: isRtl && !isNarrow ? 20 : 0,
  padding: isFull ? '0 10px' : null,
  direction: isRtl && isFull ? 'rtl' : 'unset',
}));

const WrapperFooter = glamorous.div({
  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  flexShrink: 0,
});

export const DELayoutHeader = ({ children }) => children;
export const DELayoutContent = ({ children }) => children;
export const DELayoutSide = ({ children }) => children;
export const DELayoutMain = ({ children }) => children;
export const DELayoutFooter = ({ children }) => children;

export const DELayout = ({ children, isRtl, isHome, isNarrow, isFull }) => {
  const mchildren = React.Children.toArray(children);
  const component = vchildren => vtype => R.find(R.propEq('type', vtype))(vchildren);
  const mcomponent = component(mchildren);

  const main = (
    <WrapperMain isRtl={isRtl} isNarrow={isNarrow} isFull={isFull}>
      {mcomponent(DELayoutMain)}
    </WrapperMain>
  );

  if (isFull) return main;

  return (
    <Wrapper isRtl={isRtl}>
      <WrapperHeader>
        {mcomponent(DELayoutHeader)}
      </WrapperHeader>
      <WrapperContent isRtl={isRtl} isHome={isHome}>
        {mcomponent(DELayoutContent)}
        <WrapperInnerContent isRtl={isRtl} isNarrow={isNarrow}>
          <WrapperSide isNarrow={isNarrow}>
            {mcomponent(DELayoutSide)}
          </WrapperSide>
          {main}
        </WrapperInnerContent>
      </WrapperContent>
      <WrapperFooter>
        {mcomponent(DELayoutFooter)}
      </WrapperFooter>
    </Wrapper>
  );
};

DELayout.propTypes = {
  children: PropTypes.node,
  isRtl: PropTypes.bool,
  isHome: PropTypes.bool,
  isNarrow: PropTypes.bool,
  isFull: PropTypes.bool,
};
