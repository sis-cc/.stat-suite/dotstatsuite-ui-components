import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import {
  DELayout, DELayoutHeader, DELayoutContent, DELayoutSide, DELayoutMain, DELayoutFooter,
  Formats, FontSizes, Themes
} from '../../';

export const Element = ({ backgroundColor, color, label, height }) => (
  <div style={{
    backgroundColor,
    color,
    height,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: FontSizes.SIZE1,
  }}>
    <div>{label}</div>
  </div>
);

const Layout = props => (
  <DELayout {...props} isFull={boolean('isFull (layout)', false)}>
    <DELayoutHeader>
      <Element
        label="DELayoutHeader"
        backgroundColor="#0B486B"
        color="#FFFFFF"
        height={50}
      />
    </DELayoutHeader>
    <DELayoutContent>
      <Element
        label="DELayoutContent"
        backgroundColor="#3B8686"
        color="#FFFFFF"
        height={100}
      />
    </DELayoutContent>
    <DELayoutSide>
      <Element
        label="DELayoutSide"
        backgroundColor="#79BD9A"
        height={props.isNarrow ? 100 : 1000} />
    </DELayoutSide>
    <DELayoutMain>
      <Element label="DELayoutMain" backgroundColor="#A8DBA8" height={400} />
    </DELayoutMain>
    <DELayoutFooter>
      <Element label="DELayoutFooter" backgroundColor="#CFF09E" height={50} />
    </DELayoutFooter>
  </DELayout>
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('layout', () => {
  const BoiledLayout = storyWithBoil({ Formats, Themes, padding: 0, margin: 0 })(Layout);
  return <BoiledLayout />;
});
