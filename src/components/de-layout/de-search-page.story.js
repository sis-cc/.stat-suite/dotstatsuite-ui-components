import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import {
  DELayout, DELayoutHeader, DELayoutContent, DELayoutSide, DELayoutMain, DELayoutFooter,
  Formats, FontSizes, Themes,
} from '../../';
import { SBHeader } from '../de-header/de-header.story';
import { SBSearchHeader } from '../de-search-header/de-search-header.story';
import { SBBreadcrumbs } from '../de-breadcrumbs/de-breadcrumbs.story';
import { SBFiltersCurrent } from '../vx-filters-current/vx-filters-current.story';
import { SBFilters } from '../vx-filters/vx-filters.story';
import { SBSearchDataflows } from '../de-search-dataflows/de-search-dataflows.story';
import { SBFooter } from '../de-footer/de-footer.story';

const Page = props => (
  <DELayout {...props}>
    <DELayoutHeader>
      <SBHeader {...props} />
      <SBSearchHeader {...props} />
    </DELayoutHeader>
    <DELayoutContent>
      <SBBreadcrumbs {...props} />
    </DELayoutContent>
    <DELayoutSide>
      <SBFiltersCurrent {...props} />
      <SBFilters {...props} />
    </DELayoutSide>
    <DELayoutMain>
      <SBSearchDataflows {...props} />
    </DELayoutMain>
    <DELayoutFooter>
      <SBFooter {...props} />
    </DELayoutFooter>
  </DELayout>
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('★ search-page', () => {
  const BoiledPage = storyWithBoil({ Formats, Themes, padding: 0, margin: 0 })(Page);
  return <BoiledPage />;
});
