import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import {
  DELayout, DELayoutHeader, DELayoutContent, DELayoutSide, DELayoutMain, DELayoutFooter,
  Formats, FontSizes, Themes,
} from '../../';
import { Element } from './de-layout.story';
import { SBHeader } from '../de-header/de-header.story';
import { SBSearchHeader } from '../de-search-header/de-search-header.story';
import { SBBreadcrumbs } from '../de-breadcrumbs/de-breadcrumbs.story';
import { SBFiltersCurrent } from '../vx-filters-current/vx-filters-current.story';
import { SBFilters } from '../vx-filters/vx-filters.story';
import { SBFooter } from '../de-footer/de-footer.story';
import { SBVisualisationToolbar } from '../de-visualisation-toolbar/de-visualisation-toolbar.story';
import { SBFilterPeriod } from '../de-filter-period/de-filter-period.story';
import { SBTable } from '../de-table/de-table.story';

const Page = props => (
  <DELayout {...props} isFull={boolean('isFull (layout)', false)}>
    <DELayoutHeader>
      <SBHeader {...props} />
      <SBSearchHeader {...props} onlyLogo />
    </DELayoutHeader>
    <DELayoutContent>
      <SBBreadcrumbs {...props} />
    </DELayoutContent>
    <DELayoutSide>
      <SBFiltersCurrent {...props} />
      <SBFilterPeriod {...props} />
      <SBFilters {...props} />
    </DELayoutSide>
    <DELayoutMain>
      <SBVisualisationToolbar {...props} />
      <SBTable {...props} />
    </DELayoutMain>
    <DELayoutFooter>
      <SBFooter {...props} />
    </DELayoutFooter>
  </DELayout>
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('★ visualisation-page', () => {
  const BoiledPage = storyWithBoil({ Formats, Themes, padding: 0, margin: 0 })(Page);
  return <BoiledPage />;
});
