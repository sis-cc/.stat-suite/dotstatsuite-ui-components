import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import {
  DELayout, DELayoutHeader, DELayoutContent, DELayoutFooter,
  Formats, Themes,
} from '../../';
import { SBHeader } from '../de-header/de-header.story';
import { SBSplash } from '../de-splash/de-splash.story';
import { SBSearchHome } from '../vx-spotlight/vx-spotlight.story';
import { SBSearchFacets } from '../de-search-facets/de-search-facets.story';
import { SBFooter } from '../de-footer/de-footer.story';

const Page = props => (
  <DELayout {...props} isHome>
    <DELayoutHeader>
      <SBHeader {...props} />
    </DELayoutHeader>
    <DELayoutContent>
      <SBSplash {...props} />
      <SBSearchHome {...props} />
      <SBSearchFacets {...props} />
    </DELayoutContent>
    <DELayoutFooter>
      <SBFooter {...props} />
    </DELayoutFooter>
  </DELayout>
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('★ home-page', () => {
  const BoiledPage = storyWithBoil({ Formats, Themes, padding: 0, margin: 0 })(Page);
  return <BoiledPage />;
});
