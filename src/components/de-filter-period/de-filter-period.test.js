import React from 'react';
import { FormattedMessage } from 'react-intl';
import { renderWithIntl } from '../../../helpers/intl-enzyme-test-helper';
import { DEFilterPeriod } from '../../';
import { action } from '@storybook/addon-actions';

const [ min, max ] = [ 1960, 2020 ];

describe('DE - Filter Period', () => {
  const props = {
    titleLabel:<FormattedMessage id="de.filter.period.title" />,
    tagLabel:<FormattedMessage id="de.filter.period.tag" values={{ min, max }} />,
    changePeriod:action('changePeriod'),
    options:{min, max, labelStepSize: 10},
    period:[2000, 2015],
  };

  it('should match snapshot ltr', () => {
    const component = renderWithIntl(<DEFilterPeriod {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithIntl(<DEFilterPeriod {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });
});
