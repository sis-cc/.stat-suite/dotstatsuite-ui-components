import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes, RangeSlider } from '@blueprintjs/core';
import { VXFilterContainer } from '../vx-filter-container';
import { withPeriod } from './with-period';

const Wrapper = glamorous.div({
  padding: 20,
  paddingTop: 4,
  paddingBottom: 8,
});

const StyledRangeSlider = glamorous(RangeSlider)({
  direction: 'ltr',
  '& .pt-slider-axis': {
    display: 'none',
  }
});

const FilterPeriod = ({
  isRtl, titleLabel, tagLabel, changePeriod, period, onPeriodChange, options,
}) => (
  <VXFilterContainer
    titleLabel={titleLabel}
    tagLabel={tagLabel}
    isRtl={isRtl}
    noCollapse
    noScroll
  >
    <Wrapper>
      <StyledRangeSlider
        {...options}
        onChange={onPeriodChange}
        onRelease={changePeriod}
        value={period}
      />
    </Wrapper>
  </VXFilterContainer>
);

FilterPeriod.propTypes = {
  isRtl: PropTypes.bool,
  titleLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  options: PropTypes.object,
  onPeriodChange: PropTypes.func,
  changePeriod: PropTypes.func,
  period: PropTypes.array,
};

export const DEFilterPeriod = withPeriod(FilterPeriod);
