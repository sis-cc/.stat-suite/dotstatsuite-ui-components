import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEFilterPeriod, Formats, Themes } from '../../';

const [ min, max ] = [ 1960, 2020 ];

export const SBFilterPeriod = boilers => (
  <DEFilterPeriod
    {...boilers}
    titleLabel={<FormattedMessage id="de.filter.period.title" />}
    tagLabel={<FormattedMessage id="de.filter.period.tag" values={{ min, max }} />}
    changePeriod={action('changePeriod')}
    options={{min, max, labelStepSize: 10}}
    period={[2000, 2015]}
  />
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('filter-period', () => {
  const BoiledFilterPeriod = storyWithBoil({ Formats, Themes })(SBFilterPeriod);
  return <BoiledFilterPeriod />;
});
