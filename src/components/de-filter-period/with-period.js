import React from 'react';
import R from 'ramda';

export const withPeriod = Component => class extends React.Component {
  state = { period: [] };

  bubbleUp = period => {
    if (R.is(Function, this.props.changePeriod)) this.props.changePeriod(period);
  };

  rehydrateState = props => {
    this.setState({ period: R.propOr(R.prop('period')(this.state), 'period')(props) });
  };

  componentWillMount = () => this.rehydrateState(this.props);
  componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

  onPeriodChange = period => this.setState({ period });

  render = () => (
    <Component
      {...this.props}
      period={this.state.period}
      onPeriodChange={this.onPeriodChange}
    />
  );
};
