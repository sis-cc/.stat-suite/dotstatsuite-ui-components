import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXFilter, VXFilterNote, Formats, Themes } from '../../';
import { spotlightTreeEngine } from '../../utils';

export const list = [
  { id: 1, label: 'صباح الخير' },
  { id: 2, label: 'value 2 testwithsuperlonglabel withsuperlonglabelwithsuper longlabelwithsuperlonglabelwithsuperlong labelwithsuperlonglabelwithsuperlonglabel' },
  { id: 3, label: 'value 3', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
];
export const tree = [
  { id: 1, label: 'صباح الخير' },
  {
    id: 2,
    label: 'value 2',
    isExpanded: true,
    childNodes: [
      { id: 20, label: 'value 20' },
      { id: 21, label: 'value 21' },
      { id: 22, label: 'value 22', className: 'pt-disabled', isDisabled: true, childNodes: [{ id: 220, label: 'value 220', isSelected: true }] },
      { id: 23, label: 'value 3', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
      { id: 24, label: 'Franck' },
      {
        id: 25, 
        label: 'tomate',
        childNodes: [
        { id: 240, label: 'tutur' },
          { id: 200, label: 'value 200' },
          { id: 210, label: 'value 210' },
          { id: 221, label: 'value 221', className: 'pt-disabled', isDisabled: true, childNodes: [{ id: 2210, label: 'value 2210' }] },
          { id: 230, label: 'value 30', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
          { id: 250, label: 'flanc' },
        ]
      },
    ],
  },
  { id: 3, label: 'value 3' },
  {
    id: 4,
    label: 'value 4',
    className: 'pt-disabled',
    isDisabled: true,
    childNodes: [
      { id: 40, label: 'value 40' },
      { id: 41, label: 'value 41' },
      { id: 42, label: 'value 42', childNodes: [{ id: 4444, label: 'value 4444' }] },
      { id: 43, label: 'value 43', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
      { id: 44, label: 'toto' },
      { id: 45, label: 'tutu' },
    ],
  },
];

export const SBFilter = ({ intl, isRtl }) => {
  const isBlank = boolean('isBlank (filter)', false);
  const isTree = boolean('isTree (filter)', true);
  const hasSpotlight = boolean('hasSpotlight (filter)', true);
  let items = [];
  if (!isBlank) items = isTree ? tree : list;

  return (
    <VXFilter
      id="1"
      items={items}
      titleLabel="filter 1"
      tagLabel="happy tag ㋡"
      selectFilterValue={action('selectFilterValue')}
      isRtl={isRtl}
      hasSpotlight={hasSpotlight}
      term="221"
      spotlight={{
        engine: spotlightTreeEngine,
        placeholder: intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
        fields: {
          'vx.spotlight.field.label': {
            id: 'vx.spotlight.field.label',
            accessor: 'label',
            isSelected: true,
          },
        },
      }}
    />
  );
};

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('filter', () => {
  const BoiledFilter = storyWithBoil({ Formats, Themes })(SBFilter);
  return <BoiledFilter />;
});
