import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Tree, Classes } from '@blueprintjs/core';
import { VXSpotlight } from '../../';
import { withFilter } from './with-filter';
import { VXFilterContainer } from '../vx-filter-container';

const StyledNote = glamorous.span({
  fontStyle: 'italic',
}, ({ theme }) => ({
  color: theme.filter.color,
}));

export const VXFilterNote = ({ children }) => <StyledNote>{children}</StyledNote>;

VXFilterNote.propTypes = {
  children: PropTypes.node,
};

const StyledTree = glamorous(Tree, { filterProps: ['isRtl', 'isTree'] })({
  paddingTop: 5,
  direction: 'ltr',
}, ({ isRtl, isTree, theme }) => ({
  transform: isRtl ? 'scaleX(-1)' : null,
  [`& .${Classes.TREE_NODE_CONTENT}`]: {
    paddingLeft: isTree ? null : 5,
  },
  [`& .${Classes.TREE_NODE_CARET_NONE}`]: {
    minWidth: isTree ? 30 : 0,
  },
  [`& .${Classes.DISABLED} > .${Classes.TREE_NODE_CONTENT}`]: {
    color: theme.filter.color,
    '&:hover': {
      backgroundColor: theme.filter.background,
    },
  },
  [`& .${Classes.TREE_NODE_LABEL}`]: {
    transform: isRtl ? 'scaleX(-1)' : null,
    textAlign: isRtl ? 'right' : null,
  },
  [`& .${Classes.TREE_NODE_SECONDARY_LABEL}`]: {
    transform: isRtl ? 'scaleX(-1)' : null,
  },
}));

const Filter = ({
  titleLabel, isBlank, isRtl, isOpen, isTree, onChangeIsOpen, tagLabel,
  spotlight, onSpotlightChange, hasSpotlight, spotlightedItems, term,
  onNodeExpand, onNodeCollapse, onNodeClick,
}) => (
  <VXFilterContainer
    titleLabel={titleLabel}
    tagLabel={tagLabel}
    isRtl={isRtl}
    isOpen={isOpen}
    onChangeIsOpen={onChangeIsOpen}
    isBlank={isBlank}
    topElement={
      hasSpotlight
        ? <VXSpotlight
            spotlight={spotlight}
            term={term}
            changeSpotlight={onSpotlightChange}
            isRtl={isRtl}
          />
        : null
    }
  >
    <StyledTree
      isRtl={isRtl}
      isTree={isTree}
      contents={spotlightedItems}
      onNodeClick={onNodeClick}
      onNodeExpand={onNodeExpand}
      onNodeCollapse={onNodeCollapse}
    />
  </VXFilterContainer>
);

Filter.propTypes = {
  titleLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  tagLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  onChangeIsOpen: PropTypes.func,
  isOpen: PropTypes.bool,
  isBlank: PropTypes.bool,
  isRtl: PropTypes.bool,
  isTree: PropTypes.bool,
  hasSpotlight: PropTypes.bool,
  spotlight: PropTypes.object,
  term: PropTypes.string,
  onSpotlightChange: PropTypes.func.isRequired,
  spotlightedItems: PropTypes.array,
  onNodeClick: PropTypes.func.isRequired,
  onNodeExpand: PropTypes.func.isRequired,
  onNodeCollapse: PropTypes.func.isRequired,
};

Filter.defaultProps = {
  hasSpotlight: true,
};

export const VXFilter = withFilter(Filter);
