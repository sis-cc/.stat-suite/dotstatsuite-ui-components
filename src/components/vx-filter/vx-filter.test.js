import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { VXFilter, VXFilterNote, Themes } from '../../';
import { spotlightTreeEngine } from '../../utils';

export const list = [
  { id: 1, label: 'صباح الخير' },
  { id: 2, label: 'value 2 testwithsuperlonglabel withsuperlonglabelwithsuper longlabelwithsuperlonglabelwithsuperlong labelwithsuperlonglabelwithsuperlonglabel' },
  { id: 3, label: 'value 3', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
];

export const tree = [
  { id: 1, label: 'صباح الخير' },
  {
    id: 2,
    label: 'value 2',
    childNodes: [
      { id: 20, label: 'value 20' },
      { id: 21, label: 'value 21' },
      { id: 22, label: 'value 22', className: 'pt-disabled', isDisabled: true, childNodes: [{ id: 220, label: 'value 220' }] },
      { id: 23, label: 'value 3', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
    ],
  },
  { id: 3, label: 'value 3' },
  {
    id: 4,
    label: 'value 4',
    className: 'pt-disabled',
    isDisabled: true,
    childNodes: [
      { id: 40, label: 'value 40' },
      { id: 41, label: 'value 41' },
      { id: 42, label: 'value 42', childNodes: [{ id: 420, label: 'value 420' }] },
      { id: 43, label: 'value 43', secondaryLabel: <VXFilterNote>23</VXFilterNote> },
    ],
  },
];

describe('VX - filter', () => {
  const props = {
    id: "1",
    items: tree,
    titleLabel: "filter 1",
    tagLabel: "happy tag ㋡",
    selectFilterValue: () => {},
    hasSpotlight: true,
    term: "2",
    spotlight: {
      engine: spotlightTreeEngine,
      placeholder: <FormattedMessage id='vx.spotlight.placeholder' />,
      fields: {
        'vx.spotlight.field.label': {
          id: 'vx.spotlight.field.label',
          accessor: 'label',
          isSelected: true,
        },
      },
    },
  }
  
  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXFilter {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<VXFilter {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot narrow', () => {
    const component = renderWithGoogles(<VXFilter {...props} isNarrow />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot with list', () => {
    const component = renderWithGoogles(<VXFilter {...props} items={list} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot not spotlight', () => {
    const component = renderWithGoogles(<VXFilter {...props} hasSpotlight={false} />);
    expect(component).toMatchSnapshot();
  });
});

describe('VX - filter note', () => {
  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXFilterNote>23</VXFilterNote>) ;
    expect(component).toMatchSnapshot();
  });
});
