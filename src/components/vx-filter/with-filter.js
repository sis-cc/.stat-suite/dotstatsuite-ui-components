import React from 'react';
import R from 'ramda';

export const withFilter = Component => class extends React.Component {
  state = { term: '' };

  bubbleUp = (id) => {
    if (R.is(Function, this.props.selectFilterValue)) {
      this.props.selectFilterValue(this.props.id, id);
    }
  };

  rehydrateState = props => {
    this.setState({ term: R.propOr(R.prop('term')(this.state), 'term')(props) });
  };

  componentWillMount = () => this.rehydrateState(this.props);
  componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

  onSpotlightChange = spotlight => this.setState({ term: R.prop('term')(spotlight) });

  onNodeExpandOrCollapse = isExpanded => node => {
    node.isExpanded = !!isExpanded;
    this.forceUpdate();
  };

  // toggle isExpanded when facetValue is not selectable (to avoid 0 result)
  onNodeClick = node => {
    if (R.prop('isDisabled')(node)) {
      node.isExpanded = !node.isExpanded;
    } else {
      node.isSelected = !node.isSelected;
      this.bubbleUp(node.id);
    }
    this.forceUpdate();
  };

  render = () => {
    const spotlightedItems = this.props.spotlight.engine({
      ...this.props.spotlight,
      term: this.state.term,
    })(this.props.items);

    return (
      <Component
        {...this.props}
        term={this.state.term}
        spotlight={this.props.spotlight}
        isBlank={R.isEmpty(this.props.items)}
        isTree={R.pipe(R.pluck('childNodes'), R.any(R.is(Array)))(this.props.items)}
        spotlightedItems={spotlightedItems}
        onSpotlightChange={this.onSpotlightChange}
        onNodeClick={this.onNodeClick}
        onNodeExpand={this.onNodeExpandOrCollapse(true)}
        onNodeCollapse={this.onNodeExpandOrCollapse()}
      />
    );
  };
};
