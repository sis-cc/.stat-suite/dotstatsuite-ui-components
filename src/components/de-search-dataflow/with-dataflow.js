import { compose, mapProps, withHandlers } from 'recompose';
import { highlightDataflow } from './utils';

export const withDataflow = Component => compose(
  mapProps(({ dimensionLabel, ...item }) => {
    return ({ ...item, dataflow: highlightDataflow(item, dimensionLabel) });
  }),
  withHandlers({
    onSelectDataflow: ({ selectDataflow }) => id => (event) => {
      event.preventDefault();
      selectDataflow(id);
    },
  }),
)(Component);
