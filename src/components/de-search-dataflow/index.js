import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import R from 'ramda';
import { withDataflow } from './with-dataflow';
import { DEVisualisationToolbar } from '../../';

const Wrapper = glamorous.div({
  display: 'flex',
  lineHeight: '22px',
}, ({ isRtl, isNarrow, theme }) => ({
  borderBottom: theme.searchDataflow.borderBottomDataflow,
  padding: '10px 0',
}), ({ isRtl, isNarrow }) => ({
  direction: isRtl ? 'rtl' : 'ltr',
  flexDirection: isNarrow ? 'column' : 'row',
}));

const Content = glamorous.div({
  flex: '1 1 auto',
}, ({ theme }) => ({
  '& mark': {
    backgroundColor: 'transparent !important',
    color: theme.searchDataflow.colorMark,
    textDecoration: 'underline',
  },
}));

const Title = glamorous.span({
  display: 'inline-block',
  minHeight: 24,
  fontWeight: 'bold',
  cursor: 'pointer',
}, ({ theme }) => ({
  fontSize:  theme.searchDataflow.fontSizeTitle,
  color: theme.searchDataflow.colorTitle,
  '&:hover': {
    textDecoration: 'underline',
  },
}));

const StyledLink = glamorous.a({
  '&:hover': {
    textDecoration: 'underline',
  },
});

const Description = glamorous.div({
  margin: '0 0 5px 0',
  lineHeight: '1.5',
}, ({ theme }) => ({
  color: theme.searchDataflow.colorDescription,
  padding: '5px 0',
}));

const Topics = glamorous.div({
  marginTop: 5,
  paddingTop: 5,
  fontStyle: 'italic',
}, ({ theme }) => ({
  borderTop: theme.searchDataflow.borderTopTopic,
  color: theme.searchDataflow.colorTopic,
}));

const TopicLabel = glamorous.span({
  paddingRight: 4,
});

const LastUpdated = glamorous.div({}, ({ theme }) => ({
  color: theme.searchDataflow.colorLastUpdated,
}));

const SearchDataflow = ({
  isRtl, isNarrow, dataflow, lastUpdatedLabel, onSelectDataflow,
  actions, loadingActionId, selectedMenuId, selectedActionId, sourceLabel,
}) => {
  return (
    <Wrapper isRtl={isRtl} isNarrow={isNarrow}>
      <Content>
        <a href={dataflow.href}>
          <Title onClick={onSelectDataflow(dataflow.id)}>
            {dataflow.title || dataflow.id}
          </Title>
        </a>
        { R.isEmpty(dataflow.description) && R.isEmpty(dataflow.matches)
          ? null
          : <Description>
              {dataflow.description}
              {dataflow.matches}
            </Description>
        }
        { R.isEmpty(dataflow.topics)
          ? null
          : <Topics isRtl={isRtl}>
              <TopicLabel>{dataflow.topicLabel}</TopicLabel>
              {dataflow.topics}
            </Topics>
        }
        <LastUpdated>
          {lastUpdatedLabel(dataflow.lastUpdated)}
        </LastUpdated>
        {isRtl 
          ? <LastUpdated>
              <span>{dataflow.source}</span> :{sourceLabel}
            </LastUpdated>
          : <LastUpdated>
              {sourceLabel}: <span>{dataflow.source}</span>
            </LastUpdated>
        }
      </Content>
      <DEVisualisationToolbar
        isInline
        isRtl={isRtl}
        actions={actions}
        loadingActionId={loadingActionId}
        selectedMenuId={selectedMenuId}
        selectedActionId={selectedActionId}
      />
    </Wrapper>
  );
};

SearchDataflow.propTypes = {
  dataflow: PropTypes.shape({
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
    id: PropTypes.string.isRequired,
    lastUpdated: PropTypes.string.isRequired,
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
    matches: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    topics: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
    href: PropTypes.string.isRequired,
    topicLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  }),
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  actions: PropTypes.array,
  onSelectDataflow: PropTypes.func.isRequired,
  lastUpdatedLabel: PropTypes.func.isRequired,
};

SearchDataflow.defaultProps = {
  actions: [],
};

export const DESearchDataflow = withDataflow(SearchDataflow);
