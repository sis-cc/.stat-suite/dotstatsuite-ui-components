import React from 'react';
import R from 'ramda';
import glamorous from 'glamorous';

export const NEW_LINE_REGEXP = /(?:\r\n|\r|\n)/g;
export const ID_IN_TITLE = /(<mark>)?\(\S*\)(<\/mark>)?$/g;
export const MARK_REGEX_WITH_TAG = /(<mark>.*?<\/mark>)/g;
export const MARK_REGEX_WITHOUT_TAG = /<\/*mark>/g;
export const MATCH_TYPES = {
  DATAFLOW: 'dataflow',
  DESCRIPTION: 'description',
  TOPIC: 'topic',
  CODE: 'code',
  DIMENSION: 'dimension',
};
export const API_HIGHLIGHT_REGEXP = /<(.*?)mark>/g;


export const removeIdFromTitle = (title) => {
  if (R.isNil(title)) return;
  return R.trim(R.replace(ID_IN_TITLE, '', title));
};

const labelIntoReactElement = (stringToHighlight) => {
  if (R.isEmpty(stringToHighlight) || R.isNil(stringToHighlight)) return null;
  if (R.contains('mark>', stringToHighlight)) {
    const noMark = R.reject(R.isEmpty, R.split(MARK_REGEX_WITHOUT_TAG, stringToHighlight));
    return React.createElement('mark', { key: `${stringToHighlight} __mark` }, noMark);
  }
  return React.createElement('span', { key: `${stringToHighlight} __span` }, stringToHighlight);
};

export const parseHighlight = (stringToHighlight) => {
  if (R.isEmpty(stringToHighlight)) return null;
  const splitStringToHighlight = R.reject(R.isEmpty, R.split(MARK_REGEX_WITH_TAG, stringToHighlight));
  return R.map(
      elem => labelIntoReactElement(elem),
      splitStringToHighlight,
    )
};

export const parseItemMatch = (match, dimensionLabel) => {
  const matchText = R.prop('match-text', match);
  const type = R.prop('type', match);
  const concept = R.prop('concept', match);
  let displayConcept = concept;
  const displayMatch = R.replace(NEW_LINE_REGEXP, ', ', matchText);
  if (type === MATCH_TYPES.DIMENSION) {
    displayConcept = dimensionLabel;
  }

  const displayMatchElement = parseHighlight(displayMatch);
  const displayConceptElement = React.createElement(glamorous.div({ fontWeight: 'bold', display: 'inline' }), { key: `${displayConcept}` }, `${displayConcept}: `);

  if (!R.isEmpty(concept)) {
    return React.createElement('div', { key: `${displayMatch}` }, [displayConceptElement, displayMatchElement]);
  }
  return null;
};

const clearHighlights = content => R.replace(API_HIGHLIGHT_REGEXP, '', content);
const hasMatchText = content => R.hasIn('match-text', content) && (R.hasIn('concept', content) && R.isNil(R.prop('concept', content)));
const hasMatchTextAndConcept = content => R.hasIn('match-text', content) && (R.hasIn('concept', content) && !R.isNil(R.prop('concept', content)));

export const highlightDataflow = (dataflow, dimensionLabel) => {
  const highlightedDataflow = {
    ...dataflow,
    matches: [],
    title: R.isNil(R.prop('title', dataflow)) ? '' : removeIdFromTitle(dataflow.title),
    topics: R.isNil(R.prop('topics', dataflow)) ? [] : R.join(' - ', dataflow.topics),
    description: R.isNil(R.prop('description', dataflow)) ? '' : dataflow.description,
  };

  const matches = R.prop('matches', dataflow);
  if (R.isEmpty(matches) || R.isNil(matches)) return highlightedDataflow;

  //Match order defined by - https://github.com/cis-itn-oecd/data-explorer/issues/257
  const titleInMatches = R.find(R.propEq('type', MATCH_TYPES.DATAFLOW))(matches);
  if (!R.isNil(titleInMatches) && hasMatchText(titleInMatches)) {
    highlightedDataflow.title = parseHighlight(removeIdFromTitle(titleInMatches['match-text']));
    return highlightedDataflow;
  }

  const descriptionInMatches = R.find(R.propEq('type', MATCH_TYPES.DESCRIPTION))(matches);
  if (!R.isNil(descriptionInMatches) && hasMatchText(descriptionInMatches)) {
    highlightedDataflow.description = parseHighlight(descriptionInMatches['match-text']);
    return highlightedDataflow;
  }

  const topicsInMatches = R.filter(topic => R.prop('type', topic) === MATCH_TYPES.TOPIC && hasMatchText(topic), matches);
  if (!R.isEmpty(topicsInMatches) && R.hasIn('topics', dataflow)) {
    const topics = R.map(topic => {
      const inMatches = R.find(x => clearHighlights(R.prop('match-text', x)) == topic)(topicsInMatches);
      const match = R.isNil(inMatches) ? topic : R.prop('match-text', inMatches);
      return match;
    }, R.prop('topics', dataflow))
    highlightedDataflow.topics = parseHighlight(R.join(' - ', topics));
    return highlightedDataflow;
  }

  const dimensionInMatches = R.find(R.propEq('type', MATCH_TYPES.DIMENSION))(matches);
  if (!R.isNil(dimensionInMatches) && hasMatchText(dimensionInMatches)) {
    highlightedDataflow.matches = parseItemMatch(dimensionInMatches, dimensionLabel)
    return highlightedDataflow;
  }

  const codeInMatches = R.find(R.propEq('type', MATCH_TYPES.CODE))(matches);
  if (!R.isNil(codeInMatches) && hasMatchTextAndConcept(codeInMatches)) {
    highlightedDataflow.matches = parseItemMatch(codeInMatches, dimensionLabel);
    return highlightedDataflow;
  }
  return highlightedDataflow;
};

