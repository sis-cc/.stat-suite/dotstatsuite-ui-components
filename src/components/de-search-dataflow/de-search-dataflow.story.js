import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import { FormattedMessage } from 'react-intl';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DESearchDataflow, Formats, Themes } from '../../';

export const SBSearchDataflow = ({ isRtl, isNarrow, intl }) => {
  const withTitleHighlight = boolean('withTitleHighlight (dataflow)', false);
  const withDescriptionHighlight = boolean('withDescriptionHighlight (dataflow)', false);
  const withTopicsHighlight = boolean('withTopicsHighlight (dataflow)', false);
  const withDimensionHighlight = boolean('withDimensionHighlight (dataflow)', false);
  const withCodeHighlight = boolean('withCodeHighlight (dataflow)', false);

  let matches = [];
  if (withTitleHighlight) {
    matches = [
      {
        type: 'dataflow',
        "concept": null,
        'match-text': '<mark>Population</mark> broken down by country and sex for various <mark>indicators</mark>. (<mark>POPULATION</mark>)'
      },
      {
        type: 'description',
        'match-text': '<mark>Population</mark> <mark>broken</mark> down by country and sex for various indicators.'
      },
    ];
  } else if (withDescriptionHighlight) {
    matches = [
      {
        type: 'description',
        "concept": null,
        'match-text': '<mark>Population</mark> <mark>broken</mark> down by country and sex for various indicators.'
      },
      {
        type: 'topic',
        "concept": null,
        'match-text': 'Demography and <mark>population</mark>'
      },
    ];
  } else if (withTopicsHighlight) {
    matches = [
      {
        type: 'topic',
        "concept": null,
        'match-text': 'Demography and <mark>population</mark>'
      }
    ];
  } else if (!withDimensionHighlight && !withCodeHighlight) {
    matches = matches;
  } else if (withDimensionHighlight && withCodeHighlight) {
    matches = [
      {
        concept: 'Bank lending survey item',
        'match-text': 'Bank lending volume (BLV) Financial situation - market <mark>financing</mark> conditions (BMFC) Financial situation - profitability (BPRO)',
        type: 'code',
      },
      {
        'match-text': 'Source of property price <mark>statistics</mark> (RPP_SOURCE)',
        "concept": null,
        type: 'dimension',
      },
    ];
  } else if (!withDimensionHighlight && withCodeHighlight) {
    matches = [
      {
        concept: 'Code',
        'match-text': '<mark>Population</mark> (hist5)  00-04, persons (YP9901L1), Population (hist5)  05-09, persons (YP9902L1) ...',
        type: 'code',
      },
    ];
  } else if (withDimensionHighlight && !withCodeHighlight) {
    matches = [
        {
          "type": "dimension",
          "concept": null,
          "match-text": "Unit (UNIT_MEASURE), <mark>Dimension</mark> by which the indicators are described (e.g.: percentage, USD, etc.)"
        }
      ]
  };
 
  const dataflow = {
    id: 'OECD:POPULATION(1.0)',
    title: 'Population broken down by country and sex for various indicators. (POPULATION)',
    description: 'Population broken down by country and sex for various indicators.',
    lastUpdated: '2017-12-11 13:34:43',
    source: 'FEDev1',
    topics: [
      'Demography and population',
      'Population',
    ],
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    index: 1,
    matches,
  };

  const actions = [
    { icon: 'import',
      id: 'download',
      label: 'Download',
      action: action('download'),
    },
    { icon: 'refresh',
      id: 'refresh',
      label: 'Refresh',
      action: action('refresh'),
    },
  ];

  SBSearchDataflow.propTypes = {
    isRtl: PropTypes.bool,
    isNarrow: PropTypes.bool,
    intl: PropTypes.object,
  };

  return (
    <DESearchDataflow
      {...dataflow}
      isRtl={isRtl}
      isNarrow={isNarrow}
      actions={actions}
      loadingActionId="refresh"
      selectDataflow={action('selectDataflow')}
      topicLabel={<FormattedMessage id="de.search.dataflow.topic"/>}
      dimensionLabel={intl.formatMessage({ id: 'de.search.dataflow.dimension' })}
      lastUpdatedLabel={date => (
        <FormattedMessage
          id="de.search.dataflow.last.updated"
          values={{ date: <span className="text-muted">{date}</span> }}
        />
      )}
      sourceLabel={<FormattedMessage id="de.search.dataflow.source"/>}
    />
  );
};

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('search-dataflow', () => {
  const BoiledSearchDataflow = storyWithBoil({ Formats, Themes })(SBSearchDataflow);
  return <BoiledSearchDataflow />;
});
