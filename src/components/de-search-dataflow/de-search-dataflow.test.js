import React from 'react';
import { shallow, mount } from 'enzyme';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { DESearchDataflow, Themes } from '../../';
import { ResultItemMatch } from './';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { action } from '@storybook/addon-actions';

describe('DE - Search Dataflow', () => {
  const matches = [
    {
      type: 'dataflow',
      "concept": null,
      'match-text': '<mark>Population</mark> broken down by country and sex for various <mark>indicators</mark>. (<mark>POPULATION</mark>)'
    },
    {
      type: 'description',
      'match-text': '<mark>Population</mark> <mark>broken</mark> down by country and sex for various indicators.'
    },
  ];

  const matchesDescription = [
    {
      type: 'description',
      "concept": null,
      'match-text': '<mark>Population</mark> <mark>broken</mark> down by country and sex for various indicators.'
    },
    {
      type: 'topic',
      "concept": null,
      'match-text': 'Demography and <mark>population</mark>'
    },
  ];

  const matchesTopic = [
    {
      type: 'topic',
      "concept": null,
      'match-text': 'Demography and <mark>population</mark>'
    }
  ];

  const matchesCode = [
    {
      concept: 'Code',
      'match-text': '<mark>Population</mark> (hist5)  00-04, persons (YP9901L1), Population (hist5)  05-09, persons (YP9902L1) ...',
      type: 'code',
    },
  ];

  const matchesDimension = [
    {
      "type": "dimension",
      "concept": null,
      "match-text": "Unit (UNIT_MEASURE), <mark>Dimension</mark> by which the indicators are described (e.g.: percentage, USD, etc.)"
    }
  ]

  const dataflow = {
    id: 'OECD:POPULATION(1.0)',
    title: 'Population broken down by country and sex for various indicators. (POPULATION)',
    description: 'Population broken down by country and sex for various indicators.',
    lastUpdated: '2017-12-11 13:34:43',
    topics: [
      'Demography and population',
      'Population',
    ],
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    index: 1,
  };

  const dataflow2 = {
    id: 'OECD:POPULATION(1.0)',
    title: null,
    description: null,
    lastUpdated: '2017-12-11 13:34:43',
    topics: null,
    uri: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    href: 'http://dotstatcor-dev2.main.oecd.org/NsiWebserviceDisseminationExternal/rest/data/OECD,POPULATION,1.0/.YP9901L1..',
    index: 1,
  };

  const actions = [
    { icon: 'import',
      id: 'download',
      label: 'Download',
      action: () => {},
    },
  ];

  const lastUpdatedLabel = date => (
    <FormattedMessage
      id="de.search.dataflow.last.updated"
      values={{ date: <span className="text-muted">{date}</span> }}
    />
  );

  const props = {
    ...dataflow,
    selectDataflow: action('selectDataflow'),
    topicLabel: <FormattedMessage id="de.search.dataflow.topic"/>,
    dimensionLabel: <FormattedMessage id= 'de.search.dataflow.dimension' />,
    lastUpdatedLabel: lastUpdatedLabel,
    actions,
  };

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} matches={matches} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} isRtl matches={matches}/>);
    expect(component).toMatchSnapshot();
  });

   it('should match snapshot rtl and isNarrow', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} isRtl isNarrow matches={matches}/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot no highlight', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} match={[]} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot descriptionInMatches ', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} matches={matchesDescription} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot topicsInMatches  ', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} matches={matchesTopic} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot dimensionInMatches  ', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} matches={matchesDimension} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot codeInMatches', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} matches={matchesCode} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot null items in dataflow2', () => {
    const component = renderWithGoogles(<DESearchDataflow {...props} {...dataflow2} matches={matchesCode} />);
    expect(component).toMatchSnapshot();
  });
});
