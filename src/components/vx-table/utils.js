import * as R from 'ramda';

export const getValues = R.propOr([], 'values');

const getValuesLength = R.pipe(getValues, R.length);

export const isLast = (index) => (items) => R.equals(index+1, R.length(items));

const vultiply = R.reduce((memo, item) => R.pipe(getValuesLength, R.multiply(memo))(item), 1);

export const getSpanCount = index => R.pipe(R.drop(index+1), vultiply);

export const getSpanRepetition = index => R.pipe(R.take(index), vultiply);

export const ms_getGridColumn = (xOffset, spanCount, values, j, n) => {
  return xOffset + spanCount * j + 1 + n * spanCount * R.length(values);
};

export const getHasBorderRight = (values, spanRepetition, j, n) => {
  return R.equals(j+1, R.length(values)) && R.equals(n+1, spanRepetition)
};

const getMaxSpanRepetition = data => getSpanRepetition(R.pipe(R.length, R.dec)(data))(data);

export const getColumnCount = R.converge(
  R.multiply,
  [R.pipe(R.last, getValuesLength), getMaxSpanRepetition]
);
