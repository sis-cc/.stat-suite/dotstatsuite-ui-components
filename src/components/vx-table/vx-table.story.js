import React from 'react';
import * as R from 'ramda';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { Formats, Themes } from '../../';
import { default as RefTable } from '.';
import Table from './table';
import Header from './header';
import SectionHeader from './section-header';
import SectionRows from './section-rows';
import { Table as StyledTable } from './style';
import * as U from './utils';
import { headerData, sectionHeaderData, sectionRowsData, headerDataFactory, sectionDataFactory } from './mocks';

const stories = storiesOf('data-explorer|table-stuff|lab|responsive', module);
stories.addDecorator(withKnobs);

stories.add('header', () => {
  const BoiledHeader = storyWithBoil({ Formats, Themes })(() => R.times(n => (
    <StyledTable columnCount={8} style={{marginBottom: 16}}>
      <Header data={headerDataFactory(3)} />
    </StyledTable>
  ), 4));
  return <BoiledHeader />;
});

stories.add('section header', () => {
  const BoiledSectionHeader = storyWithBoil({ Formats, Themes })(() => (
    <React.Fragment>
      <StyledTable columnCount={1} style={{marginBottom: 32}}>
        <SectionHeader columnCount={1} data={sectionHeaderData(3)} />
      </StyledTable>
      <StyledTable columnCount={1} style={{marginBottom: 32}}>
        <SectionHeader columnCount={1} data={sectionHeaderData(5)} />
      </StyledTable>
      <StyledTable columnCount={1} style={{marginBottom: 32}}>
        <SectionHeader columnCount={1} data={sectionHeaderData(9)} />
      </StyledTable>
      <StyledTable columnCount={1} style={{marginBottom: 32}}>
        <SectionHeader />
      </StyledTable>
    </React.Fragment>
  ));
  return <BoiledSectionHeader />;
});

stories.add('section rows', () => {
  const BoiledSectionRows = storyWithBoil({ Formats, Themes })(() => (
    <React.Fragment>
      <StyledTable columnCount={5} style={{marginBottom: 32}}>
        <SectionRows headerData={R.take(1, headerData)} data={sectionRowsData()} />
      </StyledTable>
      <StyledTable columnCount={7} style={{marginBottom: 32}}>
        <SectionRows headerData={R.take(2, headerData)} data={sectionRowsData()} />
      </StyledTable>
      <StyledTable columnCount={11} style={{marginBottom: 32}}>
        <SectionRows headerData={headerData} data={sectionRowsData()} />
      </StyledTable>
      <StyledTable columnCount={1} style={{marginBottom: 32}}>
        <SectionRows />
      </StyledTable>
    </React.Fragment>
  ));
  return <BoiledSectionRows />;
});

stories.add('table', () => {
  const BoiledTable = storyWithBoil({ Formats, Themes })(() => (
    <Table
      header={headerDataFactory(3)}
      sections={sectionDataFactory(20)}
    />
  ));
  return <BoiledTable />;
});

stories.add('table (ref)', () => {
  const BoiledTable = storyWithBoil({ Formats, Themes })(() => <RefTable />);
  return <BoiledTable />;
});
