import * as R from 'ramda';
import faker from 'faker';

export const headerDataFactory = seed => {
  faker.seed(seed);
  return R.times(n => ({
    id: faker.random.number(),
    label: faker.lorem.word(),
    values: R.times(() => ({
      id: faker.random.number(),
      label: faker.lorem.word(),
    }), R.inc(n)), // R.always(2), R.inc, R.subtract(seed)
  }), seed);
};

export const sectionHeaderDataFactory = seed => {
  faker.seed(seed);
  return R.times(() => ([
    {id: faker.random.number(), label: faker.lorem.word()},
    {id: faker.random.number(), label: faker.lorem.words()},
  ]), seed);
};

export const sectionRowsDataFactory = seed => {
  faker.seed(seed);
  return R.times(() => R.times(() => ({
    id: faker.random.number(),
    label: faker.lorem.word(),
  }), 2), seed);
};

export const sectionDataFactory = seed => {
  faker.seed(seed);
  return R.times(() => ([
    sectionHeaderDataFactory(4),
    sectionRowsDataFactory(6),
  ]), seed);
};

export const headerData = [
  {id: 'sex', label: 'Sex', values: [
    {id: 'female', label: 'Female'},
    {id: 'male', label: 'Male'},
  ]},
  {id: 'color', label: 'Color', values: [
    {id: 'green', label: 'Green'},
    {id: 'blue', label: 'Blue'},
  ]},
  {id: 'time_period', label: 'Time Period', values: [
    {id: '2000', label: '2000'},
    {id: '2001', label: '2001'},
  ]}, 
];

export const sectionHeaderData = seed => {
  faker.seed(seed);
  return R.times(() => ([
    {id: faker.random.number(), label: faker.lorem.word()},
    {id: faker.random.number(), label: faker.lorem.words()},
  ]), seed);
};

export const sectionRowsData = () => {
  const sectionRow = R.times(() => ({id: faker.random.number(), label: faker.lorem.word()}), 2);
  return R.times(R.always(sectionRow), 6);
};
