import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Table as StyledTable } from './style';
import Header from './header';
import SectionHeader from './section-header';
import SectionRows from './section-rows';

const Table = ({ header, sections }) => (
  <StyledTable columnCount={9}>
    <Header data={header} />
    {R.map(([headerData, rows]) => (
      <React.Fragment>
        <SectionHeader columnCount={9} data={headerData} />
        <SectionRows headerData={header} data={rows} />
      </React.Fragment>
    ), sections)}
  </StyledTable>
);

export default Table;
