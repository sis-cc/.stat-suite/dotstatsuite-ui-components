# Table

## Todo

#### section header
- sticky
- IE msGridRow, msGridColumn

#### section rows
- hasNoBorderTop, hasBorderBottom
- IE msGridRow, msGridColumn

#### observations
- dimensionIds

## Specs
- [x] 3000 observations max
- [ ] fit to content (not compliant with virtualization)
- [x] sticky header
- [ ] sticky section headers
- [x] multiple x, y and z
- [x] compliant with modern browsers and IE11 and Edge
- [ ] supports rtl
- [ ] sticky label header (horizontal)
- [ ] handle empty rows
- [ ] handle empty columns (how to handle time period holes)
- [ ] interleave rendering if needed