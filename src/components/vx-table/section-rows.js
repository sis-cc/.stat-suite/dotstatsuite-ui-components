import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { compose, withProps, setPropTypes, defaultProps, setDisplayName } from 'recompose';
import * as U from './utils';
import { XCell, SCell } from './style';

const enhance = compose(
  setDisplayName('SectionRows(enhanced)'),
  setPropTypes({
    headerData: PropTypes.array.isRequired,
  }),
  defaultProps({
    headerData: [],
  }),
  withProps(({ headerData }) => {
    const columnCount = U.getColumnCount(headerData);

    return {
      columnCount,
      getHasRightBorder: R.equals(R.dec(columnCount)),
    };
  }),
);

const SectionRows = ({ data, columnCount, itemFormatter, renderObservation, getHasRightBorder }) => (
  R.addIndex(R.map)(
    (row, i) => (
      <React.Fragment key={`row-${i}`}>
        {R.map(dimension => (
          <XCell key={dimension.id} style={{msGridRow: 0, msGridColumn: 0}} isDimension>
            {itemFormatter(dimension)}
          </XCell>
        ), row)}
        <SCell style={{msGridRow: 0, msGridColumn: 0}} isDimension />
        {R.times(j => (
          <XCell key={`obs-${j}`} style={{msGridRow: 0, msGridColumn: 0}} hasBorderRight={getHasRightBorder(j)}>
            {renderObservation([j])}
          </XCell>
        ), columnCount)}
      </React.Fragment>
    ),
    data,
  )
);

SectionRows.propTypes = {
  data: PropTypes.array.isRequired,
  itemFormatter: PropTypes.func.isRequired,
  columnCount: PropTypes.number,
  renderObservation: PropTypes.func.isRequired,
  getHasRightBorder: PropTypes.func.isRequired,
};

SectionRows.defaultProps = {
  data: [],
  itemFormatter: R.prop('label'),
  columnCount: 1,
  renderObservation: dimensionIds => R.join(' ', dimensionIds),
};

export default enhance(SectionRows);
