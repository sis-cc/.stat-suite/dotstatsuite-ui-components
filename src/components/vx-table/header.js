import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import sizeMe from 'react-sizeme';
import { compose, withState, withHandlers } from 'recompose';
import * as U from './utils';
import { YCell } from './style';

const withSpaceAndTime = compose(
  withState('yOffsets', 'setYOffsets', R.pipe(R.prop('data'), R.length, R.times(R.always(0)))),
  withHandlers({
    onYVectorSize: ({ setYOffsets }) => index => ({ height }) => setYOffsets(
      R.update(index, height),
    ),
    getOffset: ({ yOffsets }) => index => R.addIndex(R.reduceWhile)(
      (acc, x, i) => R.lte(i, index),
      R.add,
      0,
      yOffsets,
    ),
  }),
);

const YVector = sizeMe({
  noPlaceholder: true,
  monitorHeight: true,
  monitorWidth: false,
})(YCell);

const Header = ({ data, itemFormatter, onYVectorSize, getOffset, xOffset }) => R.addIndex(R.map)(
  (item, i) => {
    const values = U.getValues(item);
    const spanRepetition = U.getSpanRepetition(i)(data);
    return (
      <React.Fragment key={item.id}>
        <YVector
          yOffset={0}
          onSize={onYVectorSize(i)}
          spanCount={xOffset}
          style={{
            msGridColumnSpan: xOffset,
            msGridRow: i+1,
            msGridColumn: 1,
          }}
          isDimension
        >
          {itemFormatter(item)}
        </YVector>
        {R.times(
          n => R.addIndex(R.map)(
            (value, j) => {
              const spanCount = U.getSpanCount(i)(data);
              return (
                <YCell
                  key={value.id}
                  yOffset={0}
                  spanCount={spanCount}
                  hasBorderRight={U.getHasBorderRight(values, spanRepetition, j, n)}
                  style={{
                    msGridColumnSpan: spanCount,
                    msGridRow: i+1,
                    msGridColumn: U.ms_getGridColumn(xOffset, spanCount, values, j, n),
                  }}
                >
                  {itemFormatter(value)}
                </YCell>
              );
            },
            values,
          ),
          spanRepetition,
        )}
      </React.Fragment>
    );
  },
  data,
);

Header.propTypes = {
  data: PropTypes.array.isRequired,
  itemFormatter: PropTypes.func.isRequired,
  onYVectorSize: PropTypes.func.isRequired,
  getOffset: PropTypes.func.isRequired,
  xOffset: PropTypes.number,
};

Header.defaultProps = {
  data: [],
  itemFormatter: R.prop('label'),
  xOffset: 3,
};

export default withSpaceAndTime(Header);

/*import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import sizeMe from 'react-sizeme';
import { compose, withState, withHandlers, setDisplayName } from 'recompose';
import { YCell } from '../styles';

// tmp
const getValues = R.propOr([], 'values');
const getValuesLength = R.pipe(getValues, R.length);
const isLast = (index) => (items) => R.equals(index+1, R.length(items));
const vultiply = R.reduce((memo, item) => R.pipe(getValuesLength, R.multiply(memo))(item), 1);
const getSpanCount = index => R.pipe(R.drop(index+1), vultiply);
const getSpanRepetition = index => R.pipe(R.take(index), vultiply);
const ms_getGridColumn = (xOffset, spanCount, values, j, n) => {
  return xOffset + spanCount * j + 1 + n * spanCount * R.length(values);
};
const getHasBorderRight = (values, spanRepetition, j, n) => {
  return R.equals(j+1, R.length(values)) && R.equals(n+1, spanRepetition)
};
// tmp

const enhance = compose(
  setDisplayName('Header(enhanced)'),
);

const YVector = sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(YCell);

const Header = ({
  data, itemFormatter, onYVectorSize, getYOffset, xColumnCount
}) => R.addIndex(R.map)(
  (item, i) => {
    const values = getValues(item);
    const spanRepetition = getSpanRepetition(i)(data);
    return (
      <React.Fragment key={item.id}>
        <YVector
          yOffset={getYOffset(i)}
          onSize={onYVectorSize(i)}
          spanCount={xColumnCount}
          style={{
            msGridColumnSpan: xColumnCount,
            msGridRow: i+1,
            msGridColumn: 1,
          }}
          isDimension
        >
          {itemFormatter(item)}
        </YVector>
        {R.times(
          n => R.addIndex(R.map)(
            (value, j) => {
              const spanCount = getSpanCount(i)(data);
              return (
                <YCell
                  key={value.id}
                  yOffset={getYOffset(i)}
                  spanCount={spanCount}
                  hasBorderRight={getHasBorderRight(values, spanRepetition, j, n)}
                  style={{
                    msGridColumnSpan: spanCount,
                    msGridRow: i+1,
                    msGridColumn: ms_getGridColumn(xColumnCount, spanCount, values, j, n),
                  }}
                >
                  {itemFormatter(value)}
                </YCell>
              );
            },
            values,
          ),
          spanRepetition,
        )}
      </React.Fragment>
    );
  },
  data,
);

Header.propTypes = {
  data: PropTypes.array.isRequired,
  itemFormatter: PropTypes.func.isRequired,
  onYVectorSize: PropTypes.func.isRequired,
  getYOffset: PropTypes.func.isRequired,
  xOffset: PropTypes.number,
};

Header.defaultProps = {
  itemFormatter: R.prop('label'),
};

export default enhance(Header);*/

