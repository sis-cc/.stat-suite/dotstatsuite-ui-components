import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { ZCell, ZAtom, ZQuant } from './style';

const SectionHeader = ({ data, columnCount, yOffset, itemFormatter }) => (
  <ZCell
    spanCount={columnCount}
    style={{msGridColumnSpan: columnCount, msGridRow: yOffset, msGridColumn: 1}}
    //yOffset={getOffset(2)}
  >
    <ZAtom>
      {R.pipe(
        R.addIndex(R.map)(([dimension, value], index) => (
          <React.Fragment key={`${dimension.id}:${value.id}`}>
            { R.not(R.equals(index, 0)) && <ZQuant>{'\u2B24'}</ZQuant> }
            <ZQuant isDimension>{itemFormatter(dimension)}{' '}</ZQuant>
            <ZQuant isValue>{itemFormatter(value)}</ZQuant>
          </React.Fragment>
        )),
      )(data)}
    </ZAtom>
  </ZCell>
);

SectionHeader.propTypes = {
  data: PropTypes.array.isRequired,
  itemFormatter: PropTypes.func.isRequired,
  yOffset: PropTypes.number,
  columnCount: PropTypes.number,
};

SectionHeader.defaultProps = {
  data: [],
  itemFormatter: R.prop('label'),
  yOffset: 1,
  columnCount: 1,
};

export default SectionHeader;
