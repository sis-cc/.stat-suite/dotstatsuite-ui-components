import React from 'react';
import { times, update, always, addIndex, add, reduceWhile, lte, flip, nth } from 'ramda';
import sizeMe from 'react-sizeme';
import { compose, withState, withHandlers } from 'recompose';
import { Table, YCell, XCell, ZCell, ZAtom, ZQuant, SCell } from './style';

const X = 6;
const Y = 2;
const Z = 5;

const withSpaceAndTime = compose(
  withState('yOffsets', 'setYOffsets', times(always(0), Y+1)),
  withHandlers({
    onYVectorSize: ({ setYOffsets }) => index => ({ height }) => setYOffsets(update(index, height)),
    getOffset: ({ yOffsets }) => index => addIndex(reduceWhile)(
      (acc, x, i) => lte(i, index),
      add,
      0,
      yOffsets,
    ),
  }),
);

const YVector = sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(YCell);
const XVector = sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(XCell);

const Tensor = ({ onYVectorSize, getOffset }) => (
  <Table columnCount={11}>
    {/* line 0 */}
    <YVector
      yOffset={0}
      onSize={onYVectorSize(0)}
      spanCount={3} style={{msGridColumnSpan: 3, msGridRow: 1, msGridColumn: 1}}
      isDimension
    >
      Sex
    </YVector>
    <YCell
      yOffset={0}
      spanCount={4} style={{msGridColumnSpan: 4, msGridRow: 1, msGridColumn: 4}}
    >
      Female
    </YCell>
    <YCell
      yOffset={0}
      spanCount={4} style={{msGridColumnSpan: 4, msGridRow: 1, msGridColumn: 8}}
      hasBorderRight
    >
      Male
    </YCell>
    
    {/* line 1 */}
    <YVector
      yOffset={getOffset(0)}
      onSize={onYVectorSize(1)}
      spanCount={3} style={{msGridColumnSpan: 3, msGridRow: 2, msGridColumn: 1}}
      isDimension
    >
      Time period
    </YVector>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 4}} yOffset={getOffset(0)}>2000</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 5}} yOffset={getOffset(0)}>2001</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 6}} yOffset={getOffset(0)}>2002</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 7}} yOffset={getOffset(0)}>2003</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 8}} yOffset={getOffset(0)}>2000</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 9}} yOffset={getOffset(0)}>2001</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 10}} yOffset={getOffset(0)}>2002</YCell>
    <YCell style={{msGridColumnSpan: 1, msGridRow: 2, msGridColumn: 11}} yOffset={getOffset(0)} hasBorderRight>
      December 2003
    </YCell>

    {/* line 2 */}
    <XVector
      yOffset={getOffset(1)}
      onSize={onYVectorSize(2)}
      style={{msGridRow: 3, msGridColumn: 1}}
      isDimension
    >
      Country
    </XVector>
    <XCell style={{msGridRow: 3, msGridColumn: 2}} yOffset={getOffset(1)} isDimension>Region</XCell>
    <SCell style={{msGridRow: 3, msGridColumn: 3}} yOffset={getOffset(1)} isDimension />
    <SCell style={{msGridRow: 3, msGridColumn: 4}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 5}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 6}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 7}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 8}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 9}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 10}} yOffset={getOffset(1)} />
    <SCell style={{msGridRow: 3, msGridColumn: 11}} yOffset={getOffset(1)} hasBorderRight />

    {/* section 0 */}
    <ZCell
      spanCount={11} style={{msGridColumnSpan: 11, msGridRow: 4, msGridColumn: 1}}
      yOffset={getOffset(2)}
    >
      <ZAtom>
        <ZQuant isDimension>Times:&nbsp;</ZQuant>
        <ZQuant isValue>N°{0}</ZQuant>
        <ZQuant>⚫</ZQuant>
        <ZQuant isDimension>Indicator:&nbsp;</ZQuant>
        <ZQuant isValue>Dropout rate</ZQuant>
        <ZQuant>⚫</ZQuant>
        <ZQuant isDimension>Education level:&nbsp;</ZQuant>
        <ZQuant isValue>Total</ZQuant>
      </ZAtom>
    </ZCell>

    {/* values 0 */}
    <XCell style={{msGridRow: 5, msGridColumn: 1}} hasNoBorderTop isDimension>
      CR0 - Cosmology is the study of the origin
    </XCell>
    <XCell style={{msGridRow: 5, msGridColumn: 2}} hasNoBorderTop isDimension>
      UA0 - Physical cosmology is studied by scientists
    </XCell>
    <SCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 3}} isDimension />
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 4}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 5}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 6}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 7}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 8}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 9}}>-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 10}} >-</XCell>
    <XCell hasNoBorderTop style={{msGridRow: 5, msGridColumn: 11}} hasBorderRight>-</XCell>

    {/* values 1 */}
    <XCell style={{msGridRow: 6, msGridColumn: 1}} isDimension>CR0</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 2}} isDimension>UA1</XCell>
    <SCell style={{msGridRow: 6, msGridColumn: 3}} isDimension />
    <XCell style={{msGridRow: 6, msGridColumn: 4}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 5}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 6}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 7}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 8}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 9}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 10}}>-</XCell>
    <XCell style={{msGridRow: 6, msGridColumn: 11}} hasBorderRight>-</XCell>
    
    {/* section 1 */}
    <ZCell
      spanCount={11} style={{msGridColumnSpan: 11, msGridRow: 7, msGridColumn: 1}}
      yOffset={getOffset(2)}
    >
      <ZAtom>
        <ZQuant isDimension>Times:&nbsp;</ZQuant>
        <ZQuant isValue>N°{1}</ZQuant>
        <ZQuant>⚫</ZQuant>
        <ZQuant isDimension>Indicator:&nbsp;</ZQuant>
        <ZQuant isValue>Dropout rate</ZQuant>
        <ZQuant>⚫</ZQuant>
        <ZQuant isDimension>Education level:&nbsp;</ZQuant>
        <ZQuant isValue>
          CSS Grid Layout is a relatively new layout tool for the web.
          It's incredibly powerful and expressive and enables new layouts 
          that were previously very difficult or altogether impossible.
        </ZQuant>
      </ZAtom>
    </ZCell>

    {/* values 0 */}
    <XCell style={{msGridRow: 8, msGridColumn: 1}} hasNoBorderTop hasBorderBottom isDimension>CR1</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 2}} hasNoBorderTop hasBorderBottom isDimension>UA0</XCell>
    <SCell style={{msGridRow: 8, msGridColumn: 3}} hasNoBorderTop hasBorderBottom isDimension />
    <XCell style={{msGridRow: 8, msGridColumn: 4}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 5}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 6}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 7}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 8}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 9}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 10}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 8, msGridColumn: 11}} hasNoBorderTop hasBorderBottom hasBorderRight>-</XCell>
    
    {/* values 1 */}
    <XCell style={{msGridRow: 9, msGridColumn: 1}} hasNoBorderTop hasBorderBottom isDimension>CR1</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 2}} hasNoBorderTop hasBorderBottom isDimension>UA1</XCell>
    <SCell style={{msGridRow: 9, msGridColumn: 3}} hasNoBorderTop hasBorderBottom isDimension />
    <XCell style={{msGridRow: 9, msGridColumn: 4}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 5}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 6}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 7}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 8}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 9}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 10}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 9, msGridColumn: 11}} hasNoBorderTop hasBorderBottom hasBorderRight>-</XCell>

    {/* values 2 */}
    <XCell style={{msGridRow: 10, msGridColumn: 1}} hasNoBorderTop hasBorderBottom isDimension>CR1</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 2}} hasNoBorderTop hasBorderBottom isDimension>UA0</XCell>
    <SCell style={{msGridRow: 10, msGridColumn: 3}} hasNoBorderTop hasBorderBottom isDimension />
    <XCell style={{msGridRow: 10, msGridColumn: 4}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 5}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 6}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 7}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 8}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 9}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 10}} hasNoBorderTop hasBorderBottom>-</XCell>
    <XCell style={{msGridRow: 10, msGridColumn: 11}} hasNoBorderTop hasBorderBottom hasBorderRight>-</XCell>
    
    {/* values 3 */}
    {times(n => (
      <React.Fragment key={n}>
        <XCell style={{msGridRow: 11, msGridColumn: 1}} hasNoBorderTop hasBorderBottom isDimension>CR1</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 2}} hasNoBorderTop hasBorderBottom isDimension>UA1</XCell>
        <SCell style={{msGridRow: 11, msGridColumn: 3}} hasNoBorderTop hasBorderBottom isDimension />
        <XCell style={{msGridRow: 11, msGridColumn: 4}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 5}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 6}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 7}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 8}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 9}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 10}} hasNoBorderTop hasBorderBottom>-</XCell>
        <XCell style={{msGridRow: 11, msGridColumn: 11}} hasNoBorderTop hasBorderBottom hasBorderRight>-</XCell>
      </React.Fragment>
    ))(375)}
  </Table>
);

export default withSpaceAndTime(Tensor);
