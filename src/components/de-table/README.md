# Table

## Todo
- IE msGridRow, msGridColumn
- if no section, need border bottom for row header and last rows of section without header
- 13, 25, 29 -> last line -> browser limitation (perf, limit lines), 10000+ cells
- how to deal with single valued dimensions
- how to handle uncomplete layout (everything in sections)
- single dimension
- throws error in section-rows when dimensions is empty

## after-mvp
- fix stories, rm legacies
- order of values should be aligned to layout order (sounds right)

## Specs
- [x] 3000 observations max
- [x] fit to content (not compliant with virtualization)
- [x] sticky header
- [x] multiple x, y and z
- [x] handle empty rows
- [x] handle empty sections
- [x] handle empty columns
- [-] sticky section headers (with dynamic size)
- [-] compliant with modern browsers and IE11 and Edge
- [ ] update layout if too small (scroll detection)
- [ ] undeterminist spans in header
- [ ] supports rtl (MUI migration)
- [ ] sticky label header (horizontal)