import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { ItemTooltip } from './tooltip';

const Cell = glamorous.div({}, ({ parents }) => ({
  paddingLeft: R.pipe(R.when(R.isNil, R.always([])), R.length, R.multiply(12))(parents)
}))

export const ItemCell = ({ label, parents, flags, styleKey, isRtl }) => {
  const hasNoFlags = R.anyPass([R.isNil, R.isEmpty])(flags);
  if (hasNoFlags) {
    return <Cell parents={parents}>{label}</Cell>;
  }
  return (
    <Cell parents={parents}>
      {label}
      <ItemTooltip isRtl={isRtl} styleKey={styleKey} flags={flags} />
    </Cell>
  );
};

ItemCell.propTypes = {
  label: PropTypes.string,
  flags: PropTypes.array,
  parents: PropTypes.array,
  styleKey: PropTypes.string,
  isRtl: PropTypes.bool,
};
