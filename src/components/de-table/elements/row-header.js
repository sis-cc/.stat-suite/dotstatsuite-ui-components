import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import sizeMe from 'react-sizeme';
import { compose, branch, renderNothing, setDisplayName, withProps } from 'recompose';
import { XCell, SCell } from '../styles';
import { ItemCell } from './itemCell';

const enhance = compose(
  setDisplayName('RowHeader(enhanced)'),
  branch(({ data }) => R.isEmpty(data), renderNothing),
  withProps(({ yColumnCount, yOffset }) => ({
    getHasBorderRight: R.equals(R.dec(yColumnCount)),
    msGridRow: R.inc(yOffset),
  })),
);

const XVector = sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(XCell);

const RowHeader = ({
  data, xColumnCount, msGridRow, headerData,
  getHasBorderRight, onYVectorSize, getYOffset, yOffset, isRtl,
}) => (
  <React.Fragment>
    {R.addIndex(R.map)((rowColumn, i) => (
      <XVector
        key={i}
        yOffset={getYOffset(yOffset)}
        onSize={onYVectorSize(yOffset)}
        style={{msGridRow, msGridColumn: R.inc(i)}}
        isDimension
        isRowHeader
        hasBorderRight={isRtl}
      >
        <ItemCell isRtl={isRtl} {...R.propOr({}, 'dimension', rowColumn)} />
      </XVector>
    ), data)}
    <SCell
      style={{msGridRow, msGridColumn: xColumnCount}}
      yOffset={getYOffset(yOffset)}
      isRowHeader
    />
    {R.addIndex(R.map)((header, j) => (
      <SCell
        key={`row-header-${j}`}
        style={{msGridRow, msGridColumn: R.add(xColumnCount, R.inc(j))}}
        hasBorderRight={isRtl ? false : getHasBorderRight(j)}
        yOffset={getYOffset(yOffset)}
        isRowHeader
        isRtl={isRtl}
      >
        <ItemCell isRtl={isRtl} {...header} styleKey="tableSerieFlags" />
      </SCell>
    ), headerData)}
  </React.Fragment>
);

RowHeader.propTypes = {
  data: PropTypes.array.isRequired,
  yColumnCount: PropTypes.number.isRequired,
  xColumnCount: PropTypes.number.isRequired,
  msGridRow: PropTypes.number.isRequired,
  yOffset: PropTypes.number.isRequired,
  getHasBorderRight: PropTypes.func.isRequired,
  onYVectorSize: PropTypes.func.isRequired,
  getYOffset: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

export default enhance(RowHeader);
