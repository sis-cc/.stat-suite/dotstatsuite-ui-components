import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { compose, branch, renderNothing, setDisplayName } from 'recompose';
import { Icon } from '@blueprintjs/core';
import { ZCell, ZAtom, ZQuant } from '../styles';
import { ItemTooltip } from './tooltip';

const enhance = compose(
  setDisplayName('SectionHeader(enhanced)'),
  branch(({ section }) => R.isEmpty(R.propOr([], 'data', section)), renderNothing),
);

const SectionHeader = ({ section, columnCount, yOffset, msGridRow, isRtl, theme }) => (
  <ZCell
    spanCount={columnCount}
    style={{msGridColumnSpan: columnCount, msGridRow, msGridColumn: 1}}
    yOffset={yOffset}
  >
    <ZAtom>
      {R.pipe(
        R.addIndex(R.map)((entry, index) => (
          <React.Fragment key={index}>
            { R.not(R.equals(index, 0)) && <ZQuant><Icon iconName='dot'/></ZQuant> }
            <ZQuant isDimension isRtl={isRtl}>{R.pathOr('', ['dimension', 'label'], entry)}{':'}</ZQuant>
            <ZQuant isValue>{R.pathOr('', ['value', 'label'], entry)}</ZQuant>
            <ItemTooltip isRtl={isRtl} styleKey="tableSectionValueFlags" flags={R.pathOr([], ['value', 'flags'], entry)} />
          </React.Fragment>
        )),
      )(R.propOr([], 'data', section))}
    </ZAtom>
    <ItemTooltip isRtl={isRtl} styleKey="tableSectionValueFlags" flags={R.propOr([], 'flags', section)} />
  </ZCell>
);

SectionHeader.propTypes = {
  section: PropTypes.object.isRequired,
  yOffset: PropTypes.number.isRequired,
  msGridRow: PropTypes.number.isRequired,
  columnCount: PropTypes.number.isRequired,
  isRtl: PropTypes.bool,
};

export default enhance(SectionHeader);
