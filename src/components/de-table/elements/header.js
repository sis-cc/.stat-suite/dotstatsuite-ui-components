import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import sizeMe from 'react-sizeme';
import { compose, withState, withHandlers, setDisplayName, withProps, renderNothing, branch } from 'recompose';
import { YCell } from '../styles';
import { ItemCell } from './itemCell';

//value={itemFormatter(getDimensionFromRow(headerRow)).value}

const YVector = sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(YCell);

const enhance = compose(
  setDisplayName('Header(enhanced)'),
  branch(({ data }) => R.equals([''], data), renderNothing),
  withProps(({ yColumnCount }) => ({
    getHasBorderRight: R.equals(R.dec(yColumnCount)),
    getDimensionFromRow: R.pipe(R.head, R.prop('dimension')),
  })),
);

const Header = ({
  data, onYVectorSize, getYOffset,
  xColumnCount, yColumnCount,
  getHasBorderRight, getDimensionFromRow, isRtl,
}) => (
  R.addIndex(R.map)((headerRow, i) => (
    <React.Fragment key={i}>
      <YVector
        isRtl={isRtl}
        yOffset={getYOffset(i)}
        onSize={onYVectorSize(i)}
        spanCount={xColumnCount}
        style={{ msGridColumnSpan: xColumnCount, msGridRow: R.inc(i), msGridColumn: 1 }}
        isDimension
        isHeader
        hasBorderRight={isRtl}
      >
        <ItemCell isRtl={isRtl} {...getDimensionFromRow(headerRow)} />
      </YVector>
      {R.addIndex(R.map)(({ dimension, value }, j) => (
        <YCell
          isRtl={isRtl}
          key={`header-column-${j}`}
          yOffset={getYOffset(i)}
          hasBorderRight={isRtl ? false : getHasBorderRight(j)}
          style={{msGridRow: R.inc(i), msGridColumn: R.add(R.inc(xColumnCount), j)}}
        >
          <ItemCell isRtl={isRtl} {...value} styleKey="tableColumnValueFlags" />
        </YCell>
      ))(headerRow)}
    </React.Fragment>
  ))(data)
);

Header.propTypes = {
  data: PropTypes.array.isRequired,
  yColumnCount: PropTypes.number.isRequired,
  xColumnCount: PropTypes.number.isRequired,
  onYVectorSize: PropTypes.func.isRequired,
  getYOffset: PropTypes.func.isRequired,
  xOffset: PropTypes.number,
  getHasBorderRight: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

export default enhance(Header);
