import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';
import glamorous from 'glamorous';
import * as R from 'ramda';
import {
  Classes,
  Icon,
  Position,
  Tooltip
} from '@blueprintjs/core';

const Content = glamorous.ul({
  borderRadius: 3,
  boxSizing: 'border-box',
  color: 'white',
  fontFamily: 'Segoe UI',
  fontSize: 12,
  listStyleType: 'none',
  maxWidth: 270,
  padding: 0,
  margin: 0,
});

const tooltip = css({
  '& > div::before' : {
     top: 0
   }
});

const Flags = glamorous.div({}, ({ styleKey, theme }) => ({
  ...R.propOr({}, styleKey, theme),
  display: 'flex',
  flex: 'row',
  alignItems: 'baseline',
}));

const Span = glamorous.span({}, ({ styleKey, theme }) => ({
  ...R.propOr({}, styleKey, theme)
}));

export const ItemTooltip = ({ flags, isRtl, styleKey }) => {
  if (R.isNil(flags) || R.isEmpty(flags)) return null;

  const uncodedIcon = <Span styleKey={styleKey}><Icon iconName="asterisk" iconSize={null} /></Span>;

  const codedIcons = R.pipe(
    R.reduce(
      (acc, flag) => R.when(
        R.always(R.has('code', flag)),
        R.append(flag.code)
      )(acc),
      []
    ),
    R.join(',')
  )(flags);

  const hasUncoded = R.any(R.complement(R.has('code')), flags);

  const content = (
    <Content>
      {R.addIndex(R.map)(
        ({ label }, index) => <li key={index}>{label}</li>,
        flags
      )}
    </Content>
  );

  return (
    <Tooltip
      content={content}
      hoverCloseDelay={0}
      hoverOpenDelay={0}
      transitionDuration={10}
      openOnTargetFocus={false}
      position={isRtl ? Position.LEFT : Position.RIGHT}
      useSmartPositioning={true}
      tooltipClassName={`${tooltip}`}
    >
      <Flags styleKey={styleKey}>
        ({codedIcons}{!R.isEmpty(codedIcons) && hasUncoded ? ',' : null}{hasUncoded ? uncodedIcon : null})
      </Flags>
    </Tooltip>
  );
};

ItemTooltip.propTypes = {
  flags: PropTypes.arrayOf(PropTypes.shape({
    code: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    label: PropTypes.string
  })),
  isRtl: PropTypes.bool,
  styleKey: PropTypes.string,
};
