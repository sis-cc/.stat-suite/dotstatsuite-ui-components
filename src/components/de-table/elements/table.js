import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import {
  compose, branch, renderNothing, setDisplayName, setPropTypes, defaultProps,
  withProps, withState, withHandlers,
} from 'recompose';
import Header from './header';
import RowHeader from './row-header';
import SectionHeader from './section-header';
import SectionRows from './section-rows';
import { Table as StyledTable } from '../styles';

const HEADER = 'HEADER';
const ROW_HEADER = 'ROW_HEADER';

const updatedHeights = headerYOffsets => R.pipe(
  R.length,
  R.times(R.always(0)),
  R.addIndex(R.map)((zero, index) =>
    R.isNil(R.nth(index)(headerYOffsets)) ? zero : R.nth(index)(headerYOffsets)
  )
);

export const enhanceProps = ({ headerData, sectionsData }) => {
  const xLayoutData = R.pathOr([], [0, 1, 0, 'data'], sectionsData);
  const xColumnCount = R.pipe(
    R.length,
    R.inc
  )(xLayoutData);
  const yColumnCount = R.length(headerData);
  const columnCount = R.add(xColumnCount, yColumnCount);
  const yOffsetRowHeader = 0;
  const yOffsetSectionHeader = R.inc(yOffsetRowHeader);
  const msGridRowOffsetFactory = sectionIndex => R.sum([
    R.inc(yOffsetSectionHeader),
    sectionIndex,
    R.pipe(
      R.take(sectionIndex),
      R.reduce(
        R.useWith(R.add, [R.identity, R.pipe(R.last, R.length)]),
        0
      ),
    )(sectionsData)
  ]);

  return {
    transposedHeaderData: R.pipe(R.pluck('data'), R.transpose)(headerData),
    xLayoutData,
    xColumnCount, yColumnCount, columnCount,
    yOffsetRowHeader, yOffsetSectionHeader,
    hasBorderBottomSectionRows: R.equals(R.dec(R.length(sectionsData))),
    msGridRowOffsetFactory,
  };
};

const enhance = compose(
  setDisplayName('Table(enhanced)'),
  defaultProps({
    headerData: [],
    sectionsData: [],
    cells: {},
  }),
  withProps(enhanceProps),
  withState(
    'yOffsets',
    'setYOffsets',
    { [HEADER]: [0], [ROW_HEADER]: [0] }
  ),
  withHandlers({
    onYVectorSize: ({ setYOffsets }) => (label, headerData) => index => ({ height }) => setYOffsets(hashYOffset => {
      const yOffsets = R.equals(label, HEADER) ? updatedHeights(R.prop(HEADER)(hashYOffset))(headerData) : R.prop(label)(hashYOffset);
      return R.mergeRight(hashYOffset)({ [label]: R.update(index, height)(yOffsets) });
    }),
    getYOffset: ({ yOffsets }) => (index) => {
      return R.pipe(R.prop(HEADER), R.slice(0, index), R.sum)(yOffsets);
    },
    getSubHeaderOffset: ({ yOffsets }) => (headerData) => (index) => {
      const lengthHeader = R.length(headerData);
      return R.add(
        R.pipe(R.prop(HEADER), R.slice(0, lengthHeader), R.sum)(yOffsets),
        R.pipe(R.prop(ROW_HEADER), R.slice(0, index), R.sum)(yOffsets),
      );
    },
  }),
);

const Table = ({
  transposedHeaderData, sectionsData, xLayoutData,
  columnCount, xColumnCount, yColumnCount,
  onYVectorSize, getYOffset, yOffsetRowHeader, yOffsetSectionHeader,
  hasBorderBottomSectionRows, cells, headerData,
  msGridRowOffsetFactory, getSubHeaderOffset, yOffsets, isRtl
}) => (
  <StyledTable columnCount={columnCount} xColumnCount={xColumnCount}>
    <Header
      data={transposedHeaderData}
      xColumnCount={xColumnCount}
      yColumnCount={yColumnCount}
      onYVectorSize={onYVectorSize(HEADER, transposedHeaderData)}
      getYOffset={getYOffset}
      isRtl={isRtl}
    />
    <RowHeader
      data={xLayoutData}
      headerData={headerData}
      yColumnCount={yColumnCount}
      xColumnCount={xColumnCount}
      onYVectorSize={onYVectorSize(ROW_HEADER)}
      getYOffset={getSubHeaderOffset(transposedHeaderData)}
      yOffset={yOffsetRowHeader}
      isRtl={isRtl}
    />
    {R.addIndex(R.map)(([sectionHeaderData, sectionRowsData], sectionIndex) => {
      const msGridRowOffset = msGridRowOffsetFactory(sectionIndex);

      return (
        <React.Fragment key={`section-${sectionIndex}`}>
          <SectionHeader
            columnCount={columnCount}
            section={sectionHeaderData}
            yOffset={getSubHeaderOffset(transposedHeaderData)(yOffsetSectionHeader)}
            msGridRow={msGridRowOffset}
            isRtl={isRtl}
          />
          <SectionRows
            data={sectionRowsData}
            headerData={headerData}
            cells={cells}
            sectionData={sectionHeaderData}
            yColumnCount={yColumnCount}
            xColumnCount={xColumnCount}
            sectionHasBorderBottom={hasBorderBottomSectionRows(sectionIndex)}
            yOffset={yOffsetRowHeader}
            msGridRowOffset={msGridRowOffset}
            isRtl={isRtl}
          />
        </React.Fragment>
      );
    }, sectionsData)}
  </StyledTable>
);

Table.propTypes = {
  cells: PropTypes.object.isRequired,
  headerData: PropTypes.array.isRequired,
  transposedHeaderData: PropTypes.array.isRequired,
  sectionsData: PropTypes.array.isRequired,
  xLayoutData: PropTypes.array.isRequired,
  columnCount: PropTypes.number.isRequired,
  xColumnCount: PropTypes.number.isRequired,
  yColumnCount: PropTypes.number.isRequired,
  onYVectorSize: PropTypes.func.isRequired,
  getYOffset: PropTypes.func.isRequired,
  hasBorderBottomSectionRows: PropTypes.func.isRequired,
  msGridRowOffsetFactory: PropTypes.func,
  isRtl: PropTypes.bool,
};

export default enhance(Table);
