import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { compose, withProps, setDisplayName, setPropTypes, defaultProps, renderNothing, branch } from 'recompose';
import { XCell, SCell } from '../styles';
import { ItemCell } from './itemCell';
import { ValueCell } from './valueCell';
import { ItemTooltip } from './tooltip';

const enhance = compose(
  setDisplayName('SectionRows(enhanced)'),
  branch(({ yColumnCount }) => R.lte(yColumnCount, 0), renderNothing),
  setPropTypes({
    yOffset: PropTypes.number.isRequired,
    msGridRowOffset: PropTypes.number.isRequired,
  }),
  withProps(({ yColumnCount, sectionHasBorderBottom, data, yOffset, msGridRowOffset }) => ({
    getHasBorderRight: R.equals(R.dec(yColumnCount)),
    getHasNoBorderTop: R.or(R.equals(0), R.always(R.not(sectionHasBorderBottom))), // sectionHasBorderBottom <=> hasSection
    getHasBorderBottom: R.both(R.always(sectionHasBorderBottom), R.equals(R.dec(R.length(data)))),
    msGridRow: rowIndex => R.sum([R.inc(rowIndex), msGridRowOffset]),
  })),
);

const SectionRows = ({
  data, sectionData, yColumnCount, xColumnCount, cells, msGridRow,
  getHasBorderRight, getHasBorderBottom, getHasNoBorderTop, headerData, isRtl,
}) => (
  R.addIndex(R.map)(
    (row, i) => (
      <React.Fragment key={`section-row-${i}`}>
        {R.addIndex(R.map)((entry, columnIndex) => (
          <XCell
            key={columnIndex}
            style={{msGridRow: msGridRow(i), msGridColumn: R.inc(columnIndex)}}
            isDimension
            parents={R.pathOr([], ['value', 'parents']. entry)}
            hasBorderBottom={getHasBorderBottom(i)}
            hasNoBorderTop={getHasNoBorderTop(i)}
            hasBorderRight={isRtl}
          >
            <ItemCell {...R.propOr({}, 'value', entry)} styleKey="tableRowValueFlags" />
          </XCell>
        ), R.propOr([], 'data', row))}
        <SCell
          style={{msGridRow: msGridRow(i), msGridColumn: xColumnCount}}
          isRow
          hasBorderBottom={getHasBorderBottom(i)}
          hasNoBorderTop={getHasNoBorderTop(i)}
        >
          <ItemTooltip isRtl={isRtl} styleKey="tableSerieFlags" flags={R.propOr([], 'flags', row)} />
        </SCell>
        {
          R.addIndex(R.map)(
            (column, columnIndex) => <XCell
                key={`obs-${columnIndex}`}
                style={{msGridRow: msGridRow(i), msGridColumn: R.add(xColumnCount, R.inc(columnIndex))}}
                hasBorderRight={isRtl ? false : getHasBorderRight(columnIndex)}
                hasBorderBottom={getHasBorderBottom(i)}
                hasNoBorderTop={getHasNoBorderTop(i)}
              >
                <ValueCell isRtl={isRtl} {...R.pathOr({}, [column.key, sectionData.key, row.key, 0], cells)}/>
              </XCell>,
              headerData
          )
        }
      </React.Fragment>
    ),
    data,
  )
);

SectionRows.propTypes = {
  cells: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
  headerData: PropTypes.array.isRequired,
  sectionData: PropTypes.object.isRequired,
  yColumnCount: PropTypes.number.isRequired,
  xColumnCount: PropTypes.number.isRequired,
  msGridRow: PropTypes.func.isRequired,
  getHasBorderRight: PropTypes.func.isRequired,
  getHasNoBorderTop: PropTypes.func.isRequired,
  getHasBorderBottom: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

SectionRows.defaultProps = {
  cells: {},
  data: [],
  headerData: [],
  sectionData: [],
  yColumnCount: 1,
  renderObservation: info => (
    <span style={{fontSize: 10}}>
      {R.head(info)}
    </span>
  ),
};

export default enhance(SectionRows);

//{R.pipe(R.toPairs, R.map(R.join(':')), R.join(' '))(info)}

//{renderObservation(getInfos({ columnIndex, rowData, sectionData, transposedHeaderData }))}
