import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { ItemTooltip } from './tooltip';

const Cell = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between'
}, ({ isRtl }) => ({
  flexDirection: isRtl ? 'row-reverse' : 'row', 
}));

const Tooltip = glamorous(ItemTooltip)({
  marginRight: 4 
});

export const ValueCell = ({ value, flags, isRtl }) => {
  const hasNoFlags = R.anyPass([R.isNil, R.isEmpty])(flags);
  if (hasNoFlags) {
    return <div>{value}</div>;
  }
  return (
    <Cell isRtl={isRtl}>
      <Tooltip isRtl={isRtl} styleKey="tableValueFlags" flags={flags} />
      <div>{value}</div>
    </Cell>
  );
};

ValueCell.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  flags: PropTypes.array,
  isRtl: PropTypes.bool,
};
