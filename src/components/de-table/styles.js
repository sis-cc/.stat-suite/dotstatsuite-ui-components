import glamorous from 'glamorous';
import * as R from 'ramda';

const Cell = glamorous.div({
  padding: '4px 8px',
  zIndex: 1
}, ({ hasBorderRight, hasBorderBottom, hasNoBorderTop, spanCount, yOffset, isRtl, theme }) => ({
  borderTop: hasNoBorderTop ? null : `1px solid ${theme.table.border}`,
  borderLeft: `1px solid ${theme.table.border}`,
  borderRight: hasBorderRight ? `1px solid ${theme.table.border}` : null,
  borderBottom: hasBorderBottom ? `1px solid ${theme.table.border}` : null,
  '@supports (display: grid)': {
    gridColumn: `span ${spanCount}`,
  },
  '@supports (position: sticky)': {
    position: R.isNil(yOffset) ? null : 'sticky',
    top: yOffset,
  },
}));

export const Table = glamorous.div({
  display: '-ms-grid',
  '@supports (display: grid)': {
    display: 'grid',
  },
  gridAutoRows: 'auto',
  msGridAutoFlow: 'row',
}, ({ columnCount, xColumnCount, theme }) => {
  const gridTemplateColumns = R.pipe(
    R.times(R.always('auto')),
    R.update(R.dec(xColumnCount), '30px'),
    R.join(' '),
  )(columnCount);

  return {
    gridTemplateColumns,
    msGridColumns: gridTemplateColumns,
  };
});

export const YCell = glamorous(Cell)({}, ({ isDimension, isHeader, theme }) => ({
  color: isDimension ? theme.table.yFontHeader : theme.table.yFont,
  textAlign: isDimension ? 'right' : 'center',
  backgroundColor: theme.table.yBg,
  fontWeight: isHeader ? 'bold' : 'normal',
}));

export const XCell = glamorous(Cell)({}, ({ isDimension, isRowHeader, theme }) => ({
  color: isDimension ? theme.table.xFontHeader : theme.table.xFont,
  backgroundColor: isDimension ? theme.table.xBgHeader : theme.table.xBg,
  fontWeight: isRowHeader ? 'bold' : 'normal',
  paddingLeft: 8,
  borderBottom: isRowHeader ? `1px solid ${theme.table.border}` : null,
  textAlign: isDimension ? null : 'right',
  '&:hover': {
    backgroundColor: isDimension ? theme.table.xBgHeader : theme.table.xBgHeader,
  },
  zIndex: isRowHeader ? 1 : 0
}));

export const ZCell = glamorous(Cell)({
  textAlign: 'left',
  borderTop: 'none',
  borderLeft: 'none',
  borderRight: 'none',
  display: 'flex',
  justifyContent: 'space-between'
}, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
}));

export const ZAtom = glamorous.div({}, ({ theme }) => ({
  backgroundColor: theme.table.zBg,
  zIndex: 1,
  display: 'flex',
}));

export const ZQuant = glamorous.span({}, ({ isDimension, theme, isRtl }) => ({
  padding: 0,
  color: isDimension ? theme.table.zFontHeader : theme.table.zFont,
  marginRight: isDimension ? isRtl ? 0 : 5 : 0,
  marginLeft: R.and(isDimension, isRtl) ? 5 : 0,
  zIndex: 1
}));

export const SCell = glamorous(Cell)({}, ({ theme, isRow, isRowHeader }) => ({
  backgroundColor: theme.table.sBg,
  borderBottom: isRowHeader ? `1px solid ${theme.table.border}` : null,
  textAlign: 'center',
  padding: '4px 0px !important',
  zIndex: isRow ? 0 : 1
}));
