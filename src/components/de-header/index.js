import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { ButtonGroup, Button } from '@blueprintjs/core';
import R from 'ramda';

const Wrapper = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
}, ({ isRtl, theme }) => ({
  flexDirection: isRtl ? 'row-reverse' : 'row',
  ...theme.header,
}));

const Logo = glamorous.img({
  maxHeight: 45,
});

const StyledButton = glamorous(Button)({
  background: 'none !important',
  height: 36,
}, ({ active, theme }) => ({
  color: `${active ? theme.header.selectedColor : theme.header.color} !important`,
}));

export const DEHeader = ({
  availableLocales, locale, changeLocale, isRtl, logo, LabelRenderer, user
}) => (
  <Wrapper isRtl={isRtl}>
    <Logo src={logo} alt="logo" />
    <ButtonGroup minimal>
      {user}
      {
        R.map(l => (
            <StyledButton
              key={l}
              active={l === locale}
              onClick={l === locale ? null : () => changeLocale(l)}
            >
              <LabelRenderer id={l} />
            </StyledButton>
          ))(availableLocales)
      }
    </ButtonGroup>
  </Wrapper>
);

DEHeader.propTypes = {
  user: PropTypes.node,
  availableLocales: PropTypes.array,
  locale: PropTypes.string,
  changeLocale: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
  logo: PropTypes.string,
  LabelRenderer: PropTypes.func,
};
