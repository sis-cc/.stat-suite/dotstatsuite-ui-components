import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEHeader, Formats, Themes } from '../../';
import logo from '../../assets/sis-cc-logo.png';

export const SBHeader = (boilers) => {
  const availableLocales = ['en', 'fr', 'ar'];
  const isArabic = boolean('isArabic (header)', false);

  return (
    <DEHeader
      availableLocales={availableLocales}
      locale={isArabic ? 'ar' : 'en'}
      changeLocale={action('changeLocale')}
      {...boilers}
      LabelRenderer={FormattedMessage}
      logo={logo}
    />
  );
};

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('header', () => {
  const BoiledHeader = storyWithBoil({ Formats, Themes })(SBHeader);
  return <BoiledHeader />;
});
