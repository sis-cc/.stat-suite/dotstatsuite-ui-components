import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DEHeader, Themes } from '../../';
import logo from '../../assets/sis-cc-logo.png';

describe('DE - Header', () => {
  const props = {
    availableLocales: ['en', 'fr', 'ar'],
    locale: 'en',
    changeLocale: () => {},
    logo,
    LabelRenderer: FormattedMessage,
  };

  const propsWithoutLocales = {
    availableLocales: [],
    locale: 'en',
    changeLocale: () => {},
    logo,
    LabelRenderer: FormattedMessage,
  };

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<DEHeader {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<DEHeader {...props} isRtl />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot without Locales available', () => {
    const component = renderWithGoogles(<DEHeader {...propsWithoutLocales} isRtl />);
    expect(component).toMatchSnapshot();
  });
});
