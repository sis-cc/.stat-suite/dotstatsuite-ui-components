import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { compose } from 'recompose';
import R from 'ramda';
import { storyWithBoil } from '../../../helpers/story-helper';
import {
  VXFilterContainer, VXFilterContainerCollapsible, Layout, Colors, Formats, FontSizes, Themes
} from '../../';

const Element = ({
  backgroundColor = Colors.BLUE2,
  color = Colors.WHITE1,
  heightFactor,
  label,
}) => (
  <div style={{
    width: '100%',
    height: Layout.FILTER_MAX_HEIGHT * heightFactor,
    backgroundColor,
    color,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: FontSizes.SIZE1,
  }}>
    <div>{label}</div>
  </div>
);

export const SBFilterContainer = boilers => {
  const props = {
    ...boilers,
    statusLabel: boolean('hasStatus (container)', true) ? 'status' : null,
    isBlank: boolean('isBlank (container)', false),
    noScroll: boolean('noScroll (container)', false),
    noHeader: boolean('noHeader (container)', false),
    topElement: <Element backgroundColor={Colors.GREEN1} label="topElement" heightFactor={0.2} />,
  };

  return (
    <div>
      <VXFilterContainer { ...props } titleLabel="container">
        <Element
          label="content"
          heightFactor={boolean('isScrollable (container)', false) ? 1.5 : 0.8}
        />
      </VXFilterContainer>
      <VXFilterContainerCollapsible
        { ...props }
        titleLabel="collapsible container"
        isOpen={boolean('isOpen (container)', true)}
        changeIsOpen={action('changeIsOpen')}
      >
        <Element
          label="content"
          heightFactor={boolean('isScrollable (container)', false) ? 1.5 : 0.8}
        />
      </VXFilterContainerCollapsible>
    </div>
  );
}

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('filters-container', () => {
  const BoiledFilterContainer = storyWithBoil({ Formats, Themes })(SBFilterContainer);
  return <BoiledFilterContainer />;
});
