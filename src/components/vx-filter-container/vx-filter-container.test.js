import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import {
  VXFilterContainer, VXFilterContainerCollapsible, Layout, Colors, Formats, FontSizes, Themes
} from '../../';

const Element = ({
  backgroundColor = Colors.BLUE2,
  color = Colors.WHITE1,
  heightFactor
}) => (
  <div style={{
    width: '100%',
    height: Layout.FILTER_MAX_HEIGHT * heightFactor,
    backgroundColor,
    color,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: FontSizes.SIZE1,
  }}>
    <div>content</div>
  </div>
);

describe('VX - filter container', () => {
  const statusLabel = isStatusLabel => ({
    statusLabel: isStatusLabel ? 'status' : null,
  })

  const props = {
    topElement: <Element backgroundColor={Colors.GREEN1} label="topElement" heightFactor={0.2} />,
  };

  describe('container', () => {
    it('should match snapshot ltr', () => {
      const component = renderWithGoogles(
        <VXFilterContainer { ...props } titleLabel="container">
          <Element heightFactor={0.8}/>
        </VXFilterContainer>);
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot rtl',() => {
      const component = renderWithGoogles(
        <VXFilterContainer { ...props } isRtl titleLabel="container" >
          <Element heightFactor={0.8}/>
        </VXFilterContainer>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot noHeader',() => {
      const component = renderWithGoogles(
        <VXFilterContainer { ...props } isRtl titleLabel="container" noHeader>
          <Element heightFactor={0.8}/>
        </VXFilterContainer>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot noScroll',() => {
      const component = renderWithGoogles(
        <VXFilterContainer { ...props } isRtl titleLabel="container" noScroll>
          <Element heightFactor={0.8}/>
        </VXFilterContainer>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot with tagLabel',() => {
      const component = renderWithGoogles(
        <VXFilterContainer { ...props } isRtl titleLabel="container" tagLabel="tagLabel">
          <Element heightFactor={0.8}/>
        </VXFilterContainer>
      );
      expect(component).toMatchSnapshot();
    });
  });

  describe('container Collapsible', () => {
   it('should match snapshot ltr', () => {
      const component = renderWithGoogles(
        <VXFilterContainerCollapsible
          { ...props }
          titleLabel="collapsible container"
          isOpen
          changeIsOpen={() => {}}
        >
          <Element heightFactor={0.8}/>
        </VXFilterContainerCollapsible>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot rtl ', () => {
      const component = renderWithGoogles(
        <VXFilterContainerCollapsible
          { ...props }
          isRtl
          titleLabel="collapsible container"
          isOpen
          changeIsOpen={() => {}}
        >
          <Element
            label="content"
            heightFactor={0.8}
          />
        </VXFilterContainerCollapsible>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot noHeader ', () => {
      const component = renderWithGoogles(
        <VXFilterContainerCollapsible
          { ...props }
          noHeader
          titleLabel="collapsible container"
          isOpen
          changeIsOpen={() => {}}
        >
          <Element
            label="content"
            heightFactor={0.8}
          />
        </VXFilterContainerCollapsible>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot noScroll ', () => {
      const component = renderWithGoogles(
        <VXFilterContainerCollapsible
          { ...props }
          noScroll
          titleLabel="collapsible container"
          isOpen
          changeIsOpen={() => {}}
        >
          <Element
            label="content"
            heightFactor={0.8}
          />
        </VXFilterContainerCollapsible>
      );
      expect(component).toMatchSnapshot();
    });

    it('should match snapshot with tagLabel ', () => {
      const component = renderWithGoogles(
        <VXFilterContainerCollapsible
          { ...props }
          tagLabel="tagLabel"
          titleLabel="collapsible container"
          isOpen
          changeIsOpen={() => {}}
        >
          <Element
            label="content"
            heightFactor={0.8}
          />
        </VXFilterContainerCollapsible>
      );
      expect(component).toMatchSnapshot();
    });
  });
});
