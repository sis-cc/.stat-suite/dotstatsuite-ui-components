import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import cx from 'classnames';
import R from 'ramda';
import { compose, branch, renderNothing } from 'recompose';
import { Collapse, Button, Classes, Tag, Intent } from '@blueprintjs/core';
import { withCollapse } from '../../';

const Container = glamorous.div({}, ({ noHeader, theme }) => ({
  borderTop: noHeader ? null : theme.filterContainer.borderTop,
}));

const Header = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
  paddingBottom: 2,
  alignItems: 'baseline',
}, ({ theme }) => ({
  '& h6': {
    width: '100%',
    color: theme.filterContainer.color,
    margin: 0,
    lineHeight: '30px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'baseline',
  },
}));

const StyledButton = glamorous(Button)({
  outline: 'none',
});

const StyledTag = glamorous(Tag)({
  margin: '0 5px',
  minWidth: 'initial',
}, ({ theme }) => ({
  backgroundColor: theme.filterContainer.background,
}));

const WrapperChildren = glamorous.div({
  // does not work in IE
  /*
  '&::-webkit-scrollbar': {
    width: 7,
  },
  '&::-webkit-scrollbar-thumb': {
    borderRadius: 4,
    backgroundColor: 'rgba(0,0,0,.5)',
  },
  */
}, ({ noScroll, theme }) => {
  if (noScroll) return {};

  return {
    maxHeight: theme.filterContainer.maxHeight,
    overflow: 'auto',
  };
});

export const FilterContainer = ({
  titleLabel, statusLabel, tagLabel, isOpen, onChangeIsOpen, topElement, children,
  isRtl, noScroll, noHeader,
}) => (
  <div>
    {statusLabel}
    <Container noHeader={noHeader}>
      { noHeader
        ? null
        : <Header>
            <h6>{titleLabel}{ tagLabel ? <StyledTag>{tagLabel}</StyledTag> : null }</h6>
            { R.is(Function, onChangeIsOpen)
              ? <StyledButton
                  className={Classes.MINIMAL}
                  iconName={cx({ 'chevron-up': isOpen, 'chevron-down': !isOpen })}
                  onClick={onChangeIsOpen}
                />
              : null
            }
          </Header>
      }
      <Collapse isOpen={isOpen}>
        {topElement}
        <WrapperChildren noScroll={noScroll}>{children}</WrapperChildren>
      </Collapse>
    </Container>
  </div>
);

FilterContainer.propTypes = {
  titleLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  statusLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  tagLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  noScroll: PropTypes.bool,
  noHeader: PropTypes.bool,
  isOpen: PropTypes.bool,
  isRtl: PropTypes.bool,
  onChangeIsOpen: PropTypes.func,
  children: PropTypes.node,
  topElement: PropTypes.node,
};

FilterContainer.defaultProps = {
  isOpen: true,
};

const withBlank = branch(({ isBlank }) => isBlank, renderNothing);
export const VXFilterContainer = withBlank(FilterContainer);
export const VXFilterContainerCollapsible = withCollapse(VXFilterContainer);
