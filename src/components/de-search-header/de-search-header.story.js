import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { FormattedMessage } from 'react-intl';
import R from 'ramda';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DESearchHeader, Formats, Themes } from '../../';
import logo from '../../assets/dotstat-data-explorer-logo.png';

const searchLocations = {
  'de.search.header.page': {
    id: 'de.search.header.page',
    accessor: 'page',
    isSelected: true,
  },
  'de.search.header.data.explorer': {
    id: 'de.search.header.data.explorer',
    accessor: 'DE',
    disabled: true,
  },
};

export const SBSearchHeader = boilers => (
  <DESearchHeader
    {...boilers}
    logo={logo}
    onlyLogo={
      R.has('onlyLogo')(boilers)
        ? R.prop('onlyLogo')(boilers)
        : boolean('onlyLogo (search-header)', false)
    }
    changeSpotlight={action('changeSpotlight')}
    term="pop"
    placeholder={boilers.intl.formatMessage({ id: 'vx.spotlight.placeholder' })}
    spotlightOptions={{
      FieldLabelRenderer: FormattedMessage,
      rightIconName: 'caret-down',
      searchLabel: <FormattedMessage id="de.search.header"/>,
      fields: searchLocations,
    }}
  />
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('search-header', () => {
  const BoiledSearchHeader = storyWithBoil({ Formats, Themes })(SBSearchHeader);
  return <BoiledSearchHeader />;
});
