import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { css } from 'glamor';
import cx from 'classnames';
import { VXSpotlight, Theme, Layout } from '../../';

const Wrapper = glamorous.div({
  display: 'flex',
  alignItems: 'center',
  minHeight: 60,
}, ({ isRtl, isNarrow, theme }) => ({
  backgroundColor: theme.searchHeader.background,
  flexDirection: isNarrow ? 'column' : (isRtl ? 'row-reverse' : 'row'),
  justifyContent: isNarrow ? 'center' : 'space-between',
  padding: `${isNarrow ? 15 : 0}px ${theme.searchHeader.padding}`,
}));

const Logo = glamorous.img({
  maxWidth: 300,
  maxHeight: 48,
});

const VXSpotlightWrapper = glamorous.div({
  minWidth: 200,
}, ({ isNarrow }) => ({
  width: isNarrow ? '75%' : '50%',
  paddingTop: isNarrow ? 15 : null,
}));

export const DESearchHeader = ({
  isRtl, isNarrow, onlyLogo, logo, spotlightOptions, changeSpotlight, term, placeholder,
}) => (
  <Wrapper isRtl={isRtl} isNarrow={isNarrow}>
    <Logo src={logo} alt="logo" />
    { onlyLogo
      ? null
      : <VXSpotlightWrapper isNarrow={isNarrow}>
          <VXSpotlight
            hasClearAll
            hasCommit
            isRtl={isRtl}
            isNarrow={isNarrow}
            spotlight={spotlightOptions}
            term={term}
            changeSpotlight={changeSpotlight}
            placeholder={placeholder}
          />
        </VXSpotlightWrapper>
    }
  </Wrapper>
);

DESearchHeader.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
  onlyLogo: PropTypes.bool,
  logo: PropTypes.string,
  spotlightOptions: PropTypes.object,
  changeSpotlight: PropTypes.func,
};
