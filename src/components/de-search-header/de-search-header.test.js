import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { DESearchHeader } from '../../';

describe('DE - SearchHeader', () => {
  const props = {
    searchTerm: "",
    changeSearchTerm: () => {},
    onChange: () => {},
    onClear: () => {},
    deLabel: <FormattedMessage id="search.bar.data.explorer"/>,
    searchLabel: <FormattedMessage id="search.bar.page"/>,
  };

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles( 
      <DESearchHeader {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles( 
      <DESearchHeader {...props} isRtl/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isNarrow', () => {
    const component = renderWithGoogles( 
      <DESearchHeader {...props} isNarrow/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isNarrow and isRtl', () => {
    const component = renderWithGoogles( 
      <DESearchHeader {...props} isNarrow isRtl/>);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot onlyLogo', () => {
    const component = renderWithGoogles( 
      <DESearchHeader {...props} onlyLogo/>);
    expect(component).toMatchSnapshot();
  });
});
