import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import { storyWithBoil } from '../../../helpers/story-helper';
import { VXSpotlight, Formats, Colors, VXFilterContainer, Themes } from '../../';

const fields = {
  'vx.spotlight.field.label': {
    id: 'vx.spotlight.field.label',
    accessor: 'label',
    isSelected: true,
  },
  'vx.spotlight.field.code': {
    id: 'vx.spotlight.field.code',
    accessor: 'code',
    disabled: true,
  },
};

const spotlightOptions = (isSearch, hasOneOrLessFields) => ({
  fields: hasOneOrLessFields ? {} : fields,
  searchLabel: isSearch ? <FormattedMessage id="de.search.header"/> : null,
  rightIconName: isSearch ? 'caret-down' : null,
  iconName: isSearch ? null : 'filter-list',
  FieldLabelRenderer: FormattedMessage,
});

export const SBSearchHome = props => (
  <VXSpotlight
    {...props}
    hasClearAll hasCommit isHome
    spotlight={spotlightOptions(true, true)}
  />
);

export const SBSpotlight = (boilers) => {
  const commonProps = {
    ...boilers,
    changeSpotlight: action('changeSpotlight'),
    placeholder: boilers.intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
  };

  return (
    <div>
      <VXFilterContainer {...boilers} titleLabel="search as spotlight (ie in filter)">
        <div style={{ padding: 15 }}>
          <VXSpotlight
            {...commonProps}
            spotlight={spotlightOptions(false, true)}
          />
        </div>
        <div style={{ padding: 15 }}>
          <VXSpotlight
            {...commonProps}
            spotlight={spotlightOptions(false, false)}
          />
        </div>
      </VXFilterContainer>
      <VXFilterContainer {...boilers} titleLabel="search in home">
        <div style={{ backgroundColor: Colors.BLUE2, padding: 15 }}>
          <SBSearchHome {...commonProps} />
        </div>
      </VXFilterContainer>
      <VXFilterContainer {...boilers} titleLabel="search in bar">
        <div style={{ backgroundColor: Colors.BLUE2, padding: 15 }}>
          <VXSpotlight
            {...commonProps}
            hasClearAll hasCommit
            spotlight={spotlightOptions(true, false)}
          />
        </div>
      </VXFilterContainer>
    </div>
  );
};

const stories = storiesOf('visions', module);
stories.addDecorator(withKnobs);
stories.add('spotlight', () => {
  const BoiledSpotlight = storyWithBoil({ Formats, Themes })(SBSpotlight);
  return <BoiledSpotlight />;
});

