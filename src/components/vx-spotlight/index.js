import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Button, Intent } from '@blueprintjs/core';
import cx from 'classnames';
import R from 'ramda';
import * as S from './styles';
import { size } from '../../utils';
import { VXSpotlightFields } from './vx-spotlight-fields';
import { withSpotlight } from './with-spotlight';

export const Spotlight = ({
  term, placeholder, spotlight,
  onChangeTerm, onChangeField, onClearTerm, onCommitTerm, onKeyPress,
  hasClearAll, isRtl, hasCommit, isHome, isNarrow,
}) => {
  const clearAllButton = hasClearAll && !R.isEmpty(term)
    ? <Button
        className={cx(Classes.MINIMAL)}
        iconName="cross"
        onClick={onClearTerm}
      />
    : null;

  const commitButton = hasCommit
    ? <Button
        disabled={R.isEmpty(term)}
        className={cx(Classes.MINIMAL)}
        intent={Intent.PRIMARY}
        iconName="arrow-right"
        onClick={onCommitTerm}
      />
    : null;

  const spotlightFields = R.gt(size(R.prop('fields', spotlight)), 1)
    ? <VXSpotlightFields
        spotlight={spotlight}
        onChangeField={onChangeField}
        isRtl={isRtl}
        term={term}
      />
    : null;

  const rightElement = isRtl
    ? <div>{commitButton}{spotlightFields}{clearAllButton}</div>
    : <div>{clearAllButton}{spotlightFields}{commitButton}</div>;

  return (
    <S.Wrapper isHome={isHome} isNarrow={isNarrow}>
      <S.StyledInputGroup
        leftIconName="search"
        onChange={onChangeTerm}
        placeholder={placeholder}
        value={term}
        rightElement={rightElement}
        isRtl={isRtl}
        hasCommit={hasCommit}
        isHome={isHome}
        onKeyPress={onKeyPress}
      />
    </S.Wrapper>
  );
};

Spotlight.propTypes = {
  term: PropTypes.string,
  placeholder: PropTypes.string,
  spotlight: PropTypes.shape({
    fields: PropTypes.object,
    searchLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    rightIconName: PropTypes.string,
    iconName: PropTypes.string,
    FieldLabelRenderer: PropTypes.func,
  }),
  onChangeTerm: PropTypes.func.isRequired,
  onChangeField: PropTypes.func.isRequired,
  onClearTerm: PropTypes.func.isRequired,
  onCommitTerm: PropTypes.func,
  onKeyPress: PropTypes.func,
  hasClearAll: PropTypes.bool,
  isRtl: PropTypes.bool,
  hasCommit: PropTypes.bool,
  isHome: PropTypes.bool,
  isNarrow: PropTypes.bool,
};

Spotlight.defaultProps = {
  term: '',
  spotlight: {},
};

export const VXSpotlight = withSpotlight(Spotlight);
