import React from 'react';
import { FormattedMessage } from 'react-intl';
import { renderWithGoogles } from '../../../helpers/test-helper';
import { VXSpotlight } from '../../';

const fields = {
  'vx.spotlight.field.label': {
    id: 'vx.spotlight.field.label',
    accessor: 'label',
    isSelected: true,
  },
  'vx.spotlight.field.code': {
    id: 'vx.spotlight.field.code',
    accessor: 'code',
    disabled: true,
  },
};

const spotlightOptions = (isSearch, hasOneOrLessFields) => ({
  fields: hasOneOrLessFields ? {} : fields,
  searchLabel: isSearch ? <FormattedMessage id="de.search.header"/> : null,
  rightIconName: isSearch ? 'caret-down' : null,
  iconName: isSearch ? null : 'filter-list',
  FieldLabelRenderer: FormattedMessage,
});

describe('VX - spotlight', () => {
  const commonProps = {
    changeSpotlight: () => {},
    placeholder: 'placeholder',
  };

  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        spotlight={spotlightOptions(false, true)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(<VXSpotlight
        isRtl
        {...commonProps}
        spotlight={spotlightOptions(false, true)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot ltr spotlightOptions false false', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot hasCommit', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        isRtl
        hasCommit
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot hasClearAll', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        hasClearAll
        term="toto"
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isHome', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        isHome
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isHome isNarrow', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        isHome
        isNarrow
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot isNarrow', () => {
    const component = renderWithGoogles(<VXSpotlight
        {...commonProps}
        isNarrow
        spotlight={spotlightOptions(false, false)}
      />);
    expect(component).toMatchSnapshot();
  });
});
