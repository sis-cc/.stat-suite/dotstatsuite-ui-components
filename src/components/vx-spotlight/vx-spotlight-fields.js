import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Popover, Position, Button } from '@blueprintjs/core';
import cx from 'classnames';
import R from 'ramda';
import * as S from './styles';

export const VXSpotlightFields = ({ spotlight, onChangeField, isRtl }) => (
  <Popover
    position={Position.BOTTOM}
    target={
      <S.StyledButton
        className={Classes.MINIMAL}
        isRtl={isRtl}
        iconName={
          isRtl && R.prop('rightIconName', spotlight)
            ? R.prop('rightIconName', spotlight)
            : R.prop('iconName', spotlight)
        }
        rightIconName={
          isRtl && R.prop('rightIconName', spotlight)
            ? R.prop('iconName', spotlight)
            : R.prop('rightIconName', spotlight)
        }
        text={R.prop('searchLabel', spotlight)}
      />
    }
    content={
      <S.FieldsWrapper
        className={cx(Classes.BUTTON_GROUP, Classes.MINIMAL, Classes.VERTICAL, Classes.ALIGN_LEFT)}
        isRtl={isRtl}
      >
        { R.pipe(
            R.values,
            R.map(field => (
              <Button
                key={field.id}
                className={cx(Classes.MINIMAL)}
                onClick={onChangeField(field)}
                iconName={field.isSelected ? 'tick' : (isRtl ? null : 'blank') }
                disabled={field.disabled}
              >
                <spotlight.FieldLabelRenderer id={field.id} />
              </Button>
            )),
          )(R.prop('fields', spotlight))
        }
      </S.FieldsWrapper>
    }
  />
);

VXSpotlightFields.propTypes = {
  spotlight: PropTypes.shape({
    fields: PropTypes.shape({
      id: PropTypes.string,
      isSelected: PropTypes.bool,
      disabled: PropTypes.bool,
    }),
    searchLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    rightIconName: PropTypes.string,
    iconName: PropTypes.string,
    FieldLabelRenderer: PropTypes.func,
  }),
  onChangeField: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
};

VXSpotlightFields.defaultProps = {
  spotlight: {},
};
