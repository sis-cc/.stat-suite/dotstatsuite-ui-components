import { Classes, Button, InputGroup } from '@blueprintjs/core';
import glamorous from 'glamorous';

export const Wrapper = glamorous('div', { filterProps: ['isHome', 'isNarrow'] })(
  {},
  ({ isHome, isNarrow }) => ({
    padding: isHome ? 15 : null,
    borderRadius: isHome ? 4 : null,
    width: isHome ? (isNarrow ? 340 : 420) : '100%',
    backgroundColor: isHome ? 'rgba(0, 0, 0, 0.2)' : null,
  }),
);

export const StyledInputGroup = glamorous(InputGroup,
  { filterProps: ['isRtl', 'hasCommit', 'isHome'] })
({
  display: 'flex',
  width: '100%',
}, ({ isRtl, hasCommit, isHome }) => ({
  direction: isRtl ? 'rtl' : 'ltr',
  transform: isRtl ? 'scaleX(-1)' : null,
  [`& .${Classes.INPUT}`]: {
    transform: isRtl ? 'scaleX(-1)' : null,
    textAlign: isRtl ? 'right' : null,
    paddingRight: isRtl ? '40px !important' : null,
    paddingLeft: isRtl
      ? isHome || !hasCommit ? '66px !important' : '166px !important'
      : null,
  },
}));

export const FieldsWrapper = glamorous('div', { filterProps: ['isRtl'] })({
  padding: 10,
}, ({ isRtl }) => ({
  direction: isRtl ? 'rtl' : 'ltr',
  '& span': {
    marginRight: isRtl ? 7 : null,
  },
}));

export const StyledButton = glamorous(Button, { filterProps: ['isRtl'] })(
  {},
  ({ isRtl }) => ({
    direction: isRtl ? 'ltr' : null,
    transform: isRtl ? 'scaleX(-1)' : null,
  }),
);
