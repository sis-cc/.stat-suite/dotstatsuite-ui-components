import React from 'react';
import R from 'ramda';
import PropTypes from 'prop-types';

export const withSpotlight = (Component) => {
  class EnhancedComponent extends React.Component {
    state = { term: '', spotlight: {} };

    componentWillMount = () => this.rehydrateState(this.props);
    componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

    onKeyPress = (event) => { if (event.key === 'Enter') this.bubbleUp(); };

    onChangeTerm = (event) => {
      event.preventDefault();
      this.setState(
        {
          term: R.propOr('', 'value')(event.target),
        },
        this.props.hasCommit ? null : this.bubbleUp,
      );
    };

    onClearTerm = (event) => {
      event.preventDefault();
      this.setState({ term: '' }, this.bubbleUp);
    };

    onChangeField = field => (event) => {
      event.preventDefault();
      this.setState({
        spotlight: {
          ...this.state.spotlight,
          fields: {
            ...this.state.spotlight.fields,
            [field.id]: { ...field, isSelected: !field.isSelected },
          },
        },
      }, this.bubbleUp);
    };

    onCommitTerm = (event) => {
      event.preventDefault();
      this.bubbleUp();
    };

    bubbleUp = () => {
      if (R.is(Function, this.props.changeSpotlight)) this.props.changeSpotlight({ ...this.state });
    };

    rehydrateState = (props) => {
      this.setState({
        term: R.propOr(R.prop('term')(this.state), 'term')(props),
        spotlight: R.propOr(R.prop('spotlight')(this.state), 'spotlight')(props),
      });
    };

    render = () => (
      <Component
        {...this.props}
        term={this.state.term}
        spotlight={this.state.spotlight}
        onChangeTerm={this.onChangeTerm}
        onClearTerm={this.onClearTerm}
        onChangeField={this.onChangeField}
        onCommitTerm={this.props.hasCommit ? this.onCommitTerm : null}
        onKeyPress={this.props.hasCommit ? this.onKeyPress : null}
      />
    );
  }

  EnhancedComponent.propTypes = {
    changeSpotlight: PropTypes.func,
    hasCommit: PropTypes.bool,
  };

  return EnhancedComponent;
};
