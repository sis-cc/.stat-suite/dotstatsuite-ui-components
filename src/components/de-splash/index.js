import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { theme } from '../../';

const Wrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Logo = glamorous.img({
  maxHeight: 150,
  maxWidth: '100%',
});

const Title = glamorous.h1({
  fontWeight: 'normal',
}, ({ theme }) => ({
  color: theme.splash.color,
  fontSize: theme.splash.fontSize,
}));

export const DESplash = ({ title, logo }) => (
  <Wrapper>
    <Logo src={logo} alt="logo" />
    <Title>{title}</Title>
  </Wrapper>
);

DESplash.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  logo: PropTypes.string,
};
