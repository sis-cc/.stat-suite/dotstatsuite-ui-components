import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DESplash, Formats, Colors, Themes } from '../../';
import logo from '../../assets/dotstat-data-explorer-logo.png';

export const SBSplash = () => (
  <DESplash title={<FormattedMessage id="de.search.splash" />} logo={logo} />
);

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('splash', () => {
  const BoiledSplash = storyWithBoil({ Formats, backgroundColor: Colors.BLUE2, Themes })(SBSplash);
  return <BoiledSplash />;
});
