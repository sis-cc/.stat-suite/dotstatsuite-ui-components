import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ThemeProvider } from 'glamorous';
import { DESplash, Themes } from '../../';
import { renderWithGoogles } from '../../../helpers/test-helper';
import logo from '../../assets/dotstat-data-explorer-logo.png';

describe('DE - splash', () => {
  it('should match snapshot ltr', () => {
    const component = renderWithGoogles(
        <DESplash title={<FormattedMessage id="de.search.splash" />} logo={logo} />
    );
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithGoogles(
        <DESplash title={<FormattedMessage id="de.search.splash" />} logo={logo} isRtl />
    );
    expect(component).toMatchSnapshot();
  });
});
