import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Button } from '@blueprintjs/core';
import CpyTCb from 'react-copy-to-clipboard';
import cx from 'classnames';

class CopyToClipboard extends React.Component {
  state = { isActive: true, message: this.props.inactiveMessageLabel };

  setActive = () => {
    this.setState({ isActive: !this.state.isActive, message: !this.state.isActive ? this.props.inactiveMessageLabel : this.props.activeMessageLabel });
  };

  callbackSetActive = () => {
    if (this.state.isActive) {
      setTimeout(() => this.setActive({ isActive: this.state.isActive }), this.props.delay);
    }
  };

  render() {
    return (
     <CpyTCb text={this.props.dataToCopy} onCopy={() => this.callbackSetActive()}>
       <Button
         className={cx(Classes.INTENT_PRIMARY, Classes.MINIMAL)}
         onClick={this.state.isActive ? () => this.setActive() : null}
         iconName={this.props.isRtl ? null : 'clipboard'}
         rightIconName={this.props.isRtl ? 'clipboard' : null}
         text={this.state.message}
       />
     </CpyTCb>
    );
  }
}

CopyToClipboard.propTypes = {
  isRtl: PropTypes.bool,
  dataToCopy: PropTypes.string.isRequired,
  inactiveMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  activeMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  delay: PropTypes.number.isRequired,
};

export default CopyToClipboard;
