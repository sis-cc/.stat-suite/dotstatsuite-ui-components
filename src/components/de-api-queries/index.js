import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import ApiQuery from './de-query';
import * as S from './styles';
import CopyToClipboard from './de-copy-to-clipboard';

export const DEAPIQueries = ({ clipboard, isNarrow, isRtl, queries }) => (
  <S.APIQueryContainer isNarrow={isNarrow}>
    {
      R.map(
        ({ id, ...queryRest }) => (
          <ApiQuery
            key={id}
            clipboard={value => <CopyToClipboard
                isRtl={isRtl}
                dataToCopy={value}
                delay={clipboard.delay}
                inactiveMessageLabel={R.path(['labels', 'inactive'], clipboard)}
                activeMessageLabel={R.path(['labels', 'active'], clipboard)}
              />
            }
            isNarrow={isNarrow}
            isRtl={isRtl}
            {...queryRest}
          />
        ),
        queries
      )
    }
  </S.APIQueryContainer>
);


DEAPIQueries.propTypes = {
  clipboard: PropTypes.shape({
    delay: PropTypes.number.isRequired,
    labels: PropTypes.shape({
      active: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      inactive: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
    })
  }),
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  queries: PropTypes.arrayOf(PropTypes.shape({
    contents: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      value: PropTypes.string.isRequired,
    })).isRequired,
    disclaimer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    id: PropTypes.string.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  })).isRequired
};
