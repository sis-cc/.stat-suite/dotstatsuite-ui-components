import React from 'react';
import R from 'ramda';

export const withApiQuery = Component => class extends React.Component {
  state = { id: undefined };

  rehydrateState = props => {
    this.setState({ id: R.path(['contents', 0, 'id'], props) });
  };

  componentWillMount = () => this.rehydrateState(this.props);
  componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

  onChangeOption = id => {
    this.setState({ id });
  };

  render = () => {
    const content = R.find(R.propEq('id', this.state.id), this.props.contents);
    return (
      <Component
        content={content}
        onChangeOption={R.length(this.props.contents) > 1 ? this.onChangeOption : undefined}
        options={this.props.contents}
        {...this.props}
      />
    );
  }; 
};
