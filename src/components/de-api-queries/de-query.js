import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Classes } from '@blueprintjs/core';
import * as R from 'ramda';
import { withApiQuery } from './with-api-query'
import CopyToClipboard from './de-copy-to-clipboard';
import * as S from './styles';

const Query = ({ clipboard, content, disclaimer, isNarrow, isRtl, onChangeOption, options, title }) => (
  <S.Query>
    <S.StyledText isRtl={isRtl}>{disclaimer}</S.StyledText>
    <S.ContainerHeader isRtl={isRtl}>
      <S.ContainerLabel isRtl={isRtl}>
        <S.StyledLabel>{title}</S.StyledLabel>
        {clipboard(content.value)}
      </S.ContainerLabel>
      {
        R.is(Function, onChangeOption)
        ?
          <S.ContainerButton isRtl={isRtl}>
          {
            R.map(
              option => <S.StyledButton
                isRtl={isRtl}
                className={cx(Classes.MINIMAL)}
                key={option.id}
                active={content.id === option.id}
                onClick={() => onChangeOption(option.id)}
                text={option.label}
              />,
              options
            )
          }
          </S.ContainerButton>
        : null
      }
    </S.ContainerHeader>
    <S.StyledTextArea
      isRtl={isRtl}
      rows={isNarrow ? 8 : 4}
      value={content.value}
      readOnly
    />
  </S.Query>
);

export default withApiQuery(Query);

Query.propTypes = {
  clipboard: PropTypes.func.isRequired,
  content: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  disclaimer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  onChangeOption: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  })),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};
