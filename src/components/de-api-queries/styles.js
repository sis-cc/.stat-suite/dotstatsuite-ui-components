import glamorous from 'glamorous';
import { Label, TextArea, Text, Button } from '@blueprintjs/core';
import { Colors } from '../..';

export const APIQueryContainer = glamorous.div({
  margin: '10px 0',
  padding: '10px 0',
  borderTop: `1px dashed ${Colors.GREY2}`,
}, ({ isNarrow }) => ({
  width: isNarrow ? 360 : '100%',
}));

export const Query = glamorous.div({}, ({ isRtl }) => ({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  textAlign: isRtl ? 'right' : null,
}));

export const StyledLabel = glamorous(Label)({
  marginTop: 5,
  marginRight: 5,
  fontWeight: 'bold',
});

export const StyledText = glamorous(Text)({
  marginTop: 5,
}, ({ isRtl }) => ({
  textAlign: isRtl ? 'right' : null,
}));

export const StyledTextArea = glamorous(TextArea, { filterProps: ['isRtl'] })({
  marginBottom: 5,
}, ({ isRtl }) => ({
  textAlign: isRtl ? 'right' : null,
}));

export const StyledButton = glamorous(Button, { filterProps: ['isRtl'] })(({ isRtl }) => ({
  '&:not(:last-child)': {
    marginRight: isRtl ? null : 5,
    marginLeft: isRtl ? 5 : null,
  },
}));

export const ContainerHeader = glamorous.div({
  display: 'flex',
  marginBottom: 5,
  justifyContent: 'space-between',
  flexWrap: 'wrap',
}, ({ isRtl }) => ({
  flexDirection: isRtl ? 'row-reverse' : 'row',
}));

export const ContainerButton = glamorous.div({}, ({ isRtl }) => ({
  display: 'flex',
  justifyContent: 'flex-end',
  marginBottom: 5,
  flexDirection: isRtl ? 'row-reverse' : 'row',
}));

export const ContainerLabel = glamorous.div({
  display: 'flex',
  marginBottom: 5,
}, ({ isRtl }) => ({
  flexDirection: isRtl ? 'row-reverse' : 'row',
}));
