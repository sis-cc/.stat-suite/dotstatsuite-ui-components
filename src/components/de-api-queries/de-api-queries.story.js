import React from 'react';
import { FormattedMessage } from 'react-intl';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { withNotes } from '@storybook/addon-notes';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEAPIQueries, Formats } from '../../';
import doc from './doc.md';

export const SBAPIQueries = (boilers) => {
  const queries = [{
    id: 'data',
    title: <FormattedMessage id="de.api.queries.label.data" />,
    disclaimer: <FormattedMessage
      id="de.api.queries.notice"
      values={{
        br: <br/>,
        link1: <a
          target="_blank"
          href={boilers.intl.formatMessage({ id: 'de.api.queries.notice.link1' })}>
          <FormattedMessage id="de.api.queries.notice.name.link1" />
        </a>,
        link2: <a
          target="_blank"
          href={boilers.intl.formatMessage({ id: 'de.api.queries.notice.link2' })}>
          <FormattedMessage id="de.api.queries.notice.name.link2" />
        </a>,
      }}
    />,
    contents: [{
      id: 'flat',
      label: <FormattedMessage id="de.api.queries.format.flat"/>,
      value: 'http://stats.oecd.org/SDMX-JSON/data/SNA_TABLE1/AUS.B1GD.C/all?startTime=2017&endTime=2017&dimensionAtObservation=allDimensions'
    },{
      id: 'time-series',
      label: <FormattedMessage id="de.api.queries.format.time.series"/>,
      value: 'http://stats.oecd.org/SDMX-JSON/data/SNA_TABLE1/AUS.B1GD.C/all?startTime=2017&endTime=2017'
    }]
  },{
    id: 'structure',
    title: <FormattedMessage id="de.api.queries.label.structure" />,
    disclaimer: <FormattedMessage id="de.api.queries.information"/>,
    contents: [{
      id: 'structure',
      value: 'http://stats.oecd.org/restsdmx/sdmx.ashx/GetDataStructure/SNA_TABLE1'
    }]
  }];

  return (
    <DEAPIQueries
      {...boilers}
      clipboard={{
        labels: {
          active: <FormattedMessage id="de.api.queries.active"/>,
          inactive: <FormattedMessage id="de.api.queries.inactive"/>,
        },
        delay: 1000
      }}
      queries={queries}
    />
  );
};

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('api-queries', withNotes(doc)(() => {
  const BoiledAPIQueries = storyWithBoil({ Formats })(SBAPIQueries);
  return <BoiledAPIQueries />;
}));

