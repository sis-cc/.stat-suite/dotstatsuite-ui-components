import React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallow } from 'enzyme';
import { renderWithIntl } from '../../../helpers/intl-enzyme-test-helper';
import { DEAPIQueries } from '../../';
import CopyToClipboard from './de-copy-to-clipboard';

const queries = [{
    id: 'data',
    title: 'data query',
    disclaimer: 'dataquery disclaimer',
    contents: [{
      id: 'flat',
      label: 'flat format',
      value: 'flat content'
    },{
      id: 'time-series',
      label: 'time series format',
      value: 'time series content'
    }]
  },{
    id: 'structure',
    title: 'structure query',
    disclaimer: 'structure query disclaimer',
    contents: [{
      id: 'structure',
      value: 'structure content'
    }]
  }];

describe('DE - APIQueries', () => {
  it('should match snapshot ltr', () => {
    const component = renderWithIntl(<DEAPIQueries
        queries={queries}
        clipboard={{
          labels: {
            active: 'active',
            inactive: 'inactive',
          },
          delay: 1000
        }}
      />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithIntl(<DEAPIQueries
        queries={queries}
        clipboard={{
          labels: {
            active: 'active',
            inactive: 'inactive',
          },
          delay: 1000
        }}
        isRtl
      />);
    expect(component).toMatchSnapshot();
  });
});

describe('DE - CopyToClipboard', () => {
  it('setActive()', () => {
    const component = shallow(<CopyToClipboard
        dataToCopy="http://toto"
        delay={1000}
        inactiveMessageLabel={<FormattedMessage id="de.api.queries.inactive"/>}
        activeMessageLabel={<FormattedMessage id="de.api.queries.active"/>}
      />);

    expect(component.state('isActive')).toBeTrue;
    expect(component.instance().setActive());
    expect(component.state('isActive')).toBeFalse;
  });

  it('callbackSetActive()', () => {
    const component = shallow(<CopyToClipboard
        dataToCopy="http://toto"
        delay={1000}
        inactiveMessageLabel={<FormattedMessage id="de.api.queries.inactive"/>}
        activeMessageLabel={<FormattedMessage id="de.api.queries.active"/>}
      />);

    expect(component.state('isActive')).toBeTrue;
    expect(component.instance().setActive());
    expect(component.instance().callbackSetActive());
    expect(component.state('isActive')).toBeTrue;
  });
});

