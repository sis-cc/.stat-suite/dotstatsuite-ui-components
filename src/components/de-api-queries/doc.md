### DEAPIQueries // index.js
```javascript
DEAPIQueries.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  queries: PropTypes.array.isRequired,
  delayShowMessage: PropTypes.number.isRequired,
  inactiveMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  activeMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  helpLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
};
```

### Query // de-query.js
```javascript
Query.propTypes = {
  isRtl: PropTypes.bool,
  query: PropTypes.shape({
    queryLabel: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ]).isRequired,
    data: PropTypes.string.isRequired,
    option: PropTypes.arrayOf(PropTypes.shape({
      optionLabel: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
      ]).isRequired,
      id: PropTypes.string.isRequired,
      data: PropTypes.string.isRequired,
    })),
  }).isRequired,
  delayShowMessage: PropTypes.number.isRequired,
  inactiveMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  activeMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
};
```

### CopyToClipboard // de-copy-to-Clipboard.js

```javascript
CopyToClipboard.propTypes = {
  isRtl: PropTypes.bool,
  dataToCopy: PropTypes.string.isRequired,
  inactiveMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  activeMessageLabel: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  delay: PropTypes.number.isRequired,
};
```

[x] Linter

##### Last update 14/05/2018 - by Mike V.