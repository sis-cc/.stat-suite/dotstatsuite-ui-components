import React from 'react';
import { renderWithIntl } from '../../../helpers/intl-enzyme-test-helper';
import { DEBreadcrumbs } from '../../';
import { items } from './de-breadcrumbs.story';

describe('DE - Breadcrumbs', () => {
  const props = { items };

  it('should match snapshot ltr', () => {
    const component = renderWithIntl(<DEBreadcrumbs {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot rtl', () => {
    const component = renderWithIntl(<DEBreadcrumbs {...props} isRtl/>);
    expect(component).toMatchSnapshot();
  });
});
