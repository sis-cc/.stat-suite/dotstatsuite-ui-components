import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs/react';
import { storyWithBoil } from '../../../helpers/story-helper';
import { DEBreadcrumbs, Formats } from '../../';

const items = [{
  text: 'OECD Home',
  href: 'http://www.oecd.org',
}, {
  text: 'About the OECD',
  onClick: action('breadcrumb-click'),
}, {
  text: 'What we do and how',
  disabled: true,
}];

export const SBBreadcrumbs = props => <DEBreadcrumbs {...props} items={items} />;

const stories = storiesOf('data-explorer', module);
stories.addDecorator(withKnobs);
stories.add('breadcrumbs', () => {
  const BoiledBreadcrumbs = storyWithBoil({ Formats })(SBBreadcrumbs);
  return <BoiledBreadcrumbs />;
});
