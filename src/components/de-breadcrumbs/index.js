import React from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb, Classes } from '@blueprintjs/core';
import glamorous from 'glamorous';
import R from 'ramda';

const StyledBreadcrumb = glamorous(Breadcrumb)({}, ({ isRtl }) => ({
  transform: isRtl ? 'scaleX(-1)' : null,
}));

const StyledBreadcrumbs = glamorous.ul({}, ({ isRtl }) => ({
  transform: isRtl ? 'scaleX(-1)' : null,
}));

export const DEBreadcrumbs = ({ items, isRtl }) => (
  <StyledBreadcrumbs className={Classes.BREADCRUMBS} isRtl={isRtl}>
    {R.map(item => (
      <li key={item.text}>
        <StyledBreadcrumb isRtl={isRtl} {...item} />
      </li>
    ))(items)}
  </StyledBreadcrumbs>
);

DEBreadcrumbs.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    href: PropTypes.string,
    disabled: PropTypes.bool,
    target: PropTypes.string,
    onClick: PropTypes.func,
  })),
  isRtl: PropTypes.bool,
};

DEBreadcrumbs.defaultProps = {
  items: [],
};
