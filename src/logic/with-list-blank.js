import { compose, branch, renderComponent } from 'recompose';
import R from 'ramda';

export const withListBlank = BlankComponent => Component => compose(branch(
  ({ items, isLoading }) => R.or(R.isNil(items), R.isEmpty(items)) && !isLoading,
  renderComponent(BlankComponent),
))(Component);
