import React from 'react';
import R from 'ramda';

export const withCollapse = Component => class extends React.Component {
  state = { isOpen: true };

  bubbleUp = (isOpen) => {
    if (R.is(Function, this.props.changeIsOpen)) this.props.changeIsOpen(isOpen);
  };

  rehydrateState = props => {
    this.setState({ isOpen: R.propOr(R.prop('isOpen')(this.state), 'isOpen')(props) });
  };

  onChangeIsOpen = () => {
    this.setState({ isOpen: !this.state.isOpen }, () => this.bubbleUp(!this.state.isOpen));
  };

  componentWillMount = () => this.rehydrateState(this.props);
  componentWillReceiveProps = nextProps => this.rehydrateState(nextProps);

  render = () => (
    <Component
      {...this.props}
      isOpen={this.state.isOpen}
      onChangeIsOpen={this.onChangeIsOpen}
    />
  );
};
