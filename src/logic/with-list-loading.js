import { compose, branch, renderComponent } from 'recompose';

export const withListLoading = LoadingComponent => Component => compose(branch(
  ({ isLoading }) => isLoading,
  renderComponent(LoadingComponent),
))(Component);
