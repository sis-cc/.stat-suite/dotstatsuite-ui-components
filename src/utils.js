import R from 'ramda';

// length for either arrays or objects
export const size = R.pipe(R.values, R.length);

// spotlight engine
const spotlightHandler = ({ term, fields }) => item => R.any((field) => {
  const itemValue = R.is(Function, field.accessor)
    ? field.accessor(item)
    : R.prop(field.accessor, item);
  if (R.is(Number, itemValue)) return R.contains(R.toLower(term), R.toLower(R.toString(itemValue)))
  return R.contains(R.toLower(term), R.toLower(itemValue));
})(fields);

export const spotlightTreeEngine = spotlight => (items) => {
  const isSelected = R.pipe(R.prop('isSelected'), R.equals(true));
  const fields = R.pipe(
    R.prop('fields'),
    R.defaultTo([]),
    R.filter(isSelected),
    R.values,
  )(spotlight);
  const term = R.prop('term')(spotlight);

  const recurse = R.reduce((memo, node) => {
    const nodeIsValid = spotlightHandler({ term, fields })(node);
    const childNodes = R.propOr(R.propOr([], 'childNodes')(node), '_childNodes')(node);
    if (R.isEmpty(childNodes)) return nodeIsValid ? [...memo, node] : memo;
    const validChildNodes = recurse(childNodes);
    if (R.isEmpty(validChildNodes)) return nodeIsValid ? [...memo, node] : memo;
    node._childNodes = childNodes; // !pure by purpose
    node.childNodes = validChildNodes; // !pure by purpose
    return [...memo, node];
  }, []);

  return recurse(items);
};

export const getNodePath = (node, items, pathLabel = '') => {
  if (R.isNil(node.parentId)) return pathLabel;
  const nextNode = R.prop(node.parentId)(items);

  return getNodePath(
    R.prop(node.parentId)(items),
    items,
    R.join(' > ', [nextNode.label, pathLabel]),
  );
};

export const spotlightScopeListEngine = ({ term = '', fields, items, itemsById, noPath, hasPath }) => {
  const isSelected = R.pipe(R.prop('isSelected'), R.equals(true));
  const getFields = R.pipe(R.defaultTo([]), R.filter(isSelected), R.values);

  return R.reduce((acc, item) => {
    if (spotlightHandler({ term, fields: getFields(fields) })(item)) {
      if (noPath) return [ ...acc, item ];
      return [
        ...acc,
        {
          ...item,
          nodePath: hasPath ? R.join(' > ', R.propOr([],'path')(item)) : getNodePath(item, itemsById)
        }
      ];
    }
    return acc;
  }, [])(items);
};
