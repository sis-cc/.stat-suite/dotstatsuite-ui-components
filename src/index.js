import R from 'ramda';
import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

export * from './commons';
export * from './logic';
export * from './components';
export * from './utils';
