export const Prefixes = {
  vx: 'vision',
  de: 'data-explorer',
  dlm: 'data-lifecycle-manager',
};

export const Colors = {
  GREY1: '#F0F0F0',
  GREY2: '#CCCCCC',
  GREY3: '#494444',
  GREY4: '#A4A1A1',
  GREY5: '#666666',
  BLUE1: '#2973BD',
  BLUE2: '#0965C1',
  BLUE3: '#2F75B5',
  BLUE4: '#BDD7EE',
  BLUE5: '#DDEBF7',
  BLUE6: '#1E226A',
  WHITE1: '#FFFFFF',
  BLACK1: '#000000',
  GREEN1: '#8CC841',
  ORANGE: '#f7a32c',
  /*TABLE_Y_BACKGROUND: '#B5CEEB',
  TABLE_Y_FONT_HEADER: '#1C2768',
  TABLE_Y_FONT: '#43679F',
  TABLE_Z_BACKGROUND: '#386CAA',
  TABLE_Z_FONT_HEADER: '#A2C2E4',
  TABLE_Z_FONT: WHITE1,
  TABLE_X_BACKGROUND_HEADER: '#D7E6F4',
  TABLE_X_BACKGROUND: WHITE1,
  TABLE_X_BACKGROUND_BOUNDARY: GREY1,
  TABLE_X_FONT_HEADER: '#1C2768',
  TABLE_X_FONT_HEADER_VALUE: '#43679F',*/
};

export const FontSizes = {
  SIZE1: 18,
  SIZE2: 14,
  SIZE3: 12,
};

export const Formats = {
  WIDE: 'initial',
  NARROW: 400,
};

export const Layout = {
  PADDING: '5%',
  SIDE_WIDTH: 300,
  FILTER_MAX_HEIGHT: 250,
};

export const mainTheme = {
  layout: {
    fontFamily: "'Segoe UI', 'Roboto', 'Ubuntu', 'Open Sans', sans-serif",
    background: Colors.BLUE2,
    xPadding: Layout.PADDING,
    sideWidth: Layout.SIDE_WIDTH,
  },
  header: {
    color: Colors.GREY2,
    selectedColor: Colors.BLUE1,
    background: Colors.GREY1,
    padding: `2px ${Layout.PADDING}`,
  },
  footer: {
    color: Colors.GREY3,
    selectedColor: Colors.BLUE1,
    secondaryColor: Colors.GREY2,
    background: Colors.GREY1,
    padding: `10px ${Layout.PADDING}`,
    fontSize: FontSizes.SIZE2,
  },
  topics: {
    color: Colors.BLUE1,
    text: Colors.WHITE1,
    active: Colors.ORANGE,
    fontSize: FontSizes.SIZE2,
    secondaryColor: Colors.GREY3,
    secondaryFontSize: FontSizes.SIZE3,
  },
  filtersCurrent: {
    borderTop: `1px dashed ${Colors.GREY2}`
  },
  list: {
    borderTop: `2px solid ${Colors.GREY2}`,
  },
  searchDataflow: {
    borderBottomDataflow: `1px solid ${Colors.GREY4}`,
    colorMark: Colors.GREEN1,
    borderBottomMark: `2px solid ${Colors.GREEN1} !important`,
    colorTitle: `${Colors.BLUE1} !important`,
    borderBottomTitle: `2px solid ${Colors.BLUE1}`,
    fontSizeTitle: FontSizes.SIZE1, 
    colorDescription: Colors.GREY4,
    colorLastUpdated: Colors.GREY5,
    colorTopic: Colors.BLACK1,
    borderTopTopic: `1px dashed ${Colors.GREY2}`,
  },
  filterContainer: {
    borderTop: `2px solid ${Colors.GREY2}`,
    color: Colors.GREY3,
    background: `${Colors.GREEN1} !important`,
    maxHeight: Layout.FILTER_MAX_HEIGHT,
  },
  visualisationToolbar: {
    color: Colors.BLUE1,
    borderTop: `2px solid #999`,
  },
  filter: {
    color: Colors.GREY2,
    background: Colors.GREY1,
  },
  side: {
    background: `${Colors.WHITE1} !important`,
    color: Colors.GREY3,
  },
  searchHeader: {
    background: Colors.BLUE2,
    padding: Layout.PADDING,
  },
  splash: {
    color: Colors.GREY1,
    fontSize: FontSizes.SIZE1,
  },
  table: {
    yBg: '#B5CEEB',
    yFontHeader: '#1C2768',
    yFont: '#43679F',
    zBg: '#386CAA',
    zFontHeader: '#A2C2E4',
    zFont: Colors.WHITE1,
    xBg: Colors.WHITE1,
    xBgHeader: '#D7E6F4',
    xFontHeader: '#1C2768',
    xFont: '#43679F',
    oFont: Colors.GREY3,
    sBg: Colors.GREY1,
    border: Colors.GREY4,
  },
  tableSerieFlags: {
    color: Colors.GREY2,
    fontSize: 10,
  },
  tableSectionValueFlags: {
    color: Colors.WHITE1,
    fontSize: 10,
  },
  tableRowValueFlags: {
    color: '#43679F',
    fontSize: 10,
  },
  tableColumnValueFlags: {
    color: '#43679F',
    fontSize: 10,
  },
  tableValueFlags: {
    color: Colors.GREY2,
    fontSize: 10,
  },
};

// only for demo purpose
// http://www.colourlovers.com/palette/871636/A_Dream_in_Color
export const alternativeTheme = {
  layout: {
    fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif',
    background: '#1B676B',
    xPadding: Layout.PADDING,
    sideWidth: Layout.SIDE_WIDTH,
  },
  header: {
    color: '#1B676B',
    selectedColor: '#519548',
    background: '#EAFDE6',
    padding: `24px 5%`,
  },
  footer: {
    color: '#1B676B',
    selectedColor:'#519548',
    secondaryColor: '#EAFDE6',
    background: '#EAFDE6',
    padding: `10px 5%`,
    fontSize: 14,
  },
  topics: {
    color: '#1B676B',
    fontSize: 18,
    secondaryColor: '#519548',
    secondaryFontSize: 12,
  },
  filtersCurrent: {
    borderTop: '1px dashed #EAFDE6',
  },
  list: {
    borderTop: `2px solid #EAFDE6`
  },
  searchDataflow: {
    borderBottomDataflow: '1px solid #519548',
    colorMark: '#EAFDE6',
    borderBottomMark: '2px solid #EAFDE6 !important',
    colorTitle: '#1B676B !important',
    borderBottomTitle: '2px solid #1B676B',
    fontSizeTitle: 24,
    colorDescription: '#519548',
    colorLastUpdated: '#519548',
    colorTopic: '#519548',
  },
  filterContainer: {
    borderTop: '2px solid #519548',
    color: '#1B676B',
    background: '#519548 !important',
    maxHeight: 300,
  },
  visualisationToolbar: {
    color: '#1B676B',
    borderTop: '1px solid #519548',
  },
  filter: {
    color: '#1B676B',
    background: '#EAFDE6',
  },
  side: {
    background: 'red !important',
    color:'#519548',
  },
  searchHeader: {
    background: '#EAFDE6',
    padding: `24px 5%`,
  },
  splash: {
    color: '#1B676B',
    fontSize: 22,
  },
  table: {
    yBg: '#26676D',
    yFontHeader: '#72DBC1',
    yFont: Colors.WHITE1,
    zBg: '#B42D2F',
    zFontHeader: '#F7E1BC',
    zFont: Colors.WHITE1,
    xBg: Colors.WHITE1,
    xBgHeader: '#F7E1BC',
    xFontHeader: '#B42D2F',
    xFont: '#EB7B65',
    oFont: Colors.GREY3,
    sBg: '#72DBC1',
    border: Colors.GREY4,
  },
  tableCellFlags: {
    color: Colors.GREY3,
    fontSize: 8,
  },
};

export const Themes = { mainTheme, alternativeTheme };
