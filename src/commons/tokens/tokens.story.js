import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import R from 'ramda';
import { Prefixes, Colors, Formats, Layout, FontSizes } from '../..';
import translations from '../../../translations/en.json';
import splash from '../../assets/splash.jpg';

const stories = storiesOf('tokens', module);

const Color = ({ color }) => (
  <div style={{ display: 'flex', alignItems: 'center' }}>
    <div style={{ width: 30, height: 30, backgroundColor: color }}></div>
    <div style={{ marginLeft: 10 }}>{color}</div>
  </div>
);

Color.propTypes = {
  color: PropTypes.string.isRequired,
};

const Tokens = ({ tokens, kind }) => (
  <table className="pt-table pt-bordered" style={{ width: '100%' }}>
    <thead>
      <tr>
        <th>token</th>
        <th>value</th>
      </tr>
    </thead>
    <tbody>
      {
        R.compose(
          R.map(([token, value]) => (
              <tr key={token}>
                <td style={{ verticalAlign: 'middle' }}>{token}</td>
                <td style={{ verticalAlign: 'middle' }}>
                  { kind === 'color' ? <Color color={value} /> : value }
                </td>
              </tr>
            )),
          R.toPairs,
        )(tokens)
      }
    </tbody>
  </table>
);

Tokens.propTypes = {
  tokens: PropTypes.object,
  kind: PropTypes.string,
};

stories.add('visions', () => <img src={splash} alt="splash" style={{ width: '100%' }} />);
stories.add('prefixes', () => <Tokens tokens={Prefixes} />);
stories.add('colors', () => <Tokens tokens={Colors} kind="color" />);
stories.add('font sizes', () => <Tokens tokens={FontSizes} />);
stories.add('formats', () => <Tokens tokens={Formats} />);
stories.add('layout', () => <Tokens tokens={Layout} />);
stories.add('translations', () => <Tokens tokens={translations} />);
