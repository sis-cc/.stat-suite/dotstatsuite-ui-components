# dotstatsuite-ui-components

![splash](./splash.jpg?raw=true "splash")

UI-Components is a lib of React components.  The goal is to collect pure visual UI components, isolated from business logic driving them in applications.
Components from this lib are currently used in [data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer) and [data-viewer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer) from the .Stat Suite.
If components from this library are still used and maintained, they are deprecated regarding their dependencies, and are planned to be replaced by components from [visions](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-visions) library.


## setup (dev)

1. run `npm i`
1. run `npm start` to dev with hot reloading on http://localhost:3001

## usage

1. run `npm i -S @sis-cc/dotstatsuite-ui-components`
1. `import { MyComponent } from '@sis-cc/dotstatsuite-ui-components`;

## folder structure

```
.
├── src                                   # source code
│   ├── components                        # components
│   │   ├── SomeComponent                 # component specific folder
│   │   │   ├── index.js                  # component implementation
│   │   │   ├── some-component.test.js    # structural test (jest snapshot)
│   │   │   ├── some-component.story.js   # story (storybook)
│   │   ├── index.js                      # components module interface
│   ├── logic                             # ui logic
│   │   ├── index.js                      # ui logic module interface
│   ├── index.js                          # visions module interface (and entry point)
```

## dependencies

In order to rationalize the dependencies of the components, 
authorized dependencies should be discussed:
- [blueprintjs](http://blueprintjs.com/) UI lib to speed-up design
- [glamorous](https://github.com/paypal/glamorous) encapsulate style within component (maintainability)
- [classnames](https://github.com/JedWatson/classnames) utils to deal with css classes
- [recompose](https://github.com/acdlite/recompose) FP lib to implement HOC logic
- [react-intl](https://github.com/yahoo/react-intl) i18n supports
- [ramda](http://ramdajs.com) FP utility lib

## rules

- a component should have a dedicated folder
- a component should be tested (expected coverage is over 80%)
  - run `npm test` to test all components
  - run `npm run test:u` to update snapshots
  - run `npm test -p <string|regexp>` to scope tests
- a component should have a complete story
  - all usecases should be covered
  - all props should be configurable through knobs
- code should pass lint configuration
  - run `npm run lint:fix` to automatically fix common issues
  - run `npm run lint` to check code quality
- a component should have a deep definition of its relevant propTypes
