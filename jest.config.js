module.exports = {
  testURL: 'http://localhost/',
  verbose: true,
  coverageDirectory: 'coverage/',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.js',
    '!src/components/de-table/lab/*.js', // tmp material
    '!src/**/*.story.js',
    '!src/**/*.test.js',
  ],
  coverageThreshold: {
    global: {
      statements: 0,
      branches: 0,
      functions: 0,
      lines: 0,
    },
  },
  setupTestFrameworkScriptFile: '<rootDir>/test-env.js',
  testRegex: 'src/.*\\.test\\.js$',
  moduleNameMapper: {
    '.*\\.(css|less|styl|scss|sass)$': '<rootDir>/mocks/css-module.js',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/mocks/image.js',
  },
};
